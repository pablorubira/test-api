# atlas-api

## Configuración workspace

Importante: En la construcción se debe hacer "mvn clean" para la integración de RochadeDataAcccess en el proyecto
Cada cambio de versión de RochadeDataAccess debe integrarse de este modo en el proyecto atlas-api

Importante 2: Para la Compilacion de atlas-api ss necesario tener la última versión del RochadeDataAcccess.jar y RochadeDataAccess-sources.jar  censado en atlas-api/libs/. Para ello hay que lanzar el build.cmd de ..\atlas\ARS_COMMON\RochadeDataAccess
- JAR generados ficheros:
	RochadeDataAcccess.jar 			(lanzar mvn clean package)
	RochadeDataAccess-sources.jar	(lanzar mvn source:jar)
y censarlos en atlas-api\libs\RochadeDataAccess.

## Generar nuevas versiones (Git Flow)

mvn jgitflow:release-start

mvn jgitflow:release-finish

https://bitbucket.org/atlassian/jgit-flow/wiki/Home

## Configuración usuarios

Configuracion usuarios para los distintos endpoint

### POST /technicalGroups

Hay 3 tipos de usuarios:
- Usuarios Rochade que tengan permisos en Rochade para dar de alta agrupaciones tecnicas. Podrán dar de alta las agrupaciones, pero no podran cambiar los permisos del usuario responsable
- Usuarios Rochade con permisos de administracion. Podrán dar de alta agrupaciones y cambiar los permisos de usuario responsable.
- Usuarios configurados dentro de `ARS_TEXTOS` con nombre `USUARIOS_CONSULTA_API` con rol `TECH_GROUP_SUPER`. Inicialmente y antes incluso de llamar a la recarga de autorizaciones estará  `ARS_USER`


### GET /technicalGroups/{id}
Se requiere que el usuario sea un usuario de rochade

### DELETE /authorizations

Se requiere que el usuario que lanza la petición sea un usuario adminstrador de rochade.
Se entiende por usuario administrador de rochade aquel que tiene

### GET /userAuthorizations y GET /userAuthorizations/{id}

Se requiere que el usuario sea un usuario de LDAP válido segun la configuración `atlasapi_env.properties` del servidor ldap. Además el usuario tiene que tener el rol consulta en la base de datos local. Este rol se carga dede Rochade mediante el servicio `DELETE /authorizations` leyendo la informacion presente en el itemType `ARS_TEXTOS` con nombre `USUARIOS_CONSULTA_API` en su atributo `VALORES`. En este atributo en cada linea vendrá la definicion de los roles en formato properties:

Ejemplo:

```
# Definición de roles para la aplicación atlas-api
# Este valor sigue un formato de properties java
# como clave de las propiedades se establecerá
# el id del usuario al que se le quiera asignar un rol específico
# Como valor la lista de roles separados por comas ( pueden tener espacios)
# Los roles que actualmente soporta atlas-api son
# AUTH_CONSULTA : Rol de consulta de autorizaciones
# TECH_GROUP_SUPER : Rol superusuario en el alta de agrupaciones tecnicas
#                    (puede crear agrupaciones sin necesidad de indicar usuario responsable)


ARS_USER=AUTH_CONSULTA, TECH_GROUP_SUPER
ISG_USER=AUTH_CONSULTA
XIS08265=AUTH_CONSULTA
XIS16729=AUTH_CONSULTA
XIS14832=AUTH_CONSULTA
USER_ISG=AUTH_CONSULTA

```

package com.isban.atlas.service.config;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
@Configuration
@EnableSwagger2
public class SwaggerDocumentationConfig {

   ApiInfo apiInfo() {
      return new ApiInfoBuilder().title("ATLAS API")
            .description("Santander Global Tech Software Technical Catalog (ATLAS) Management API")
            .license("Santander Global Tech").licenseUrl("https://santanderglobaltech.com")
            .termsOfServiceUrl("http://atlas.isban.gs.corp/atlas").version("1.9.0")
            .contact(new Contact("Software Technical Catalog (ATLAS)", "http://atlas.isban.gs.corp/atlas", "bzcatalo@gruposantander.com")).build();
   }

   @Bean
   public Docket customImplementation() {
      ArrayList<SecurityScheme> auth = new ArrayList<>(1);
      auth.add(new BasicAuth("basicAuth"));
      return new Docket(DocumentationType.SWAGGER_2).securitySchemes(auth).select()
            .apis(RequestHandlerSelectors.basePackage("com.isban.atlas.service.api")).build()
            .apiInfo(apiInfo());
   }

}
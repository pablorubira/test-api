package com.isban.atlas.service.config;

import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.GET;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

import com.isban.atlas.service.auth.ApiRolesConstants;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig {

   /**
    * The Class LocalDatabase.
    */
   @Configuration
   @Order(1)
   public static class LocalDatabase extends WebSecurityConfigurerAdapter {

      @Value("${ldap.basedn}")
      private String userSearchBase;
      @Value("${ldap.url}")
      private String ldapUrl;
      @Value("${ldap.passw}")
      private String ldapPassword;
      @Value("${ldap.userdn}")
      private String ldapManagerDn;

      @Autowired
      private AtlasLocalDatabaseApiAuthenticationEntryPoint authEntryPoint;
      @Autowired
      private DatabaseLdapAuthoritiesPopulator databaseLdapAuthoritiesPopulator;

      @Override
      protected void configure(HttpSecurity http) throws Exception {
         http.antMatcher("/userAuthorizations/**").authorizeRequests().anyRequest()
               .hasRole(ApiRolesConstants.AUTHORITIES_CONSULTA_ROLE).and().sessionManagement()
               .sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().csrf().disable().httpBasic()
               .authenticationEntryPoint(authEntryPoint);
      }

      @Override
      protected void configure(AuthenticationManagerBuilder auth) throws Exception {
         auth.ldapAuthentication().userSearchFilter("(corpAliasLocal={0})").userSearchBase(userSearchBase)
               .contextSource().url(ldapUrl).managerDn(ldapManagerDn).managerPassword(ldapPassword).and()
               .ldapAuthoritiesPopulator(databaseLdapAuthoritiesPopulator);
      }

      @Override
      public void configure(WebSecurity web) throws Exception {
         web.ignoring().antMatchers("/", "/v2/api-docs/**", "/configuration/ui/**", "/swagger-resources/**",
               "/configuration/security/**", "/swagger-ui.html", "/webjars/**", "/api-docs/**");
      }
   }

   /**
    * The Class RochadeApiConfigurationAdapter.
    */
   @Configuration
   @Order(2)
   public static class RochadeApiConfigurationAdapter extends WebSecurityConfigurerAdapter {
      @Autowired
      private AtlasRochadeAuthenticationProvider atlasAuthProvider;

      @Autowired
      private AtlasRochadeApiAuthenticationEntryPoint authEntryPoint;

      @Override
      protected void configure(HttpSecurity http) throws Exception {
         http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and().csrf()
               .disable().authorizeRequests().antMatchers(GET, "/").permitAll().antMatchers("/h2-console/**")
               .hasAuthority("ADMINIST").antMatchers(DELETE, "/authorizations")
               .hasAuthority("ADMINIST").anyRequest().authenticated().and().httpBasic()
               .authenticationEntryPoint(authEntryPoint);
         http.headers().frameOptions().disable();
      }

      @Autowired
      protected void configureGlobal(AuthenticationManagerBuilder auth) {
         auth.authenticationProvider(atlasAuthProvider);
      }

      @Override
      public void configure(WebSecurity web) throws Exception {
         web.ignoring().antMatchers("/", "/v2/api-docs/**", "/configuration/ui/**", "/swagger-resources/**",
               "/configuration/security/**", "/swagger-ui.html", "/webjars/**", "/api-docs/**");
      }

   }

}

package com.isban.atlas.service.config;

import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.isban.atlas.service.api.userauth.domain.UserAuthorization;
import com.isban.atlas.service.exceptions.ApiException;
import com.isban.atlas.service.repository.jpa.UserAuthorizationRepository;
import com.isban.atlas.service.repository.rochade.RochadeApiRolesRochadeRepository;
import com.isban.atlas.service.repository.rochade.RochadeSessionHolder;

import de.rochade.ds.AccessControlException;

@Component
class AtlasRochadeAuthenticationProvider implements AuthenticationProvider {

   @Autowired
   private RochadeApiRolesRochadeRepository rochadeApiRolesRochadeRepository;

   private static final Logger LOGGER = LoggerFactory.getLogger(AtlasRochadeAuthenticationProvider.class);

   @Autowired
   private RochadeSessionHolder rochadeSessionHolder;

   @Autowired
   private UserAuthorizationRepository userRepository;

   @Override
   public Authentication authenticate(Authentication auth) {
      LOGGER.info("Authentication Rochade");
      String username = auth.getName();
      String password = auth.getCredentials().toString();
      try {
         rochadeSessionHolder.openRochadeSession(username, password);
         LOGGER.info("Usuario {} autenticado con exito contra Rochade", username);
         ArrayList<GrantedAuthority> authorities = new ArrayList<>();
         addGrantedAuthorityFromRochadeMemberClass(authorities);
         addGrantedAuthorityFromLocalDatabaseRoles(username, authorities);
         return new UsernamePasswordAuthenticationToken(username, password, authorities);
      } catch (ApiException e) {
         if (isCause(AccessControlException.class, e)) {
            throw new BadCredentialsException("El usuario o la contraseña especificadas no son válidos", e);
         } else {
            throw new AuthenticationServiceException(
                  "No se ha podido comprobar el usuario debido a problemas de conexión", e);
         }
      }
   }

   private void addGrantedAuthorityFromLocalDatabaseRoles(String username,
         ArrayList<GrantedAuthority> authorities) {
      UserAuthorization user = userRepository.findByName(username);
      if (user != null) {
         for (String rol : user.getRoles()) {
            if (StringUtils.isNoneEmpty(rol)) {
               authorities.add(new SimpleGrantedAuthority(rol));
            }
         }
      }
   }

   private void addGrantedAuthorityFromRochadeMemberClass(ArrayList<GrantedAuthority> authorities) {
      String rochadeRol = rochadeApiRolesRochadeRepository.getRochadeLoggedUserType();
      if (StringUtils.isNoneEmpty(rochadeRol)) {
         authorities.add(new SimpleGrantedAuthority(rochadeRol));
         LOGGER.info("El usuario tiene establecido el rol en Rochade {}", rochadeRol);
      } else {
         LOGGER.info("El usuario no tiene establecido rol en Rochade ");
      }
   }

   @Override
   public boolean supports(Class<?> auth) {
      return auth.equals(UsernamePasswordAuthenticationToken.class);
   }

   private static boolean isCause(Class<? extends Throwable> expected, Throwable exc) {
      return expected.isInstance(exc) || exc != null && isCause(expected, exc.getCause());
   }
}

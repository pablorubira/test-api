package com.isban.atlas.service.config;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import de.rochade.ds.AccessControlException;

@Component
public class AtlasNeo4jAuthenticationProvider implements AuthenticationProvider {

   private static final Logger LOGGER = LoggerFactory.getLogger(AtlasNeo4jAuthenticationProvider.class);

   @Override
   public Authentication authenticate(Authentication auth) {
      LOGGER.info("Authentication Neo4j");
      String username = auth.getName();
      String password = auth.getCredentials().toString();
      try {
         LOGGER.info("Usuario {} autenticado con exito contra neo", username);
         return new UsernamePasswordAuthenticationToken(username, password,
               new ArrayList<GrantedAuthority>());
      } catch (Exception e) {
         if (isCause(AccessControlException.class, e)) {
            throw new BadCredentialsException("El usuario o la contraseña especificadas no son válidos", e);
         } else {
            throw new AuthenticationServiceException(
                  "No se ha podido comprobar el usuario debido a problemas de conexión", e);
         }
      }
   }

   @Override
   public boolean supports(Class<?> auth) {
      return auth.equals(UsernamePasswordAuthenticationToken.class);
   }

   public static boolean isCause(Class<? extends Throwable> expected, Throwable exc) {
      return expected.isInstance(exc) || exc != null && isCause(expected, exc.getCause());
   }
}

package com.isban.atlas.service.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;
import org.springframework.stereotype.Service;

import com.isban.atlas.service.api.userauth.domain.UserAuthorization;
import com.isban.atlas.service.repository.jpa.UserAuthorizationRepository;

@Service
class DatabaseLdapAuthoritiesPopulator implements LdapAuthoritiesPopulator {

   @Autowired
   private UserAuthorizationRepository userRepository;

   @Override
   public Collection<? extends GrantedAuthority> getGrantedAuthorities(DirContextOperations userData,
         String username) {
      List<GrantedAuthority> authorities = new ArrayList<>();
      try {
         UserAuthorization user = userRepository.findByName(username);
         if (user != null) {
            for (String rol : user.getRoles()) {
               authorities.add(new SimpleGrantedAuthority(rol));
            }
            return authorities;
         } else {
            return Collections.emptyList();
         }
      } catch (Exception e) {
         return Collections.emptyList();
      }
   }

}

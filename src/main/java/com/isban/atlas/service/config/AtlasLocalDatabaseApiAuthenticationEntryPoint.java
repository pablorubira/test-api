package com.isban.atlas.service.config;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component
class AtlasLocalDatabaseApiAuthenticationEntryPoint extends BasicAuthenticationEntryPoint {

   private static final Logger LOGGER = LoggerFactory
         .getLogger(AtlasLocalDatabaseApiAuthenticationEntryPoint.class);

   private static final String ATLAS_REALM_NAME = "AtlasLocal";

   @Override
   public void commence(HttpServletRequest request, HttpServletResponse response,
         AuthenticationException authEx) throws IOException, ServletException {
      LOGGER.error("Se ha producido un error de autenticación para el usuario " , authEx.getMessage());
      response.setContentType(MediaType.APPLICATION_JSON_UTF8_VALUE);
      PrintWriter writer = response.getWriter();
      if (authEx instanceof AuthenticationServiceException) {
         response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
         writer.println(
               "{ \"code\": \"InternalServerError\" , \"message\": \"" + authEx.getMessage() + "\"}");
      } else {
         response.addHeader("WWW-Authenticate", "Basic realm=" + getRealmName());
         response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
         writer.println("{ \"code\": \"UnauthorizedUser\" , \"message\": \"" + authEx.getMessage() + "\"}");
      }
   }

   @Override
   public void afterPropertiesSet() throws Exception {
      setRealmName(ATLAS_REALM_NAME);
      super.afterPropertiesSet();
   }
}
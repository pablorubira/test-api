package com.isban.atlas.service.api.userauth.domain;

import io.swagger.annotations.ApiModelProperty;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Society {
   @JsonIgnore
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long id;
   @JsonProperty("id")
   private String name;

   @JsonProperty("name")
   private String businessName;

   @JsonProperty("authorizations")
   @OneToOne(cascade = CascadeType.ALL, mappedBy = "society")
   private Authorizations authorizations;

   @JsonIgnore
   @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "society", orphanRemoval = true)
   private Set<UserAuthorization> users = new HashSet<>();

   public Society(String name) {
      this();
      this.name = name;
   }

   public Society() {
      this.users = new HashSet<>();
      this.authorizations = new Authorizations(this);
   }

   /**
   * Get id
   * @return id
   **/
   @ApiModelProperty(example = "94074", readOnly = true, value = "")
   public Long getId() {
      return id;
   }

   /**
   * Get authorizations
   * @return authorizations
   **/
   @ApiModelProperty(value = "")
   @Valid
   public Authorizations getAuthorizations() {
      return authorizations;
   }

   public boolean addUser(UserAuthorization user) {
      return users.add(user);
   }

   public Set<UserAuthorization> getUsers() {
      return users;
   }

   public boolean addAuthorizedClient(AuthorizedClient authorizedClient) {
      return authorizations.addAuthorizedClient(authorizedClient);
   }

   public boolean addAuthorizedSociety(AuthorizedSociety authorizedSociety) {
      return authorizations.addAuthorizedSociety(authorizedSociety);
   }

   public boolean addAuthorizedArchitecture(AuthorizedArchitecture authorizedArchitecture) {
	  return authorizations.addAuthorizedArchitecture(authorizedArchitecture);
   }

   public PreferredArchitecture setPreferredArchitecture(PreferredArchitecture preferredArchitecture) {
	  return authorizations.setPreferredArchitecture(preferredArchitecture);
	}

   /**
    * Get name
    * @return name
   **/
   @ApiModelProperty(example = "Isban Global", readOnly = true, value = "")
   public String getName() {
      return name;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getBusinessName() {
      return businessName;
   }

   public void setBusinessName(String bussinesName) {
      this.businessName = bussinesName;
   }

   @Override
   public String toString() {
      return "Society [id=" + id + ", name=" + name + ", businessName=" + businessName + ", authorizations="
            + authorizations + ", users=" + users + "]";
   }

}
package com.isban.atlas.service.api.userauth;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.isban.atlas.service.exceptions.ApiException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

@Api(value = "authorizations", tags = { "Authorizations", })

public interface AuthorizationsApi {

   @ApiOperation(value = "Flush authorizations in-memory data and reload from database", response = Void.class, authorizations = {
   @Authorization(value = "basicAuth") }, tags = { "Authorizations", })
   @ApiResponses(value = {
		 @ApiResponse(code = 204, message = "No Content", response = Void.class),
         @ApiResponse(code = 401, message = "Unauthorized (user / password not valid)", response = Error.class),
         @ApiResponse(code = 403, message = "Forbidden (user has not permission to delete user authorizations)", response = Error.class),
         @ApiResponse(code = 500, message = "Internal Server Error", response = Error.class),
         @ApiResponse(code = 200, message = "Success")})
   @DeleteMapping(produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
   @RequestMapping(value = "/authorizations",
	    produces = { "application/json" },
   		method = RequestMethod.DELETE)
   public ResponseEntity<Void> authorizationsDelete() throws ApiException;

}

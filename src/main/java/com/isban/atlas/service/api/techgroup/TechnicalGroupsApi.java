package com.isban.atlas.service.api.techgroup;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.isban.atlas.service.api.techgroup.domain.TechnicalGroup;
import com.isban.atlas.service.api.techgroup.domain.TechnicalGroupPost;
import com.isban.atlas.service.api.techgroup.domain.TechnicalGroupPut;
import com.isban.atlas.service.api.techgroup.domain.TechnicalGroupVersion;

/**
 * The Interface TechnicalGroupApi.
 */
@Api(value = "technicalGroups", tags={ "Technical Groups", })
public interface TechnicalGroupsApi {

    /**
     * Gets technical groups cataloged in ATLAS
     *
     * @param name
     * @param platform
     * @return
     */
    @ApiOperation(value = "Gets technical groups cataloged in ATLAS", notes = "Gets technical groups cataloged in ATLAS\\ Query parameters is required to filter the results ", response = TechnicalGroup.class, responseContainer = "List", authorizations = {
        @Authorization(value = "basicAuth")
    }, tags={ "Technical Groups", })
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Success", response = TechnicalGroup.class, responseContainer = "List"),
        @ApiResponse(code = 204, message = "No Content (None technical group returned)", response = Void.class),
        @ApiResponse(code = 400, message = "Bad request (errors on query parameters)", response = Error.class),
        @ApiResponse(code = 401, message = "Unauthorized (user / password not valid)", response = Error.class),
        @ApiResponse(code = 500, message = "Internal Server Error", response = Error.class) })

    @RequestMapping(value = "/technicalGroups",
        produces = { "application/json" },
        params = {"name!=", "platform!=", "repository!="},
        method = RequestMethod.GET)
    ResponseEntity<List<TechnicalGroup>> technicalGroupsGet( @NotNull @ApiParam(value = "name to filter by", required = false) @RequestParam(value = "name", required = false) String name, @NotNull @ApiParam(value = "Platform id to filter by", required = true) @RequestParam(value = "platform", required = false) String platform, @ApiParam(value = "Repository to filter by") @RequestParam(value = "repository", required = false) String repository, @ApiParam(value="All parameters", hidden = true )  @RequestParam MultiValueMap<String,String> requestParams);

   /**
    * Gets technical group by id cataloged in ATLAS.
    *
    * @param id
    *           the id
    * @return the response entity
    */
   @ApiOperation(value = "Gets technical group by id cataloged in ATLAS", notes = "", response = TechnicalGroup.class, authorizations = {
        @Authorization(value = "basicAuth")
    }, tags={ "Technical Groups", })
    @ApiResponses(value = {
         @ApiResponse(code = 200, message = "Success", response = TechnicalGroup.class),
         @ApiResponse(code = 400, message = "Bad request (errors on path parameters)", response = Error.class),
         @ApiResponse(code = 401, message = "Unauthorized (user / password not valid)", response = Error.class),
         @ApiResponse(code = 404, message = "Technical group not found", response = Void.class),
         @ApiResponse(code = 500, message = "Internal Server Error", response = Error.class) })

    @RequestMapping(value = "/technicalGroups/{id}",
        produces = { "application/json" },
        method = RequestMethod.GET)
    ResponseEntity<TechnicalGroup> technicalGroupsIdGet(@ApiParam(value = "Id of the technical group to be fetched",required=true ) @PathVariable("id") String id);


   /**
    * Modify technical group by id cataloged in ATLAS.
    *
    * @param id
    *           the id
    * @return the response entity
    */
   @ApiOperation(value = "Modifies a technical group", notes = "Modifies specified attributes of a technical group only", response = TechnicalGroup.class, authorizations = {
        @Authorization(value = "basicAuth")
    }, tags={ "Technical Groups", })
    @ApiResponses(value = {
         @ApiResponse(code = 200, message = "Success", response = TechnicalGroup.class),
         @ApiResponse(code = 400, message = "Bad request (errors on path parameters)", response = Error.class),
         @ApiResponse(code = 401, message = "Unauthorized (user / password not valid)", response = Error.class),
         @ApiResponse(code = 403, message = "Forbidden (user has not permission to modify requested technical group)", response = Error.class),
         @ApiResponse(code = 404, message = "Technical group not found", response = Void.class),
         @ApiResponse(code = 500, message = "Internal Server Error", response = Error.class) })

    @RequestMapping(value = "/technicalGroups/{id}",
        produces = { "application/json" },
        consumes = { "application/json" },
        method = RequestMethod.PUT)
    ResponseEntity<TechnicalGroup> technicalGroupsIdPut(@ApiParam(value = "Id of the technical group to be fetched",required=true ) @PathVariable("id") String id,
              @ApiParam(value = "**Only modified data of technical group object need to be sent. _Not specified attributes will remain intact_**" ,required=true )  @Valid @RequestBody TechnicalGroupPut technicalGroup);


   /**
    * Creates a new version of a technical group in ATLAS.
    *
    * @param technicalGroup
    *           the technical group
    * @return the response entity
    */
    @ApiOperation(value = "Creates a new version of a technical group in ATLAS", notes = "Creates a new version of a technical group in ATLAS", response = TechnicalGroupVersion.class, authorizations = {
       @Authorization(value = "basicAuth")
   }, tags={ "Technical Groups", })
   @ApiResponses(value = {
       @ApiResponse(code = 201, message = "Created technical group", response = TechnicalGroupVersion.class),
       @ApiResponse(code = 400, message = "Bad request (errors on body parameters)", response = Error.class),
       @ApiResponse(code = 401, message = "Unauthorized (user / password not valid)", response = Error.class),
       @ApiResponse(code = 403, message = "Forbidden (user has not permission to modify requested technical group)", response = Error.class),
       @ApiResponse(code = 404, message = "Technical group not found", response = Void.class),
       @ApiResponse(code = 409, message = "Conflict (requested technical group already exists)", response = Error.class),
       @ApiResponse(code = 500, message = "Internal Server Error", response = Error.class),
       @ApiResponse(code = 200, message = "Success", response = TechnicalGroupVersion.class) })

    @RequestMapping(value = "/technicalGroups/{id}/versions",
        produces = { "application/json" },
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<TechnicalGroupVersion> technicalGroupsIdVersionsPost(@ApiParam(value = "Id of the technical group to be fetched",required=true ) @PathVariable("id") String id,@ApiParam(value = "**The whole technical group version object must be sent**" ,required=true )  @Valid @RequestBody TechnicalGroupVersion technicalGroupVersion);


    /**
     * Creates a new technical group in ATLAS
     *
     * @param technicalGroup
     * @return
     */
   @ApiOperation(value = "Creates a new technical group in ATLAS", notes = "Creates a new technical group in ATLAS", response = TechnicalGroup.class, authorizations = {
        @Authorization(value = "basicAuth")
    }, tags={ "Technical Groups", })
   @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Created technical group", response = TechnicalGroup.class),
        @ApiResponse(code = 400, message = "Bad request (errors on body parameters)", response = Error.class),
        @ApiResponse(code = 401, message = "Unauthorized (user / password not valid)", response = Error.class),
        @ApiResponse(code = 403, message = "Forbidden (user has not permission to modify requested technical group)", response = Error.class),
        @ApiResponse(code = 409, message = "Conflict (requested technical group already exists)", response = Error.class),
        @ApiResponse(code = 500, message = "Internal Server Error", response = Error.class),
        @ApiResponse(code = 200, message = "Success", response = TechnicalGroup.class) })

    @RequestMapping(value = "/technicalGroups",
        produces = { "application/json" },
        consumes = { "application/json" },
        method = RequestMethod.POST)
    ResponseEntity<TechnicalGroup> technicalGroupsPost(@ApiParam(value = "**Name, repositories, responsible, platform and functionalService must be sent, except for _repositories_ that are not required for On-premise platforms and _responsible_ that is not required when requesting user is an ATLAS administrator.** " ,required=true )  @Valid @RequestBody TechnicalGroupPost technicalGroup);

}
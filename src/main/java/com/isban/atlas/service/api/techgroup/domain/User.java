package com.isban.atlas.service.api.techgroup.domain;

import java.util.Objects;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * User
 */

public class User {
   @JsonProperty("id")
   @NotBlank(message = "El campo id del usuario tiene que existir y no estar vacio")
   private String id = null;

   @JsonProperty("name")
   private String name = null;

   @JsonProperty("email")
   private String email = null;

   @JsonProperty("department")
   private String department = null;

   public User id(String id) {
      this.id = id;
      return this;
   }

   public User (String id) {
	  this.id = id;
   }

   public User () {}

   /**
    * Get id
    *
    * @return id
    **/
   @ApiModelProperty(example = "xIS01234", value = "")
   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public User name(String name) {
      this.name = name;
      return this;
   }

   /**
    * Get name
    *
    * @return name
    **/
   @ApiModelProperty(example = "John Doe", readOnly = true, value = "")
   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public User email(String email) {
      this.email = email;
      return this;
   }

   /**
    * Get email
    *
    * @return email
    **/
   @ApiModelProperty(example = "john.doe@isban.com", readOnly = true, value = "")
   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public User department(String department) {
      this.department = department;
      return this;
   }

   /**
    * Get department
    *
    * @return department
    **/
   @ApiModelProperty(example = "00000 - Department", readOnly = true, value = "")
   public String getDepartment() {
      return department;
   }

   public void setDepartment(String department) {
      this.department = department;
   }

   @Override
   public boolean equals(java.lang.Object o) {
      if (this == o) {
         return true;
      }
      if (o == null || getClass() != o.getClass()) {
         return false;
      }
      User user = (User) o;
      return Objects.equals(this.id, user.id)
    		 && Objects.equals(this.name, user.name)
    		 && Objects.equals(this.email, user.email)
    		 && Objects.equals(this.department, user.department);
   }

   @Override
   public int hashCode() {
      return Objects.hash(id, name, email, department);
   }

   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append("class User {\n");
      sb.append("    id: ").append(toIndentedString(id)).append("\n");
      sb.append("    name: ").append(toIndentedString(name)).append("\n");
      sb.append("    email: ").append(toIndentedString(email)).append("\n");
      sb.append("    department: ").append(toIndentedString(department)).append("\n");
      sb.append("}");
      return sb.toString();
   }

   /**
    * Convert the given object to string with each line indented by 4 spaces (except the first line).
    */
   private String toIndentedString(java.lang.Object o) {
      if (o == null) {
         return "null";
      }
      return o.toString().replace("\n", "\n    ");
   }
}
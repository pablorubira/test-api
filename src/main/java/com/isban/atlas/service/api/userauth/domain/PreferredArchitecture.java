package com.isban.atlas.service.api.userauth.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * AuthorizedArchitecture
 */

@Entity
public class PreferredArchitecture   {
	 @JsonIgnore
	 @Id
	 @GeneratedValue(strategy = GenerationType.IDENTITY)
	 private Long id;
	 @JsonProperty("id")
	 private String name;

	 @JsonProperty("name")
	 private String businessName;

	 public PreferredArchitecture() {
		 super();
	 }

	 public PreferredArchitecture(String name) {
		 this.name = name;
	 }

	 public Long getId() {
		 return id;
	 }

	 public String getName() {
		 return name;
	 }

	 public void setId(Long id) {
		 this.id = id;
	 }

	 public String getBusinessName() {
		 return businessName;
	 }

	 public void setBusinessName(String bussinesName) {
		 this.businessName = bussinesName;
	 }

	 @Override
	 public String toString() {
		 return "PreferredArchitecture id=" + id + ", name=" + name + ", businessName=" + businessName;
	 }

}
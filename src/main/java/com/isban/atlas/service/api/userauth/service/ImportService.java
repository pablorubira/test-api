package com.isban.atlas.service.api.userauth.service;

import com.isban.atlas.service.exceptions.ApiException;

public interface ImportService {

   void clearDatabase() throws ApiException;

   void load() throws ApiException;

   void reload() throws ApiException;
}

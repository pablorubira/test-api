package com.isban.atlas.service.api.userauth.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Area {
   @JsonIgnore
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @JsonProperty("area")
   private String name;

   public Area(String name) {
      this.name = name;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   @Override
   public String toString() {
      return "Area [area=" + name + "]";
   }

}
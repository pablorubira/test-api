package com.isban.atlas.service.api.userauth.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class AuthorizedClient {
   @JsonIgnore
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long id;
   @JsonProperty("id")
   private String name;

   public AuthorizedClient() {
   }

   public AuthorizedClient(String name) {
      this();
      this.name = name;
   }

   public Long getId() {
      return id;
   }

   public String getName() {
      return name;
   }

   @Override
   public String toString() {
      return "ClientDeploymentLine [id=" + id + ", name=" + name + "]";
   }

   @JsonProperty("client")
   public String getClient() {
      return name.split(" ")[0];
   }

   @JsonProperty("deploymentLine")
   public String getDeploymentLine() {
      return name.split(" ")[1];
   }
}

package com.isban.atlas.service.api.techgroup.domain;

import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Status
 */

public class Status   {
  @NotBlank(message = "No se ha especificado el parámetro obligatorio 'id'")
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("name")
  private String name = null;

  public Status id(String id) {
    this.id = id;
    return this;
  }

  public Status (String id) {
    this.id = id;
  }

  public Status () { };

  /**
  * Get id
  * @return id
  **/
  @ApiModelProperty(example = "PUBLICADA", value = "")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Status name(String name) {
    this.name = name;
    return this;
  }

  /**
  * Get name
  * @return name
  **/
  @ApiModelProperty(example = "PUBLICADA", readOnly = true, value = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Status status = (Status) o;
    return Objects.equals(this.id, status.id) &&
        Objects.equals(this.name, status.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Status {\n");

    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}


package com.isban.atlas.service.api.userauth.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Authorizations {

   @JsonIgnore
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long id;

   @JsonIgnore
   @OneToOne(cascade = CascadeType.ALL)
   @JoinColumn(name = "society_id")
   private Society society;

   @JsonProperty("authorizedClients")
   @ManyToMany(cascade = CascadeType.ALL)
   private Set<AuthorizedClient> authorizedClients = new HashSet<>();

   @JsonProperty("authorizedSocieties")
   @ManyToMany(cascade = CascadeType.ALL)
   private Set<AuthorizedSociety> authorizedSocieties = new HashSet<>();

   @JsonProperty("authorizedArchitectures")
   @ManyToMany(cascade = CascadeType.ALL)
   private Set<AuthorizedArchitecture> authorizedArchitectures = new HashSet<>();

   @JsonProperty("preferredArchitecture")
   @OneToOne(cascade = CascadeType.ALL)
   private PreferredArchitecture preferredArchitecture;

   public Authorizations() {
      super();
   }

   public Authorizations(Society society) {
      this();
      this.society = society;
   }

   public Society getSociety() {
	  return society;
   }

   public void setSociety(Society society) {
	  this.society = society;
   }

   public Set<AuthorizedClient> getAuthorizedClients() {
	  return authorizedClients;
   }

   public boolean addAuthorizedClient(AuthorizedClient authorizedClient) {
	  return this.authorizedClients.add(authorizedClient);
   }

   public Set<AuthorizedSociety> getAuthorizedSocieties() {
	  return authorizedSocieties;
   }

   public boolean addAuthorizedSociety(AuthorizedSociety authorizedSociety) {
	  return this.authorizedSocieties.add(authorizedSociety);
   }

   public Set<AuthorizedArchitecture> getAuthorizedArchitectures() {
	  return authorizedArchitectures;
   }

   public boolean addAuthorizedArchitecture(AuthorizedArchitecture authorizedArchitecture) {
	  return this.authorizedArchitectures.add(authorizedArchitecture);
   }

   public PreferredArchitecture getPreferredArchitecture() {
	  return preferredArchitecture;
   }

   public PreferredArchitecture setPreferredArchitecture(PreferredArchitecture preferredArchitecture) {
	  return this.preferredArchitecture=preferredArchitecture;
   }

}
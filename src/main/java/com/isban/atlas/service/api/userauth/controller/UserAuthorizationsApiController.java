
package com.isban.atlas.service.api.userauth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.isban.atlas.service.api.userauth.UserAuthorizationsApi;
import com.isban.atlas.service.api.userauth.domain.Area;
import com.isban.atlas.service.api.userauth.domain.UserAuthorization;
import com.isban.atlas.service.exceptions.NotFoundException;
import com.isban.atlas.service.repository.jpa.UserAuthorizationRepository;

@RestController
public class UserAuthorizationsApiController implements UserAuthorizationsApi {

   private UserAuthorizationRepository userRepository;

   @Autowired
   public UserAuthorizationsApiController(UserAuthorizationRepository userRepository) {
      this.userRepository = userRepository;
   }

   public Page<UserAuthorization> readAll(@RequestParam(value = "page", defaultValue = "0") int page,
         @RequestParam(value = "size", defaultValue = "10") int size) {
      return userRepository.findAll(new PageRequest(page, size));
   }

   public UserAuthorization userAuthorizationsIdGet(@PathVariable String id) throws NotFoundException {
      UserAuthorization user = userRepository.findByName(id);
      if (user != null) {
         return user;
      } else {
         throw new NotFoundException("codigo", "No se ha encontrado el usuario con id " + id);
      }
   }

   public Area userAuthorizationsIdAreaGet(@PathVariable String id) throws NotFoundException {
	      UserAuthorization user = userRepository.findByName(id);
	      if (user != null) {
	    	  Area area = new Area(user.getArea());
	         return area;
	      } else {
	         throw new NotFoundException("codigo", "No se ha encontrado el usuario con id " + id);
	      }
	   }
}

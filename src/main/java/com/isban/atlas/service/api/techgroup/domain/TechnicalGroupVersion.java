package com.isban.atlas.service.api.techgroup.domain;

import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * TechnicalGroupVersion
 */

@JsonPropertyOrder({ "id" })
public class TechnicalGroupVersion   {
  @JsonProperty("id")
  private String id = null;

  @JsonProperty("name")
  private String name = null;

  @NotBlank(message = "No se ha especificado el parámetro obligatorio 'version'")
  @JsonProperty("version")
  private String version = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("publishDate")
  private String publishDate = null;

  @JsonProperty("publishInstance")
  private String publishInstance = null;

  @NotNull(message = "No se ha especificado el parámetro obligatorio 'status'")
  @JsonProperty("status")
  private Status status = null;

  public TechnicalGroupVersion id(String id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(example = "93742171-2b1c-4f51-beb2-b3e88be50421 V01R00F00", readOnly = true, value = "")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public TechnicalGroupVersion name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(example = "BDPGPO_SCH", readOnly = true, value = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public TechnicalGroupVersion version(String version) {
    this.version = version;
    return this;
  }

   /**
   * Get version
   * @return version
  **/
  @ApiModelProperty(example = "V01R00F00", required = true, value = "")
  @NotNull
  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public TechnicalGroupVersion description(String description) {
    this.description = description;
    return this;
  }

   /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(example = "Lorem ipsum dolor sit amet, consectetur adipiscing elit...", value = "")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public TechnicalGroupVersion publishDate(String publishDate) {
    this.publishDate = publishDate;
    return this;
  }

   /**
   * Get publishDate
   * @return publishDate
  **/
  @ApiModelProperty(example = "2017-07-17", value = "")
  @Pattern(regexp="\\d{4}-\\d{2}-\\d{2}")
  public String getPublishDate() {
    return publishDate;
  }

  public void setPublishDate(String publishDate) {
    this.publishDate = publishDate;
  }

  public TechnicalGroupVersion publishInstance(String publishInstance) {
    this.publishInstance = publishInstance;
    return this;
  }

   /**
   * Get publishInstance
   * @return publishInstance
  **/
  @ApiModelProperty(example = "BDPGPO_SCH_V01R00F00.434543", value = "")
  public String getPublishInstance() {
    return publishInstance;
  }

  public void setPublishInstance(String publishInstance) {
    this.publishInstance = publishInstance;
  }

  public TechnicalGroupVersion status(Status status) {
    this.status = status;
    return this;
  }

   /**
   * Get status
   * @return status
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  @Valid
  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TechnicalGroupVersion technicalGroupVersion = (TechnicalGroupVersion) o;
    return Objects.equals(this.id, technicalGroupVersion.id) &&
        Objects.equals(this.name, technicalGroupVersion.name) &&
        Objects.equals(this.version, technicalGroupVersion.version) &&
        Objects.equals(this.description, technicalGroupVersion.description) &&
        Objects.equals(this.publishDate, technicalGroupVersion.publishDate) &&
        Objects.equals(this.publishInstance, technicalGroupVersion.publishInstance) &&
        Objects.equals(this.status, technicalGroupVersion.status);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name, version, description, publishDate, publishInstance, status);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class TechnicalGroupVersion {\n");

    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    publishDate: ").append(toIndentedString(publishDate)).append("\n");
    sb.append("    publishInstance: ").append(toIndentedString(publishInstance)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}
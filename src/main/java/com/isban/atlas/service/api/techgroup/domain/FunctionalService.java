package com.isban.atlas.service.api.techgroup.domain;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * FunctionalService
 */

public class FunctionalService {

   @JsonProperty("id")
   private String id = null;

   @JsonProperty("name")
   private String name = null;

   @JsonProperty("version")
   private String version = null;

   public FunctionalService id(String id) {
      this.id = id;
      return this;
   }

   public FunctionalService (String id) {
	      this.id = id;
   }

   public FunctionalService ( ) {}

   /**
    * Get id
    *
    * @return id
    **/
   @ApiModelProperty(example = "00014860", value = "")
   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public FunctionalService name(String name) {
      this.name = name;
      return this;
   }

   /**
    * Get name
    *
    * @return name
    **/
   @ApiModelProperty(example = "GESTOR OPERACIONES", readOnly = true, value = "")
   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public FunctionalService version(String version) {
      this.version = version;
      return this;
   }

   /**
    * Get version
    *
    * @return version
    **/
   @ApiModelProperty(example = "V00R00", readOnly = true, value = "")
   public String getVersion() {
      return version;
   }

   public void setVersion(String version) {
      this.version = version;
   }

   @Override
   public boolean equals(java.lang.Object o) {
      if (this == o) {
         return true;
      }
      if (o == null || getClass() != o.getClass()) {
         return false;
      }
      FunctionalService functionalService = (FunctionalService) o;
      return Objects.equals(this.id, functionalService.id)
            && Objects.equals(this.name, functionalService.name)
            && Objects.equals(this.version, functionalService.version);
   }

   @Override
   public int hashCode() {
      return Objects.hash(id, name, version);
   }

   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append("class FunctionalService {\n");
      sb.append("    id: ").append(toIndentedString(id)).append("\n");
      sb.append("    name: ").append(toIndentedString(name)).append("\n");
      sb.append("    version: ").append(toIndentedString(version)).append("\n");
      sb.append("}");
      return sb.toString();
   }

   /**
    * Convert the given object to string with each line indented by 4 spaces (except the first line).
    */
   private String toIndentedString(java.lang.Object o) {
      if (o == null) {
         return "null";
      }
      return o.toString().replace("\n", "\n    ");
   }
}
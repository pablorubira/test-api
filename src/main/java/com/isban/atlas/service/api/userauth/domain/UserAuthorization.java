package com.isban.atlas.service.api.userauth.domain;

import io.swagger.annotations.ApiModelProperty;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class UserAuthorization {
   @JsonIgnore
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long id;

   @JsonProperty("id")
   private String name;

   @JsonProperty("area")
   private String area;

   @JsonIgnore
   @ElementCollection(targetClass = String.class, fetch = FetchType.EAGER)
   private Set<String> roles = new HashSet<>();

   @ManyToOne
   private Society society;

   public UserAuthorization(String name) {
      this();
      this.name = name;
   }

   public UserAuthorization() {
   }

   /**
   * Get id
   * @return id
   **/
   @ApiModelProperty(example = "xIS01234", value = "")
   public Long getId() {
      return id;
   }

   public String getName() {
      return name;
   }

   /**
    * Get society
    * @return society
   **/
   @ApiModelProperty(value = "")
   @Valid
   public Society getSociety() {
      return society;
   }

   public void setSociety(Society society) {
      this.society = society;
   }

   public String getArea() {
      return area;
   }

   public void setArea(String area) {
      this.area = area;
   }

   public Collection<String> getRoles() {
      return roles;
   }

   public boolean addRole(String role) {
      return roles.add(role);
   }

   @Override
   public String toString() {
      return "UserAuthorization [id=" + id + ", name=" + name + ", society=" + society + "]";
   }

}

package com.isban.atlas.service.api.techgroup.controller;

import io.swagger.annotations.ApiParam;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.isban.atlas.service.api.techgroup.TechnicalGroupsApi;
import com.isban.atlas.service.api.techgroup.domain.TechnicalGroup;
import com.isban.atlas.service.api.techgroup.domain.TechnicalGroupPost;
import com.isban.atlas.service.api.techgroup.domain.TechnicalGroupPut;
import com.isban.atlas.service.api.techgroup.domain.TechnicalGroupVersion;
import com.isban.atlas.service.api.techgroup.service.TechnicalGroupService;
import com.isban.atlas.service.exceptions.ApiException;

@RestController
public class TechnicalGroupsApiController implements TechnicalGroupsApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(TechnicalGroupsApiController.class);

    @Autowired
    private TechnicalGroupService technicalGroupHelper;

    @Override
    public ResponseEntity<List<TechnicalGroup>> technicalGroupsGet( @ApiParam(value = "name to filter by") @RequestParam(value = "name", required = false) String name,
        @ApiParam(value = "Platform id to filter by") @RequestParam(value = "platform", required = false) String platform,
        @ApiParam(value = "Repository to filter by") @RequestParam(value = "repository", required = false) String repository,
        @ApiParam(value="All parameters", hidden = true ) @RequestParam MultiValueMap<String,String> requestParams) {
        LOGGER.info("Invocación del servicio de listado de agrupaciones técnicas por nombre:{}, plataforma:{} y repositorio:{}", name, platform, repository);
        technicalGroupHelper.checkParams(requestParams);
        if (null == name && null == platform && null == repository) {
        	LOGGER.info("No se ha especificado alguno de los parámetros obligatorios 'name' o 'platform' o 'repository'");
        	throw new ApiException("MissingRequiredParameter", "No se ha especificado alguno de los parámetros obligatorios 'name' o 'platform' o 'repository'");
        }
        List<TechnicalGroup> techGroupListResult = technicalGroupHelper.getTechnicalGroupsByBusinessNamePlatformRepository(name, platform, repository);
        if (techGroupListResult.isEmpty()) {
            LOGGER.info("No se ha encontrado ninguna Agrupación técnica con nombre {}, plataforma {} y repositorio {}", name, platform);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            LOGGER.info("Se devuelven las agrupaciones técnicas encontradas");
            return new ResponseEntity<>(techGroupListResult, HttpStatus.OK);
        }
    }

    @Override
    public ResponseEntity<TechnicalGroup> technicalGroupsIdGet(@ApiParam(value = "Id of the technical group to be fetched",required=true ) @PathVariable("id") String id) {
        LOGGER.info("Invocación del servicio de consulta de agrupaciones técnicas por id:{}", id);
        TechnicalGroup newTechnicalGroup = technicalGroupHelper.getTechnicalGroupById(id);
        if (newTechnicalGroup != null) {
            LOGGER.info("Se devuelve la agrupación técnica {} encontrada", id);
            return new ResponseEntity<>(newTechnicalGroup, HttpStatus.OK);
        } else {
            LOGGER.info("Agrupación técnica {} no encontrada", id);
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Override
    public ResponseEntity<TechnicalGroup> technicalGroupsIdPut(@ApiParam(value = "Id of the technical group to be fetched",required=true ) @PathVariable("id") String id,
            @ApiParam(value = "**Only modified data of technical group object need to be sent. _Not specified attributes will remain intact_**" ,required=true )  @Valid @RequestBody TechnicalGroupPut technicalGroup) {
            LOGGER.info("Invocación del servicio de modificación de agrupaciones técnicas");
            TechnicalGroup storedTechnicalGroup = technicalGroupHelper.getTechnicalGroupById(id);
            TechnicalGroup newTechnicalGroup = null;

            if (storedTechnicalGroup != null) {
                LOGGER.info("Modificando la agrupación técnica {} solicitada", id);
                newTechnicalGroup = technicalGroupHelper.modifiyTechnicalGroupPut(technicalGroup, storedTechnicalGroup);
            } else {
                LOGGER.info("Agrupación técnica {} no encontrada", id);
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(newTechnicalGroup, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<TechnicalGroupVersion> technicalGroupsIdVersionsPost(@ApiParam(value = "Id of the technical group to be fetched",required=true ) @PathVariable("id") String id,
        @ApiParam(value = "**The whole technical group version object must be sent**" ,required=true )  @Valid @RequestBody TechnicalGroupVersion technicalGroupVersion) {
        LOGGER.info("Invocación del servicio de creación de agrupaciones técnicas versión");
        TechnicalGroupVersion newTechnicalGroupVersion = technicalGroupHelper.createNewTechnicalGroupVersion(id, technicalGroupVersion);
        return new ResponseEntity<>(newTechnicalGroupVersion, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<TechnicalGroup> technicalGroupsPost(@ApiParam(value = "**Name, repositories, responsible, platform and functionalService must be sent, except for _repositories_ that are not required for On-premise platforms and _responsible_ that is not required when requesting user is an ATLAS administrator.** " ,required=true )  @Valid @RequestBody TechnicalGroupPost technicalGroup) {
        LOGGER.info("Invocación del servicio de creación de agrupaciones técnicas");
        TechnicalGroup newTechnicalGroup = technicalGroupHelper.createNewTechnicalGroup(technicalGroup);
        return new ResponseEntity<>(newTechnicalGroup, HttpStatus.CREATED);
    }

}
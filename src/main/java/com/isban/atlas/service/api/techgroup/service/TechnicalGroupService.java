package com.isban.atlas.service.api.techgroup.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestParam;

import com.isban.atlas.service.api.techgroup.domain.PublicationStatusManager;
import com.isban.atlas.service.api.techgroup.domain.Status;
import com.isban.atlas.service.api.techgroup.domain.TechnicalGroup;
import com.isban.atlas.service.api.techgroup.domain.TechnicalGroupPost;
import com.isban.atlas.service.api.techgroup.domain.TechnicalGroupPut;
import com.isban.atlas.service.api.techgroup.domain.TechnicalGroupVersion;
import com.isban.atlas.service.auth.ApiRolesConstants;
import com.isban.atlas.service.auth.IAuthenticationFacade;
import com.isban.atlas.service.exceptions.ApiException;
import com.isban.atlas.service.exceptions.BadRequestException;
import com.isban.atlas.service.exceptions.ConflictException;
import com.isban.atlas.service.exceptions.ForbiddenException;
import com.isban.atlas.service.exceptions.InternalErrorException;
import com.isban.atlas.service.exceptions.NotFoundException;
import com.isban.atlas.service.repository.rochade.RochadeConstants.MemberClass;
import com.isban.atlas.service.repository.rochade.TechnicalGroupRochadeRepository;

import de.rochade.ap.Application.UserInfo;
import es.isb.atlas.rochade.data.access.exception.RochadeDataAccessException;

@Service
public class TechnicalGroupService {

   private static final Logger LOGGER = LoggerFactory.getLogger(TechnicalGroupService.class);

   private IAuthenticationFacade authenticationFacade;
   private TechnicalGroupRochadeRepository rochadeConnector;
   private PublicationStatusManager statusManager;
   private final String PLATTFORM_STATUS = "VIGENTE";
   private final String ALMV1_FLOW = "0002";
   private final String MEXICO_FLOW = "0003";
   private final String ALMV2_FLOW = "0004";
   private final SimpleDateFormat PUBLISH_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

   @Autowired
   public TechnicalGroupService(TechnicalGroupRochadeRepository rochadeConnector,
         IAuthenticationFacade authenticationFacade, PublicationStatusManager statusManager) {
      this.rochadeConnector = rochadeConnector;
      this.authenticationFacade = authenticationFacade;
      this.statusManager = statusManager;
      this.PUBLISH_DATE_FORMAT.setLenient(false);
   }

   @Autowired
   private TechnicalGroupService technicalGroupHelper;

   /**
    * Crea una Agrupación Técnica en base al circuito de origen
    * @param techGroupInfo
    * @return
    * @throws ApiException
    */
   public TechnicalGroup createNewTechnicalGroup(TechnicalGroupPost techGroupInfo) throws ApiException {
      String serviceId = "";
      if (null != techGroupInfo.getFunctionalService()) {
    	  serviceId = techGroupInfo.getFunctionalService().getId();
      }
      String platformId = "";
      if (null != techGroupInfo.getPlatform()) {
    	  platformId = techGroupInfo.getPlatform().getId();
      } else {
    	  LOGGER.error("El campo platform es obligatorio, no se ha especificado ninguna plataforma");
  		  throw new BadRequestException("PlatformMissing", "No se ha especificado ninguna plataforma");
      }
      String techgroupName = techGroupInfo.getName();
      List<String> repositories = techGroupInfo.getRepositories();
      String userId = "";

      try {

    	 // Validaciones comunes a todos los circuitos
    	 LOGGER.info("Realizando validaciones sobre los datos");
    	 TechnicalGroup resultTechGroup = new TechnicalGroup();
    	 String source = getSourcePlatform(platformId);
         validateConnectedUserCanCreateTechGroup();
         validateFunctionalServiceExists(serviceId);
         validatePlatformExists(platformId);
         validatePlatformStatus(platformId);
         validateTechGroupNameMatchPlatformRegularExpresion(platformId, techgroupName);

         // Valida que la plataforma tenga origen asociado
         if (null == source || source.isEmpty()) {
        	LOGGER.error("La Plataforma {} no tiene origen asociado", platformId);
     		throw new BadRequestException("EmptyPlatformSource", "La plataforma " + platformId + " no tiene origen asociado");
         }

         LOGGER.info("Validaciones de datos realizadas correctamente");
         LOGGER.info("Circuito de Agrupaciones Técnicas con Plataforma " + platformId + " y Origen " + source);

         switch (source) {
         	// Circuito de Agrupaciones Técnicas con origen MEXICO
         	case MEXICO_FLOW:

         		// Valida que no se admitan repositorios
         		if ( null != repositories) {
            		LOGGER.error("El campo repositories no es válido para el circuito actual");
            		throw new BadRequestException("RepositoryForbidden", "El campo repositories no es válido para el circuito actual");
            	}

         		// Valida que la AT tenga responsable
        		if (hasResponsible(techGroupInfo)) {

	         		// Valida que la sociedad del usuario y servicio funcional sean la misma
        			userId = techGroupInfo.getResponsible().getId();
	         		validateResponsibleUserSociety(serviceId, userId);

	                String technicalGroupId = rochadeConnector.createTechGroup(techgroupName, techGroupInfo.getDescription(),userId, platformId, serviceId);
	                resultTechGroup = technicalGroupHelper.getTechnicalGroupById(technicalGroupId);
	                LOGGER.info("La agrupación técnica {} se ha creado correctamente:\n{}", techgroupName,resultTechGroup);

        		} else if (!isAuthenticationTechnicalGroupsSuperuser()) {
                    throw new BadRequestException("Error de validación de datos",
                            "El campo responsible que identifica al usuario responsable de la agupación técnica tiene que existir");
         		}
         		break;

         	// Circuito de Agrupaciones Técnicas con origen ALM v1 y v2
         	case ALMV1_FLOW:
         	case ALMV2_FLOW:

         		if (hasResponsible(techGroupInfo)) {

	         		//Valida que la sociedad del usuario es la misma que la de la aplicación o su gestor.
         			userId = techGroupInfo.getResponsible().getId();
	         		validateResponsibleUserServiceManagerSociety(serviceId, userId);

	         		// Valida los repositorios para que no sean duplicados ni se encuentren en uso
	            	if ( null != repositories) {
	            		checkDuplicateRepositories(repositories);
	            		validateRepositoriesExistsAndNotUsed(repositories);
	            		createRepositories(repositories);
	            	} else {
	            		LOGGER.error("El campo repositories es obligatorio, no se ha especificado ningún repositorio");
	            		throw new BadRequestException("RepositoryMissing", "No se ha especificado ningún repositorio");
	            	}

	                String techGroupId = rochadeConnector.createTechGroupAlm(techgroupName, techGroupInfo.getDescription(),userId, platformId, serviceId, repositories);
	                resultTechGroup = technicalGroupHelper.getTechnicalGroupById(techGroupId);
	                LOGGER.info("La agrupación técnica {} se ha creado correctamente:\n{}", techgroupName,resultTechGroup);

         		} else if (!isAuthenticationTechnicalGroupsSuperuser()) {
                    throw new BadRequestException("Error de validación de datos",
                            "El campo responsible que identifica al usuario responsable de la agupación técnica tiene que existir");
         		}
         		break;
         }

         // Cambia la Member Class del usuario si es necesario
         if (userId != null) {
            checkUserMemberClass(userId);
         }
         // Devuelve el json de la AT
         return resultTechGroup;
      } catch (ApiException aex) {
          throw aex;
      } catch (Exception rdaex) {
         LOGGER.error("Error en rochade durante la creación de la Agrupación Técnica {} ", techgroupName,
               rdaex);
         throw new InternalErrorException("RochadeConnectionRefused",
               "Se ha producido un error al intentar conectar con el servidor para crear la Agrupación Técnica",
               rdaex);
      }
   }


   /**
    * Modifica una Agrupación Técnica en base al circuito de origen
    * @param techGroupInfo
    * @return
    * @throws ApiException
    */
   public TechnicalGroup modifiyTechnicalGroupPut(TechnicalGroupPut techGroupInfo, TechnicalGroup storedTechnicalGroup) throws ApiException {

      String serviceId = "";
      if (null != techGroupInfo.getFunctionalService()) {
    	  serviceId = techGroupInfo.getFunctionalService().getId();
      }
      String storedServiceId = storedTechnicalGroup.getFunctionalService().getId();
      String platformId = storedTechnicalGroup.getPlatform().getId();
      String techgroupName = techGroupInfo.getName();
      String storedTechgroupName = storedTechnicalGroup.getName();
      List<String> repositories = techGroupInfo.getRepositories();
      List<String> currentrepositories = storedTechnicalGroup.getRepositories();
      String userId = "";
      if (null != techGroupInfo.getResponsible()) {
    	  userId = techGroupInfo.getResponsible().getId();
      }
      String techGroupId = storedTechnicalGroup.getId();
      String description = storedTechnicalGroup.getDescription();

      try {

    	 // Validaciones comunes a todos los circuitos
    	 LOGGER.info("Realizando validaciones sobre los datos");
    	 checkJsonPut(techGroupInfo);
    	 TechnicalGroup resultTechGroup = new TechnicalGroup();
    	 String source = getSourcePlatform(platformId);
         validateConnectedUserCanCreateTechGroup();

         if (!serviceId.isEmpty()) {
        	 validateFunctionalServiceExists(serviceId);
         } else {
        	 serviceId = storedServiceId;
         }

         validatePlatformExists(platformId);

         if (null != techgroupName) {
        	 validateTechGroupNameMatchPlatformRegularExpresion(platformId, techgroupName);
         }

         // Valida si hay nueva descripción
         if (null != techGroupInfo.getDescription()) {
        	 description = techGroupInfo.getDescription();
         }

         // Valida que la plataforma tenga origen asociado
         if (null == source || source.isEmpty()) {
        	LOGGER.error("La Plataforma {} no tiene origen asociado", platformId);
     		throw new BadRequestException("EmptyPlatformSource", "La plataforma " + platformId + " no tiene origen asociado");
         }

         LOGGER.info("Validaciones de datos realizadas correctamente");
         LOGGER.info("Circuito de Agrupaciones Técnicas con Plataforma " + platformId + " y Origen " + source);

         switch (source) {
         	// Circuito de Agrupaciones Técnicas con origen ALM v1 y v2
         	case ALMV1_FLOW:
         	case ALMV2_FLOW:

	         		//Valida que la sociedad del usuario es la misma que la de la aplicación o su gestor.
         			if (!serviceId.isEmpty() && !userId.isEmpty()) {
         				validateResponsibleUserServiceManagerSociety(serviceId, userId);
         			}

	         		// Valida los repositorios para que no sean duplicados ni se encuentren en uso
	            	if ( null != repositories) {
	            		checkDuplicateRepositories(repositories);
	            		validateRepositoriesNotUsed(repositories, currentrepositories);
	            		deleteRepositories(currentrepositories);
	            		createRepositories(repositories);
	            	}

	            	// Modificación de la Agrupación Técnica
	                rochadeConnector.modifyTechGroupPatch(techGroupId, techgroupName, storedTechgroupName, description,userId, platformId, serviceId, storedServiceId, repositories);
	                resultTechGroup = technicalGroupHelper.getTechnicalGroupById(techGroupId);
	                LOGGER.info("La agrupación técnica {} se ha modificado correctamente:\n{}", techgroupName,resultTechGroup);

         		break;

         }

         // Cambia la Member Class del usuario si es necesario
         if (userId != null) {
            checkUserMemberClass(userId);
         }
         // Devuelve el json de la AT
         return resultTechGroup;
      } catch (ApiException aex) {
          throw aex;
      } catch (Exception rdaex) {
         LOGGER.error("Error en rochade durante la modificación de la Agrupación Técnica {} ", techgroupName,
               rdaex);
         throw new InternalErrorException("RochadeConnectionRefused",
               "Se ha producido un error al intentar conectar con el servidor para modificar la Agrupación Técnica",
               rdaex);
      }
   }


   /**
    * Valida que la sociedad del usuario y servicio funcional sean la misma
    * @param serviceId
    * @param userId
    * @throws RochadeDataAccessException
    * @throws BadRequestException
    * @throws ForbiddenException
    */
   private void validateResponsibleUserSociety(String serviceId, String userId)
	         throws RochadeDataAccessException, BadRequestException, ForbiddenException {
      // Existe el usuario
      if (!rochadeConnector.existUser(userId)) {
         LOGGER.error("El usuario responsable {} no existe", userId);
         throw new BadRequestException("ResponsibleNotFound", "El usuario responsable indicado no existe");
      }
      // La sociedad del usuario responsable es la misma que la del servicio versión
      String serviceSociety = rochadeConnector.getFunctionalServiceSociety(serviceId);
      if (serviceSociety.isEmpty()) {
         LOGGER.error("El servicio funcional {} no pertenece a ninguna sociedad", serviceId);
         throw new ForbiddenException("FunctionalServiceWithNoSociety",
               "El servicio funcional especificado no pertenece a ninguna sociedad");
      }
      String userSociety = rochadeConnector.getUserSociety(userId);
      if (userSociety.isEmpty()) {
         LOGGER.error("El usuario responsable {} no pertenece a ninguna sociedad", userId);
         throw new ForbiddenException("UserWithNoSociety",
               "El usuario responsable especificado no pertenece a ninguna sociedad");
      }
      if (!userSociety.equals(serviceSociety)) {
         LOGGER.error(
               "La sociedad del usuario {} ({}) no se corresponde con la sociedad del Servicio Funcional {} ({})",
               userId, userSociety, serviceId, serviceSociety);
         throw new ForbiddenException("ResponsibleForbidden",
               "El usuario responsable no tiene permisos para crear una agrupación técnica asociada al servicio funcional especificado");
      }
   }


   /**
    * Valida que la sociedad del usuario es la misma que la de la aplicación o su gestor.
    * @param serviceId
    * @param userId
    * @throws RochadeDataAccessException
    * @throws BadRequestException
    * @throws ForbiddenException
    */
   private void validateResponsibleUserServiceManagerSociety(String serviceId, String userId)
	         throws RochadeDataAccessException, BadRequestException, ForbiddenException {
      // Existe el usuario
      if (!rochadeConnector.existUser(userId)) {
         LOGGER.error("El usuario responsable {} no existe", userId);
         throw new BadRequestException("ResponsibleNotFound", "El usuario responsable indicado no existe");
      }
      // Obtiene la sociedad de la aplicación a la que pertenece el servicio
      String serviceSociety = rochadeConnector.getFunctionalServiceSociety(serviceId);
      if (serviceSociety.isEmpty()) {
         LOGGER.info("El servicio funcional {} no pertenece a ninguna sociedad", serviceId);
      }
      // Obtiene la sociedad del usuario responsable
      String userSociety = rochadeConnector.getUserSociety(userId);
      // Obtiene el gestor de configuración de la aplicación
      String manager = rochadeConnector.getManagerApplication(serviceId);
      // Obtiene la sociedad del gestor de configuración de la aplicación
      String managerSociety = rochadeConnector.getUserSociety(manager);

      // El gestor de la aplicación no se encuentra configurado
      if (manager.isEmpty()) {
         LOGGER.info("La aplicación funcional del servicio {} no tiene contacto de configuración asignado", serviceId);
      }
      // El usuario responsable no tiene sociedad asignada
      if (userSociety.isEmpty()) {
         LOGGER.error("El usuario responsable {} no pertenece a ninguna sociedad", userId);
         throw new ForbiddenException("UserWithNoSociety",
               "El usuario responsable especificado no pertenece a ninguna sociedad");
      }

      // La sociedad del usuario responsable es la misma que la del servicio versión o del gestor de la aplicación
      if (!userSociety.equals(serviceSociety) && !userSociety.equals(managerSociety)) {
         LOGGER.error(
               "La sociedad del usuario {} ({}) no se corresponde con la sociedad del Servicio Funcional {} ({}) ni con la sociedad del gestor de la aplicación",
               userId, userSociety, serviceId, serviceSociety);
         throw new ForbiddenException("ResponsibleForbidden",
               "La sociedad del usuario responsable no se corresponde con la sociedad de la aplicación ni con la sociedad de su gestor");
      }

   }


   /**
    * Valida que el nombre de la Agrupación Técnica cumple la expresión regular
    *
    * @param platformId
    * @param techgroupName
    * @throws RochadeDataAccessException
    * @throws BadRequestException
    */
   private void validateTechGroupNameMatchPlatformRegularExpresion(String platformId, String techgroupName)
         throws RochadeDataAccessException, BadRequestException {
      String pattern = rochadeConnector.getPlatformTechGroupPattern(platformId);
      if (!validateTechGroupNamePattern(techgroupName, pattern)) {
         LOGGER.error(
               "El nombre de la de la agrupación técnica especificada no cumple la expresión regular {}",
               pattern);
         throw new BadRequestException("TechnicalGroupNotMatchRegExp",
               "El nombre de la de la agrupación técnica especificada no cumple la expresión regular "
                     + pattern);
      }
   }

   /**
    * Devuelve el origen de una Plataforma
    * @param platformId
    * @return
    * @throws RochadeDataAccessException
    */
   private String getSourcePlatform(String platformId) throws RochadeDataAccessException {
	   if (rochadeConnector.existPlatform(platformId)) {
		   return rochadeConnector.getSourceFromPlatform(platformId);
	   }
	return "";
   }

   /**
    * Valida que la plataforma exista en rochade
    *
    * @param platformId
    * @throws RochadeDataAccessException
    * @throws BadRequestException
    */
   private void validatePlatformExists(String platformId)
         throws RochadeDataAccessException, BadRequestException {
      if (!rochadeConnector.existPlatform(platformId)) {
         LOGGER.error("La plataforma {} no existe", platformId);
         throw new BadRequestException("PlatformNotFound", "La plataforma especificada no existe");
      }
   }

   /**
    * Valida que la plataforma tenga estado VIGENTE
    *
    * @param platformId
    * @throws RochadeDataAccessException
    * @throws BadRequestException
    */
   private void validatePlatformStatus(String platformId)
         throws RochadeDataAccessException, BadRequestException {
      if (!PLATTFORM_STATUS.equals(rochadeConnector.getPlatformStatus(platformId))) {
         LOGGER.error("La plataforma {} no tiene estado vigente", platformId);
         throw new BadRequestException("InvalidPlatformStatus", "La plataforma " + platformId + " no tiene estado vigente");
      }
   }

   /**
    * Valida que los repositorios no tienen uso
    *
    * @param respositories
    * @throws RochadeDataAccessException
    * @throws BadRequestException
    */
   private void validateRepositoriesExistsAndNotUsed(List<String> repositories)
         throws RochadeDataAccessException, BadRequestException {

	  for (String repository : repositories) {
		  if (!rochadeConnector.repositoryNotUsed(repository)) {
			  LOGGER.error("El repositorio {} se encuentra en uso", repository);
		         throw new BadRequestException("RepositoryAlreadyUsed", "El repositorio " + repository + " ya está asociado a otra Agrupación Técnica");
		  }
	  }
   }

   /**
    * Valida que los repositorios no tienen uso por otra AT
    *
    * @param respositories
    * @throws RochadeDataAccessException
    * @throws BadRequestException
    */
   private void validateRepositoriesNotUsed(List<String> repositories, List<String> currentrepositories)
         throws RochadeDataAccessException, BadRequestException {

	  if (!repositories.equals(currentrepositories)) {
		  for (String repository : repositories) {
			  if (!currentrepositories.contains(repository) && !rochadeConnector.repositoryNotUsed(repository)) {
				  LOGGER.error("El repositorio {} se encuentra en uso", repository);
				  throw new BadRequestException("RepositoryAlreadyUsed", "El repositorio " + repository + " ya está asociado a otra Agrupación Técnica");
			  }
		  }
	  }
   }

   /**
    * Crea en Rochade los repositorios de la Agrupación Técnica
    *
    * @param respositories
    * @throws RochadeDataAccessException
    * @throws BadRequestException
    */
   private void createRepositories(List<String> respositories)
         throws RochadeDataAccessException, BadRequestException {

	  for (String repository : respositories) {
		  if (!rochadeConnector.existRepository(repository)) {
			  rochadeConnector.createRepository(repository);
		  }
	  }
   }

   /**
    * Borra de Rochade los repositorios de la Agrupación Técnica
    *
    * @param respositories
    * @throws RochadeDataAccessException
    * @throws BadRequestException
    */
   private void deleteRepositories(List<String> repositories)
         throws RochadeDataAccessException, BadRequestException {

	  for (String repository : repositories) {
		  if (rochadeConnector.existRepository(repository)) {
			  rochadeConnector.deleteRepository(repository);
		  }
	  }
   }

   /**
    * Valida que se ha introducido algún repositorio
    *
    * @param repositories
    * @throws RochadeDataAccessException
    * @throws BadRequestException
    */
   private void checkDuplicateRepositories(List<String> repositories)
         throws RochadeDataAccessException, BadRequestException {

	   if (repositories.isEmpty()) {
		   LOGGER.error("El campo repositories es obligatorio, no se ha especificado ningún repositorio");
		   throw new BadRequestException("RepositoryMissing", "No se ha especificado ningún repositorio");
	   }

	   for (int i = 0; i < repositories.size(); i++) {
		    for (int j = i+1; j < repositories.size(); j++) {
		        if (repositories.get(i).equals(repositories.get(j))) {
		        	LOGGER.error("Los repositorios introducidos contienen duplicados {} ", repositories);
		        	throw new BadRequestException("DuplicatedRepositories", "Los repositorios introducidos contienen duplicados " + repositories);
		        }
		    }
		}
   }

   /**
    * Valida los parámetros de la operación /technicalGroups
    * @param params
    */
   public void checkParams(@RequestParam MultiValueMap<String,String> params) {
       final Iterator<Entry<String, List<String>>> it = params.entrySet().iterator();
       while(it.hasNext()) {
           final String param = it.next().getKey();
           if (!param.matches(("name|platform|repository"))) {
        	   LOGGER.error("El parámetro {} es incorrecto. Parámetros posibles: name | platform | repository", param);
        	   throw new BadRequestException("InvalidParameter", "El parámetro " + param + " es incorrecto. Parámetros posibles: name | platform | repository");
           }
       }
   }

   /**
    * Comprueba las propiedades del Json
    * @param techGroupInfo
    */
   public void checkJsonPut (TechnicalGroupPut techGroupInfo) {

	   if (null == techGroupInfo.getFunctionalService() && null == techGroupInfo.getSgsCode() && null == techGroupInfo.getRepositories() && null == techGroupInfo.getName() && null == techGroupInfo.getResponsible() && null == techGroupInfo.getDescription() ) {
		   LOGGER.error("El Json enviado no se cumple las validaciones a la operación. Las propiedades obligatorias son name | repositories | responsible | fucntionalService");
		   throw new BadRequestException("ContentMissing", "No se ha especificado ningún atributo a modificar");
	   }
   }

   /**
    * Valida que el servicio funcional exista en rochade
    *
    * @param serviceId
    * @throws RochadeDataAccessException
    * @throws BadRequestException
    */
   private void validateFunctionalServiceExists(String serviceId)
         throws RochadeDataAccessException, BadRequestException {
      if (!rochadeConnector.existFunctionalService(serviceId)) {
         LOGGER.error("El servicio funcional {} no existe", serviceId);
         throw new BadRequestException("FunctionalServiceNotFound",
               "El servicio funcional especificado no existe");
      }
   }

   /**
    * Valida que el usuario conectado tiene permisos para crear una Agrupación Técnica
    * @throws RochadeDataAccessException
    * @throws ForbiddenException
    */
   private void validateConnectedUserCanCreateTechGroup()
         throws RochadeDataAccessException, ForbiddenException {
      if (!rochadeConnector.canCreateTechGroup()) {
         LOGGER.error("El usuario no tiene permisos para crear Agrupaciones técnicas");
         throw new ForbiddenException("NoCreationRights",
               "El usuario no tiene permisos para crear una agrupación técnica asociada al servicio funcional especificado");
      }
   }

   /**
    * Valida que la Agrupación Técnica tiene responsable
    * @param techGroupInfo
    * @return
    */
   private boolean hasResponsible(TechnicalGroupPut techGroupInfo) {
      return techGroupInfo.getResponsible() != null;
   }
   private boolean hasResponsible(TechnicalGroupPost techGroupInfo) {
	      return techGroupInfo.getResponsible() != null;
   }

   private boolean isAuthenticationTechnicalGroupsSuperuser() {
      return authenticationFacade.isUserInRole(ApiRolesConstants.TECH_GROUP_SUPERUSER_ROLE);
   }

   private boolean isAuthenticationTechnicalGroupsRoleModifier() {
	  return authenticationFacade.isUserInRole(ApiRolesConstants.ROLE_MODIFIER);
   }

   /**
    * Comprobando permisos y cambiando member class si es necesario Se comprueba si el responsable pertenece a
    * la Member Class CONSULTA o no tiene Member Class asignada
    *
    * @param userId
    */
   private void checkUserMemberClass(String userId) {
      try {
         UserInfo userInfo = rochadeConnector.getRochadeUserInfo(userId);
         if (userInfo == null) {
            LOGGER.error("No se ha encontrado el usuario {} como usuario en Rochade", userId);
         } else {
            String userMemberClass = userInfo.getApplUserClass();
            if (userMemberClass.isEmpty() || MemberClass.CONSULTA.getValue().equals(userMemberClass)) {

            	if (isAuthenticationTechnicalGroupsRoleModifier()) {
            		rochadeConnector.changeMemberClass(userInfo.getApName(), MemberClass.GESTORES);
            	} else throw new BadRequestException("Error de validación de datos",
                        "El usuario autenticado no tiene asignado el rol " + ApiRolesConstants.ROLE_MODIFIER + " en el API (ARS_TEXTOS)");

               if (userMemberClass.isEmpty()) {
                  LOGGER.info(
                        "El usuario responsable {} no pertenece a ninguna Member Class. Se modifica su Member Class a GESTORES",
                        userId);
               } else {
                  LOGGER.info(
                        "El usuario responsable {} pertenece a la Member Class {}. Se modifica su Member Class a GESTORES",
                        userId, userMemberClass);
               }
            } else {
               LOGGER.info(
                     "El usuario responsable {} pertenece a la Member Class {}. No es necesario modificarla.",
                     userId, userMemberClass);
            }
         }
      } catch (Exception ex) {
         // TODO: Hacer un mejor tratamiento: enviar un correo al administrador, abrir un remedy....
         LOGGER.error(
               "Error al intentar cambiar la Member Class del usuario {} a GESTORES. Se mantiene su antigua Member Class.",
               userId, ex);
      }
   }

   /**
    * Crea una Agrupación Técnica Versión
    * @param techGroupId
    * @param techGroupVersInfo
    * @return
    * @throws ApiException
    */
   public TechnicalGroupVersion createNewTechnicalGroupVersion(String techGroupId, TechnicalGroupVersion techGroupVersInfo) throws ApiException {

	   String version = techGroupVersInfo.getVersion();
       Status status = techGroupVersInfo.getStatus();
       String publishDate = techGroupVersInfo.getPublishDate();
       try {
           LOGGER.info("Realizando validaciones sobre los datos");
           // Comprobamos si existe el estado indicado
           validateStatus(status);
           // Comprobamos la fecha de publicacación si debe tenerla
           validatePublishDate(status, publishDate);
           // Comprobamos si el usuario tiene permisos para crear la agrupación técnica versión
           validateConnectedUserCanCreateTechGroupVersion();
           // Comprobamos si existe la Agrupación Técncia (definición) indicada
           validateTechGroupExists(techGroupId);
           // Comprobamos si ya está creada la Agrupación Técnica Versión
           validateTechGroupVersionDuplicated(techGroupId + " " + version);
           LOGGER.info("Validaciones realizadas correctamente");

           // Dando de alta agrupación técnica
           String newId = rochadeConnector.createTechGroupVers(techGroupId, version, techGroupVersInfo.getDescription(),
                           techGroupVersInfo.getPublishInstance(), status.getId(), statusManager.getPublished(status), statusManager.getValidity(status),
                           publishDate);

           TechnicalGroupVersion resultTechGroupVersion = rochadeConnector.getTechnicalGroupVersionById(newId);
           LOGGER.info("La Agrupación técnica versión {} se ha creado correctamente:\n{}", newId,
                           resultTechGroupVersion);

           return resultTechGroupVersion;
       } catch (ApiException aex) {
           throw aex;
       } catch (Exception ex) {
           LOGGER.error("Error durante la creación de la Agrupación Técnica Versión {} {}", techGroupId, version, ex);
           throw new InternalErrorException("RochadeConnectionRefused",
                 "Se ha producido un error al intentar conectar con el servidor para crear la Versión de la Agrupación Técnica", ex);
       }
   }

   /**
    * Valida que el usuario conectado tiene permisos para crear una ATV
    * @throws RochadeDataAccessException
    */
   private void validateConnectedUserCanCreateTechGroupVersion()
                   throws RochadeDataAccessException {
       if (!rochadeConnector.canCreateTechGroupVersion()) {
           LOGGER.error("El usuario no tiene permisos para crear Agrupaciones técnicas versión");
           throw new ForbiddenException("ForbiddenUser",
                           "El usuario no tiene permisos para crear una versión de la Agrupación Técnica");
       }
   }

   /**
    * Valida que la Agrupación Técnica existe
    * @param techGroupId
    * @throws RochadeDataAccessException
    */
   private void validateTechGroupExists(String techGroupId)
                   throws RochadeDataAccessException {
       if (!rochadeConnector.existTechGroup(techGroupId)) {
           LOGGER.error("La Agrupación técnica {} no existe", techGroupId);
           throw new NotFoundException();
       }
   }

   /**
    * Valida el estado de una Agrupación Técnica
    * @param status
    */
   private void validateStatus(Status status) {
       if (!statusManager.exist(status)) {
           LOGGER.error("El estado {} no existe", status.getId());
           throw new BadRequestException("StatusNotFound", "El estado especificado no existe");
       }
   }

   /**
    * Valida la Fecha de publicación de la AT
    * @param status
    * @param publishDate
    */
   private void validatePublishDate(Status status, String publishDate) {
       if (publishDate != null && !publishDate.trim().isEmpty()) {
           // Se valida que el formato de la fecha sea correcta
           try {
               PUBLISH_DATE_FORMAT.parse(publishDate);
           } catch (ParseException pex) {
               LOGGER.error("La Fecha de publicación {} no tiene un formato correcto", publishDate);
               throw new BadRequestException("WrongDateFormat", "El parámetro 'publishDate' debe tener el formato aaaa-mm-dd");
           }
       } else {
           // Si está publicada, debe tener fecha de publicación
           if ("SI".equals(statusManager.getPublished(status))) {
               LOGGER.error("No se ha indicado fecha de publicación para una Agrupación técnica versión publicada");
               throw new BadRequestException("MissingRequiredParameter", "No se ha especificado el parámetro obligatorio 'publishDate'");
           }
       }
   }

   /**
    * Valida que la ATV no exista
    * @param techGroupVersId
    * @throws RochadeDataAccessException
    */
   private void validateTechGroupVersionDuplicated(String techGroupVersId)
                   throws RochadeDataAccessException {
       if (rochadeConnector.existTechGroupVersion(techGroupVersId)) {
           LOGGER.error("La Agrupación técnica versión {} ya existe", techGroupVersId);
           throw new ConflictException("TechnicalGroupVersionAlreadyExits",
                           "La Versión de la Agrupación Técnica ya existe");
       }

   }

   private boolean validateTechGroupNamePattern(String techGroupName, String regExp) {
      Pattern pat = Pattern.compile(regExp);
      Matcher mat = pat.matcher(techGroupName);
      return mat.matches();
   }

   private TechnicalGroup getTechnicalGroupByName(String techgroupName) throws ApiException {
      LOGGER.info("Realizando consulta de agrupación técnica por nombre {}", techgroupName);
      try {
         return rochadeConnector.getTechnicalGroupById(techgroupName);
      } catch (RochadeDataAccessException rdaex) {
         LOGGER.error("Error en rochade durante la consulta de la Agrupación Técnica {}", techgroupName,
               rdaex);
         throw new InternalErrorException("RochadeConnectionRefused",
               "Se ha producido un error al intentar conectar con el servidor para consultar la Agrupación Técnica",
               rdaex);
      }
   }

   public TechnicalGroup getTechnicalGroupById(String id) throws ApiException {
      return getTechnicalGroupByName(id);
   }

   public List<TechnicalGroup> getTechnicalGroupsByBusinessNamePlatformRepository(String businessName, String platform, String repository) throws ApiException {
       LOGGER.info("Realizando consulta de Agrupaciones técnicas por nombre {}, plataforma {} y repositorio {}", businessName, platform, repository);
       try {
           List<TechnicalGroup> techGroupsList = new ArrayList<>();
           // Obtenemos el id de las agrupaciones técnicas que tiene el businessName, la plataforma indicados y el repositorio indicados
           List<String> itemNameList = rochadeConnector.getTechnicalGroupIdsByBusinessNamePlatformRepository(businessName, platform, repository);
           // Con el id, obtenemos la información de la agrupación técnica
           for (String itemName : itemNameList) {
               techGroupsList.add(getTechnicalGroupByName(itemName));
           }
           return techGroupsList;
       } catch (Exception ex) {
           LOGGER.error("Error en rochade durante la consulta de Agrupaciones Técnicas ");
           throw new InternalErrorException("RochadeConnectionRefused",
                 "Se ha producido un error al intentar conectar con el servidor para consultar la lista de Agrupaciones Técnicas",
                 ex);
        }
   }

}
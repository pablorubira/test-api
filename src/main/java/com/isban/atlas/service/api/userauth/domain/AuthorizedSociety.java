package com.isban.atlas.service.api.userauth.domain;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class AuthorizedSociety {
   @JsonIgnore
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long id;
   @JsonProperty("id")
   private String name;

   @JsonProperty("name")
   private String businessName;

   public AuthorizedSociety() {
      super();
   }

   public AuthorizedSociety(String name) {
      this.name = name;
   }

   /**
   * Get id
   * @return id
   **/
   @ApiModelProperty(example = "94074", readOnly = true, value = "")
   public Long getId() {
      return id;
   }

   /**
   * Get name
   * @return name
   **/
   @ApiModelProperty(example = "Isban Global", readOnly = true, value = "")
   public String getName() {
      return name;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getBusinessName() {
      return businessName;
   }

   public void setBusinessName(String bussinesName) {
      this.businessName = bussinesName;
   }

   @Override
   public String toString() {
      return "AuthorizedSociety [id=" + id + ", name=" + name + ", businessName=" + businessName + "]";
   }

}
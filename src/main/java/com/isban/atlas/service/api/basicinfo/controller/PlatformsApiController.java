package com.isban.atlas.service.api.basicinfo.controller;


import io.swagger.annotations.ApiParam;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.isban.atlas.service.api.basicinfo.PlatformsApi;
import com.isban.atlas.service.api.common.domain.Platform;
import com.isban.atlas.service.repository.rochade.PlatformsRochadeRepository;

import es.isb.atlas.rochade.data.access.exception.RochadeDataAccessException;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2018-06-06T16:17:21.653+02:00")

@RestController
public class PlatformsApiController implements PlatformsApi {

    private static final Logger logger = LoggerFactory.getLogger(PlatformsApiController.class);

    @Autowired
    private PlatformsRochadeRepository rochadeConnector;


    public ResponseEntity<List<Platform>> platformsGet( @NotNull@ApiParam(value = "origin id of consumer", required = true) @RequestParam(value = "origin", required = true) String origin,
        @ApiParam(value = "name to filter by (accept * as wildcards)") @RequestParam(value = "name", required = false) String name, @ApiParam(value="Al parameters", hidden = true )  @RequestParam MultiValueMap<String,String> requestParams) throws RochadeDataAccessException  {
    	rochadeConnector.checkParams(requestParams);
    	List<Platform> layerListResult = rochadeConnector.getPlatformByFilters(origin, name);
        if (layerListResult.isEmpty()) {
        	logger.info("No se ha encontrado ninguna Plataforma técnica con origen {} y nombre {}", origin, name);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
        	logger.info("Listado de Plataformas encontradas de acuerdo a los filtros");
            return new ResponseEntity<>(layerListResult, HttpStatus.OK);
        }

    }

}
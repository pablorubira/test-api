package com.isban.atlas.service.api.techgroup.domain;

import java.util.List;
import java.util.Objects;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import io.swagger.annotations.ApiModelProperty;

/**
 * TechnicalGroupCommon
 */
@JsonPropertyOrder({ "id" })
public class TechnicalGroupCommon {

   @JsonProperty("id")
   private String id = null;

   @JsonProperty("name")
   private String name = null;

   @JsonProperty("description")
   private String description = null;

   @JsonProperty("sgsCode")
   private String sgsCode = null;

   @JsonProperty("repositories")
   private List<String> repositories = null;

   @JsonProperty("responsible")
   private User responsible = null;

   @JsonProperty("functionalService")
   private FunctionalService functionalService = null;


   /**
    * Get id
    *
    * @return id
    **/
   public TechnicalGroupCommon id(String id) {
	   this.id = id;
	   return this;
   }

   @ApiModelProperty(example = "93742171-2b1c-4f51-beb2-b3e88be50421", readOnly = true, value = "")
   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   /**
    * Get name
    *
    * @return name
    **/
   public TechnicalGroupCommon name(String name) {
	   this.name = name;
	   return this;
   }

   @ApiModelProperty(example = "BDPGPO_SCH", value = "")
   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   /**
    * Get description
    *
    * @return description
    **/
   public TechnicalGroupCommon description(String description) {
	   this.description = description;
	   return this;
   }

   @ApiModelProperty(example = "Lorem ipsum dolor sit amet, consectetur adipiscing elit...", value = "")
   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   /**
    * Get sgsCode
    *
    * @return sgsCode
    **/
   public TechnicalGroupCommon sgsCode(String sgsCode) {
	   this.sgsCode = sgsCode;
	   return this;
   }

   @ApiModelProperty(example = "BDPGPO_SCH", readOnly = true, value = "")
   public String getSgsCode() {
      return sgsCode;
   }

   public void setSgsCode(String sgsCode) {
      this.sgsCode = sgsCode;
   }

   /**
    * Get repositories
    *
    * @return repositories
    **/
   public TechnicalGroupCommon repositories(List<String> repositories) {
	   this.repositories = repositories;
	   return this;
   }

   @ApiModelProperty(example = "[\"https://github.com/foo/Foo\",\"https://github.com/bar/Bar\"]", value = "")
   public List<String> getRepositories() {
	   return repositories;
   }

   public void setRepositories(List<String> repositories) {
	   this.repositories = repositories;
   }

   /**
    * Get responsible
    *
    * @return responsible
    **/
   public TechnicalGroupCommon responsible(User responsible) {
	   this.responsible = responsible;
	   return this;
   }

   @ApiModelProperty(value = "")
   @Valid
   public User getResponsible() {
      return responsible;
   }

   public void setResponsible(User responsible) {
      this.responsible = responsible;
   }

   /**
    * Get functionalService
    *
    * @return functionalService
    **/
   public TechnicalGroupCommon functionalService(FunctionalService functionalService) {
	   this.functionalService = functionalService;
	   return this;
   }

   @ApiModelProperty(value = "")
   @Valid
   public FunctionalService getFunctionalService() {
      return functionalService;
   }

   public void setFunctionalService(FunctionalService functionalService) {
      this.functionalService = functionalService;
   }

   @Override
   public boolean equals(java.lang.Object o) {
      if (this == o) {
         return true;
      }
      if (o == null || getClass() != o.getClass()) {
         return false;
      }
      TechnicalGroupCommon technicalGroup = (TechnicalGroupCommon) o;
      return Objects.equals(this.id, technicalGroup.id)
    		&& Objects.equals(this.name, technicalGroup.name)
            && Objects.equals(this.description, technicalGroup.description)
            && Objects.equals(this.sgsCode, technicalGroup.sgsCode)
            && Objects.equals(this.repositories, technicalGroup.repositories)
            && Objects.equals(this.responsible, technicalGroup.responsible)
            && Objects.equals(this.functionalService, technicalGroup.functionalService);
   }

   @Override
   public int hashCode() {
      return Objects.hash(id, name, description, sgsCode, repositories, responsible, functionalService);
   }

   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append("class TechnicalGroupCommon {\n");
      sb.append("    id: ").append(toIndentedString(getId())).append("\n");
      sb.append("    name: ").append(toIndentedString(name)).append("\n");
      sb.append("    description: ").append(toIndentedString(description)).append("\n");
      sb.append("    sgsCode: ").append(toIndentedString(sgsCode)).append("\n");
      sb.append("    repositories: ").append(toIndentedString(repositories)).append("\n");
      sb.append("    responsible: ").append(toIndentedString(responsible)).append("\n");
      sb.append("    functionalService: ").append(toIndentedString(functionalService)).append("\n");
      sb.append("}");
      return sb.toString();
   }

   /**
    * Convert the given object to string with each line indented by 4 spaces (except the first line).
    */
   private String toIndentedString(java.lang.Object o) {
      if (o == null) {
         return "null";
      }
      return o.toString().replace("\n", "\n    ");
   }

}
package com.isban.atlas.service.api.techgroup.domain;

import org.springframework.stereotype.Component;

@Component
public class PublicationStatusManager {

    /**
     * Valores válidos para el Status y sus correspondencias con los campos de Rochade
     */
    private enum PublicationStatusValue {

        ALTA                   ("", "VIGENTE"),
        BETA                   ("", "BETA"),
        NO_PUBLICADA           ("", "VIGENTE"),
        PUBLICADA              ("SI", "VIGENTE"),
        RETIRADA_INVENTARIO    ("SI", "RETIRADA INVENTARIO"),
        RETIRADA_NO_BLOQUEANTE ("SI", "RETIRADA NO BLOQUEANTE"),
        RETIRADA_BLOQUEANTE    ("SI", "RETIRADA BLOQUEANTE");


        // Valor para el campo Publicado de Rochade
        private String published;
        // Valor para el campo Vigente de rochade
        private String validity;


        private PublicationStatusValue(String published, String validity) {
            this.published = published;
            this.validity = validity;
        }

        public String getPublished() {
            return published;
        }


        public String getValidity() {
            return validity;
        }
    }

    /**
     * Indica si existe un Status con el nombre indicado
     * @param name
     * @return
     */
    public boolean exist (String name) {
        try {
            @SuppressWarnings("unused")
            PublicationStatusValue value = PublicationStatusValue.valueOf(fromText(name));
        } catch (IllegalArgumentException | NullPointerException ex) {
            return false;
        }
        return true;
    }

    /**
     * Indica si el Status indicado existe
     * @param status
     * @return
     */
    public boolean exist (Status status) {
        return exist(status.getId());
    }

    /**
     * Obtiene el valor de publicado para el status con el nombre indicado
     * @param name
     * @return
     */
    public String getPublished (String name) {
        PublicationStatusValue value = PublicationStatusValue.valueOf(fromText(name));
        return value.getPublished();
    }

    /**
     * Obtiene el valor de publicado para el status indicado
     * @param status
     * @return
     */
    public String getPublished (Status status) {
        return getPublished(status.getId());
    }

    /**
     * Obtiene el valor de vigente para el status con el nombre indicado
     * @param name
     * @return
     */
    public String getValidity (String name) {
        PublicationStatusValue value = PublicationStatusValue.valueOf(fromText(name));
        return value.getValidity();
    }

    /**
     * Obtiene el valor de vigente para el status indicado
     * @param name
     * @return
     */
    public String getValidity (Status status) {
        return getValidity(status.getId());
    }


    static String toText (String name) {
        return name.replace("_", " ");
    }

    static String fromText (String value) {
        return value.replace(" ", "_");
    }

}

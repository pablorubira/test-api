package com.isban.atlas.service.api.common.domain;

import java.util.Objects;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * Platform
 */
public class Platform {
   @NotBlank(message = "El campo id de la plataforma debe existir y no estar vacio")
   @JsonProperty("id")
   private String id = null;

   @JsonProperty("name")
   private String name = null;

   public Platform id(String id) {
      this.id = id;
      return this;
   }

   public Platform (String id) {
	  this.id = id;
   }

   public Platform () {}

   /**
    * Get id
    *
    * @return id
    **/
   @ApiModelProperty(example = "DRUPAL", value = "")

   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public Platform name(String name) {
      this.name = name;
      return this;
   }

   /**
    * Get name
    *
    * @return name
    **/
   @ApiModelProperty(example = "Drupal", readOnly = true, value = "")
   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   @Override
   public boolean equals(java.lang.Object o) {
      if (this == o) {
         return true;
      }
      if (o == null || getClass() != o.getClass()) {
         return false;
      }
      Platform platform = (Platform) o;
      return Objects.equals(this.id, platform.id) && Objects.equals(this.name, platform.name);
   }

   @Override
   public int hashCode() {
      return Objects.hash(id, name);
   }

   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append("class Platform {\n");

      sb.append("    id: ").append(toIndentedString(id)).append("\n");
      sb.append("    name: ").append(toIndentedString(name)).append("\n");
      sb.append("}");
      return sb.toString();
   }

   /**
    * Convert the given object to string with each line indented by 4 spaces (except the first line).
    */
   private String toIndentedString(java.lang.Object o) {
      if (o == null) {
         return "null";
      }
      return o.toString().replace("\n", "\n    ");
   }
}
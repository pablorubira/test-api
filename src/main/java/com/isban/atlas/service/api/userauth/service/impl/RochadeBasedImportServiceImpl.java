package com.isban.atlas.service.api.userauth.service.impl;

import java.util.Collection;

import javax.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.isban.atlas.service.api.userauth.domain.Society;
import com.isban.atlas.service.api.userauth.domain.UserAuthorization;
import com.isban.atlas.service.api.userauth.service.ImportService;
import com.isban.atlas.service.exceptions.ApiException;
import com.isban.atlas.service.repository.jpa.SocietyRepository;
import com.isban.atlas.service.repository.jpa.UserAuthorizationRepository;
import com.isban.atlas.service.repository.rochade.UserApiRolesRochadeRepository;
import com.isban.atlas.service.repository.rochade.UserAuthorizationRochadeRepository;

import es.isb.atlas.rochade.data.access.exception.RochadeDataAccessException;

@Service(value = "rochadeBasedImportService")
public class RochadeBasedImportServiceImpl implements ImportService {

   private static final Logger LOGGER = LoggerFactory.getLogger(RochadeBasedImportServiceImpl.class);

   private EntityManager entityManager;
   private SocietyRepository societyRepository;
   private UserAuthorizationRepository userRepository;
   private UserAuthorizationRochadeRepository userAuthorizationRochadeRepository;
   private UserApiRolesRochadeRepository userApiRolesRochadeRepository;

   @Autowired
   public RochadeBasedImportServiceImpl(EntityManager entityManager, SocietyRepository societyRepository,
         UserAuthorizationRepository userRepository,
         UserAuthorizationRochadeRepository userAuthorizationRochadeRepository,
         UserApiRolesRochadeRepository userApiRolesRochadeRepository) {
      this.societyRepository = societyRepository;
      this.userRepository = userRepository;
      this.userAuthorizationRochadeRepository = userAuthorizationRochadeRepository;
      this.userApiRolesRochadeRepository = userApiRolesRochadeRepository;
      this.entityManager = entityManager;
   }

   // @Transactional
   public void clearDatabase() {
      LOGGER.info("Se borran los datos previos de la base de datos local");
      societyRepository.deleteAll();
      userRepository.deleteAll();
      entityManager.flush();
      entityManager.clear();
      LOGGER.info("Fin borrado datos previos");
   }

   // @Transactional
   public void load() throws ApiException {
      try {
         Collection<Society> societiesFromRochade = userAuthorizationRochadeRepository.getSocieties();
         LOGGER.info("Guardando informacion procedente de rochade en base de datos local");
         societyRepository.save(societiesFromRochade);
         LOGGER.info(
               "Guardando informacion sobre roles de consulta procedente de rochade en base de datos local");
         loadUserWithQueryRole();
         LOGGER.info("Fin load RochadeBasedImportServiceImpl ");
      } catch (RochadeDataAccessException e) {
         throw new ApiException("Internal Error", "Error while reloading authorizations from Rochade", e);
      }
   }

   private void loadUserWithQueryRole() throws RochadeDataAccessException, ApiException {
      Collection<UserAuthorization> usersGrantAuthorities = userApiRolesRochadeRepository.getUsers();
      for (UserAuthorization user : usersGrantAuthorities) {
         UserAuthorization existingsUser = userRepository.findByName(user.getName());
         if (existingsUser != null) {
            for (String role : user.getRoles()) {
               existingsUser.addRole(role);
            }
         } else {
            userRepository.save(user);
         }
      }
   }

   @Transactional
   public void reload() throws ApiException {
      clearDatabase();
      load();
   }
}

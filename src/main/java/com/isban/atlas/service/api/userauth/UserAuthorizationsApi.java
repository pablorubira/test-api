package com.isban.atlas.service.api.userauth;

import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.isban.atlas.service.api.Error;
import com.isban.atlas.service.api.userauth.domain.Area;
import com.isban.atlas.service.api.userauth.domain.UserAuthorization;
import com.isban.atlas.service.exceptions.ApiException;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Authorization;

@Api(value = "userAuthorizations", tags = { "UserAuthorizations" })
@RequestMapping(value = "/userAuthorizations")
public interface UserAuthorizationsApi {

   @ApiOperation(value = "Gets Users info paginated", response = UserAuthorization.class, authorizations = {
   @Authorization(value = "basicAuth") }, tags = { "UserAuthorizations" })
   @ApiResponses(value = {
		 @ApiResponse(code = 200, message = "Success", response = Page.class),
         @ApiResponse(code = 400, message = "Bad request (errors on path parameters)", response = Error.class),
         @ApiResponse(code = 401, message = "Unauthorized (user / password not valid)", response = Error.class),
         @ApiResponse(code = 404, message = "User not found", response = Void.class),
         @ApiResponse(code = 500, message = "Internal Server Error", response = Error.class) })
   @GetMapping(produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
   public Page<UserAuthorization> readAll(@RequestParam(value = "page", defaultValue = "0") int page,
         @RequestParam(value = "size", defaultValue = "10") int size);

   @ApiOperation(value = "Gets user and its authorized clientes by id cataloged in ATLAS", notes = "", response = UserAuthorization.class, authorizations = {
   @Authorization(value = "basicAuth") }, tags = { "UserAuthorizations", })
   @ApiResponses(value = {
		 @ApiResponse(code = 200, message = "Success", response = UserAuthorization.class),
         @ApiResponse(code = 400, message = "Bad request (errors on path parameters)", response = Error.class),
         @ApiResponse(code = 401, message = "Unauthorized (user / password not valid)", response = Error.class),
         @ApiResponse(code = 404, message = "UserAuthorization not found", response = Void.class),
         @ApiResponse(code = 500, message = "Internal Server Error", response = Error.class),
         @ApiResponse(code = 200, message = "Unexpected Error", response = Error.class) })

   @GetMapping(value = "/{id}", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
   public UserAuthorization userAuthorizationsIdGet(
         @ApiParam(value = "Id of the user to be fetched", required = true) @PathVariable("id") String id)
         throws ApiException;

   @ApiOperation(value = "Gets user and its area by id cataloged in ATLAS", notes = "", response = Area.class, authorizations = {
   @Authorization(value = "basicAuth") }, tags = { "UserAuthorizations", })
   @ApiResponses(value = {
		@ApiResponse(code = 200, message = "Success", response = Area.class),
		@ApiResponse(code = 400, message = "Bad request (errors on path parameters)", response = Error.class),
		@ApiResponse(code = 401, message = "Unauthorized (user / password not valid)", response = Error.class),
		@ApiResponse(code = 404, message = "UserAuthorization not found", response = Void.class),
		@ApiResponse(code = 500, message = "Internal Server Error", response = Error.class),
		@ApiResponse(code = 200, message = "Unexpected Error", response = Error.class) })

   @GetMapping(value = "/userArea/{id}", produces = { MediaType.APPLICATION_JSON_UTF8_VALUE })
   public Area userAuthorizationsIdAreaGet(
		@ApiParam(value = "Id of the user to be fetched", required = true) @PathVariable("id") String id)
	    throws ApiException;

}
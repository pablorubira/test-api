
package com.isban.atlas.service.api.userauth.controller;

import javax.persistence.PessimisticLockException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import com.isban.atlas.service.api.userauth.AuthorizationsApi;
import com.isban.atlas.service.api.userauth.service.ImportService;
import com.isban.atlas.service.exceptions.ApiException;
import com.isban.atlas.service.exceptions.LockResourceException;

@RestController
public class AuthorizationsApiController implements AuthorizationsApi {
   private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizationsApiController.class);

   private ImportService service;

   @Autowired
   public AuthorizationsApiController(@Qualifier("rochadeBasedImportService") ImportService service) {
      this.service = service;
   }

   public ResponseEntity<Void> authorizationsDelete() throws ApiException {
      try {
         LOGGER.info("Recibida petición de recarga de los datos de autorizacion de usuarios");
         service.reload();
         LOGGER.info("Recarga finalizada con éxito");
         return new ResponseEntity<>(HttpStatus.NO_CONTENT);
      } catch (PessimisticLockException e) {
         LOGGER.warn(
               "No se ha podido realizar la peticion porque el recurso esta bloqueado por otra peticion anterior");
         LOGGER.warn(
               "El proceso de recarga de la base de datos en una trasacion solo se puede ejecutar por una unica peticion concurrente");
         throw new LockResourceException("Error Petición Concurrente",
               "No se ha podido realizar el proceso de recarga porque el recurso esta bloqueado por otra peticion anterior",
               e);
      }
   }
}

package com.isban.atlas.service.utils;

public class IDGenerator {

   public static String getRandomId() {
      return java.util.UUID.randomUUID().toString();
   }

   private IDGenerator() {
   }

}

package com.isban.atlas.service.auth;

import org.springframework.security.core.Authentication;

public interface IAuthenticationFacade {

   Authentication getAuthentication();

   boolean isUserInRole(String role);
}

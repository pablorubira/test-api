package com.isban.atlas.service.auth;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationFacade implements IAuthenticationFacade {

   @Override
   public Authentication getAuthentication() {
      return SecurityContextHolder.getContext().getAuthentication();
   }

   @Override
   public boolean isUserInRole(String role) {
      Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
      for (GrantedAuthority auth : authentication.getAuthorities()) {
         if (role.equals(auth.getAuthority()))
            return true;
      }
      return false;
   }
}

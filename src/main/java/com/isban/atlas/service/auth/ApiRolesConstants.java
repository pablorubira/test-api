package com.isban.atlas.service.auth;

public class ApiRolesConstants {

   public static final String TECH_GROUP_SUPERUSER_ROLE = "TECH_GROUP_SUPER";
   public static final String AUTHORITIES_CONSULTA_ROLE = "AUTH_CONSULTA";
   public static final String ROLE_MODIFIER = "ROLE_MOD";

}

package com.isban.atlas.service;

import java.nio.file.Paths;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class AtlasApiApplication extends SpringBootServletInitializer {

   private static final String ATLAS_APIS_LOG_FILE_NAME = "atlas-apis.log";

   private static final Logger LOGGER_MAIN = LoggerFactory.getLogger(AtlasApiApplication.class);

   private static final String ATLASAPI_ENV_FILE_NAME = "atlasapi_env";
   private static final String CONFIG_PATH = "atlas.env";
   private static final String LOG_BASE_PATH = "atlas.conf";

   public static void main(String[] args) {
      configureApplication(new SpringApplicationBuilder()).run(args);
   }

   @Override
   protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
      return configureApplication(builder);
   }

   private static SpringApplicationBuilder configureApplication(SpringApplicationBuilder builder) {
      return builder.sources(AtlasApiApplication.class).properties(getAtlasProperties());
   }

   static Properties getAtlasProperties() {
      Properties props = new Properties();
      String logBasePath = System.getProperty(LOG_BASE_PATH);
      if (logBasePath != null && !logBasePath.isEmpty()) {
         props.put("logging.file", Paths.get(logBasePath).resolve(ATLAS_APIS_LOG_FILE_NAME).toString());
      }
      String configBasePath = System.getProperty(CONFIG_PATH);
      if (configBasePath != null && !configBasePath.isEmpty()) {
         props.put("spring.config.location", "file:"
               + Paths.get(configBasePath).resolve(getAsPropertiesFile(ATLASAPI_ENV_FILE_NAME)).toString());
      } else {
         LOGGER_MAIN.info(
               "No se ha establecido la propiedad atlas.env indicando la ruta de configuracion del entorno. Se intenta detectar en el claspath");
         Resource resource = new ClassPathResource(getAsPropertiesFile(ATLASAPI_ENV_FILE_NAME));
         if (!resource.exists()) {
            throw new IllegalArgumentException(
                  "No se ha podido detectar la configuración  atlasapi_env.properties de la aplicacion ni desde la propiedad de entorno atlas.env ni en el classpath");
         } else {
            props.put("spring.config.name", ATLASAPI_ENV_FILE_NAME);
         }
      }
      return props;
   }

   private static String getAsPropertiesFile(String fileName) {
      return fileName + ".properties";
   }
}

package com.isban.atlas.service.repository.rochade;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.isban.atlas.service.api.common.domain.Platform;
import com.isban.atlas.service.api.techgroup.domain.FunctionalService;
import com.isban.atlas.service.api.techgroup.domain.Status;
import com.isban.atlas.service.api.techgroup.domain.TechnicalGroup;
import com.isban.atlas.service.api.techgroup.domain.TechnicalGroupVersion;
import com.isban.atlas.service.api.techgroup.domain.User;
import com.isban.atlas.service.repository.rochade.RochadeConstants.Attributes;
import com.isban.atlas.service.repository.rochade.RochadeConstants.Links;
import com.isban.atlas.service.repository.rochade.RochadeConstants.MemberClass;
import com.isban.atlas.service.repository.rochade.RochadeConstants.Types;
import com.isban.atlas.service.utils.IDGenerator;

import de.rochade.ap.Application.UserInfo;
import de.rochade.srap.SrapException;
import es.isb.atlas.rochade.data.access.ReportPathStep;
import es.isb.atlas.rochade.data.access.RochadeSessionTools;
import es.isb.atlas.rochade.data.access.exception.RochadeDataAccessException;
import es.isb.atlas.rochade.data.access.model.LinkAttribute;
import es.isb.atlas.rochade.data.access.model.LinkData;
import es.isb.atlas.rochade.data.access.model.RochadeItemData;
import es.isb.atlas.rochade.data.access.model.RochadeItemData.CommonAtt;
import es.isb.atlas.rochade.data.access.model.TextAttribute;
import es.isb.atlas.rochade.data.access.util.RochadeConstants.Permissions;

@Service
public class TechnicalGroupRochadeRepository extends AbstractRochadeRepository {

   @Autowired
   private RochadeSessionHolder rochadeSessionHolder;

   @Autowired
   public TechnicalGroupRochadeRepository(RochadeSessionHolder rochadeHolder,
         @Value("${sa.work.type}") final char type, @Value("${sa.work.name}") final String name,
         @Value("${sa.work.version}") final String version) {
      super(rochadeHolder, type, name, version);
   }

   /**
    * Comprueba si existe el servicio versión V00R00 correspondiente al nombre de servicio indicado
    *
    * @param serviceName
    * @return
    * @throws RochadeDataAccessException
    */
   public boolean existFunctionalService(String serviceName) throws RochadeDataAccessException {
      return existItem(Types.SERVICIO_V, getServiceVersionName(serviceName));
   }

   /**
    * Comprueba si existe la plataforma indicada
    *
    * @param platformName
    * @return
    * @throws RochadeDataAccessException
    */
   public boolean existPlatform(String platformName) throws RochadeDataAccessException {
      return existItem(Types.TECNOLOGIA, platformName);
   }

   /**
    * Devuelve el estado de la plataforma indicada
    *
    * @param platformName
    * @return
    * @throws RochadeDataAccessException
    */
   public String getPlatformStatus(String platformName) throws RochadeDataAccessException {
       return getTextAttribute(Types.TECNOLOGIA, platformName, Attributes.VIGENTE);
   }

   /**
    * Comprueba si existe el repositorio indicado
    *
    * @param repository
    * @return
    * @throws RochadeDataAccessException
    */
   public boolean existRepository(String repository) throws RochadeDataAccessException {
      return existItem(Types.NEXUS_REPOSITORY, repository);
   }

   /**
    * Comprueba si un repositorio está activo
    *
    * @param repository
    * @return
    * @throws RochadeDataAccessException
    */
   public boolean isActiveRepository(String repository) throws RochadeDataAccessException {
      return isActiveItem(Types.NEXUS_REPOSITORY, repository);
   }

   /**
    * Comprueba si existe el repositorio indicado
    *
    * @param repository
    * @return
    * @throws RochadeDataAccessException
    */
   public boolean repositoryNotUsed(String repository) throws RochadeDataAccessException {
      return referenceItem(Types.NEXUS_REPOSITORY, repository);
   }

   /**
    * Crea un repositorio en Rochade
    * @param repository
    * @throws RochadeDataAccessException
    */
   public void createRepository(String repository) throws RochadeDataAccessException {
       createItem(Types.NEXUS_REPOSITORY, repository);
   }

   /**
    * Borra un repositorio en Rochade
    * @param repository
    * @throws RochadeDataAccessException
    * @throws SrapException
    */
   public void deleteRepository(String repository) throws RochadeDataAccessException {
       deleteItem(Types.NEXUS_REPOSITORY, repository);
   }

   /**
    * Devuelve el Origen de la Plataforma
    * @param platformId
    * @return
    * @throws RochadeDataAccessException
    */
   public String getSourceFromPlatform(String platformId) throws RochadeDataAccessException {
       return getLinkAttribute(Types.TECNOLOGIA, platformId, Links.TECNOLOGY_SOURCE);
   }

   /**
    * Comprueba si existe el usuario indicado, tanto en la subject area como el item usuario
    *
    * @param userId
    * @return
    * @throws RochadeDataAccessException
    */
   public boolean existUser(String userId) throws RochadeDataAccessException {
      if (existItem(Types.USUARIO, userId.toUpperCase())) {
         return getAutoPilotSession().existUser(appName, userId);
      } else {
         return false;
      }
   }

   /**
    * Comprueba si existe la agrupación técnica indicada
    *
    * @param techGroupName
    * @return
    * @throws RochadeDataAccessException
    */
   public boolean existTechGroup(String techGroupName) throws RochadeDataAccessException {
      return existItem(Types.AGRUP_TEC_D, techGroupName);
   }

   /**
    * Comprueba si existe la agrupación técnica indicada
    *
    * @param techGroupName
    * @return
    * @throws RochadeDataAccessException
    */
   public boolean existTechGroupVersion(String techGroupVersionName) throws RochadeDataAccessException {
      return existItem(Types.AGRUP_TEC_V, techGroupVersionName);
   }

   /**
    * Obtiene el nombre del Servicio Funcional indicado
    *
    * @param platformName
    * @return
    * @throws RochadeDataAccessException
    */
   public String getFunctionalServiceName(String functionalServiceId) throws RochadeDataAccessException {
      String bName = getTextAttribute(Types.SERVICIO_D, functionalServiceId, CommonAtt.BUSINESS_NAME);
      String prefix = functionalServiceId + " ";
      if (bName.startsWith(prefix)) {
         return bName.substring(prefix.length());
      } else {
         return bName;
      }
   }

   /**
    *
    * @param platformId
    * @return
    * @throws RochadeDataAccessException
    */
   public String getPlatformName(String platformId) throws RochadeDataAccessException {
      return getTextAttribute(Types.TECNOLOGIA, platformId, CommonAtt.BUSINESS_NAME);
   }

   /**
    * Obtiene la expresión regular para las Agrupaciones técnicas para la plataforma indicada
    *
    * @param platformName
    * @return
    * @throws RochadeDataAccessException
    */
   public String getPlatformTechGroupPattern(String platformName) throws RochadeDataAccessException {
      return getTextAttribute(Types.TECNOLOGIA, platformName, Attributes.EXPRESION_REGULAR_AT);
   }

   public User getUserInfo(String userId) throws RochadeDataAccessException {
      User userInfo = new User();
      userInfo.setId(userId);

      if (userId != null) {
         ReportPathStep[] steps = new ReportPathStep[1];
         steps[0] = new ReportPathStep(Links.USUARIO__DEPARTAM); // Usuario --> Departamento
         // Nivel 0: Usuario
         // Nivel 1: Geografía
         List<RochadeItemData> data = this.getSession().executeReportPath(appName, Types.USUARIO,
               userId.toUpperCase(), steps, CommonAtt.NAME, CommonAtt.TYPE, CommonAtt.BUSINESS_NAME,
               Attributes.NOMBRE, Attributes.EMAIL, Links.USUARIO__DEPARTAM);

         for (RochadeItemData item : data) {
            switch (item.getType()) {
            case Types.USUARIO:
               userInfo.setEmail(item.getTextAttValue(Attributes.EMAIL));
               userInfo.setName(item.getTextAttValue(Attributes.NOMBRE));
               break;
            case Types.DEPARTAMENTO:
               userInfo.setDepartment(item.getBusinessName());
               break;
            default:
               throw new RochadeDataAccessException(
                     "Tipo no soportado en consulta getUsers" + item.getType());
            }
         }
      }
      return userInfo;

   }

   /**
    * Obtiene la geografía a la que pertenece un servicio funcional
    *
    * @param serviceName
    *           Nombre del servicio
    * @return Nombre de la geografía a la que pertenece el servicio o vacío si no pertenece a ninguna
    * @throws RochadeDataAccessException
    */
   public String getFunctionalServiceGeography(String serviceName) throws RochadeDataAccessException {
      if (serviceName != null) {
         ReportPathStep[] steps = new ReportPathStep[3];
         steps[0] = new ReportPathStep(Links.APLICV__SERVV); // Servicio versión <-- Aplicación versión
         steps[0].setDirection(ReportPathStep.DIRECTION_USAGE);
         steps[0].hide();
         steps[1] = new ReportPathStep(Links.APLICV__LABRESP); // Aplicación versión --> Departamento
         steps[1].hide();
         steps[2] = new ReportPathStep(Links.DEPARTAMENTO__GEO); // Departamento --> Geografía
         // Nivel 0: Servicio funcional versión
         // Nivel 1: Geografía

         List<RochadeItemData> data = this.getSession().executeReportPath(appName, Types.SERVICIO_V,
               getServiceVersionName(serviceName), steps, CommonAtt.TYPE, CommonAtt.NAME);
         for (RochadeItemData item : data) {
            if (Types.GEOGRAFIA.equals(item.getType())) {
               return item.getName();
            }
         }
      }

      return "";
   }


   /**
    * Obtiene la Sociedad a la que pertenece un servicio funcional
    *
    * @param serviceName
    *           Nombre del servicio
    * @return Nombre de la sociedad a la que pertenece el servicio o vacío si no pertenece a ninguna
    * @throws RochadeDataAccessException
    */
   public String getFunctionalServiceSociety(String serviceName) throws RochadeDataAccessException {
      if (serviceName != null) {
         ReportPathStep[] steps = new ReportPathStep[4];
         steps[0] = new ReportPathStep(Links.APLICV__SERVV); // Servicio versión <-- Aplicación versión
         steps[0].setDirection(ReportPathStep.DIRECTION_USAGE);
         steps[0].hide();
         steps[1] = new ReportPathStep(Links.APLICV__LABRESP); // Aplicación versión --> Departamento
         steps[1].hide();
         steps[2] = new ReportPathStep(Links.DEPARTAMENTO__AREA); // Departamento --> Area
         steps[2].hide();
         steps[3] = new ReportPathStep(Links.SOCIEDAD_AREA); // Area <-- Sociedad
         steps[3].setDirection(ReportPathStep.DIRECTION_USAGE);
         // Nivel 0: Servicio funcional versión
         // Nivel 1: Aplicación funcional versión
         // Nivel 2: Departamento
         // Nivel 3: Area
         // Nivel 4: Sociedad

         List<RochadeItemData> data = this.getSession().executeReportPath(appName, Types.SERVICIO_V,
               getServiceVersionName(serviceName), steps, CommonAtt.TYPE, CommonAtt.NAME);
         for (RochadeItemData item : data) {
            if (Types.SOCIEDAD.equals(item.getType())) {
               return item.getName();
            }
         }
      }

      return "";
   }

   /**
    * Obtiene la geografía a la que pertenece un usuario
    *
    * @param userId
    *           Indentificador del usuario
    * @returnNombre de la geografía a la que pertenece el usuario o vacío si no pertenece a ninguna
    * @throws RochadeDataAccessException
    */
   public String getUserGeography(String userId) throws RochadeDataAccessException {
      if (userId != null) {
         ReportPathStep[] steps = new ReportPathStep[2];
         steps[0] = new ReportPathStep(Links.USUARIO__DEPARTAM); // Usuario --> Departamento
         steps[0].hide();
         steps[1] = new ReportPathStep(Links.DEPARTAMENTO__GEO); // Departamento --> Geografía
         // Nivel 0: Usuario
         // Nivel 1: Geografía

         List<RochadeItemData> data = this.getSession().executeReportPath(appName, Types.USUARIO,
               userId.toUpperCase(), steps, CommonAtt.TYPE, CommonAtt.NAME);

         for (RochadeItemData item : data) {
            if (Types.GEOGRAFIA.equals(item.getType())) {
               return item.getName();
            }
         }
      }
      return "";
   }

   /**
    * Obtiene el Gestor de la aplicación versión del servicio
    * @param serviceId
    * @return
    * @throws RochadeDataAccessException
    */
   public String getManagerApplication(String serviceId) throws RochadeDataAccessException {

       if (serviceId != null) {
             ReportPathStep[] steps = new ReportPathStep[2];
             steps[0] = new ReportPathStep(Links.APLICV__SERVV); // Servicio versión <-- Aplicación versión
             steps[0].setDirection(ReportPathStep.DIRECTION_USAGE);
             steps[0].hide();
             steps[1] = new ReportPathStep(Links.APLICV__USURESP); // Aplicación versión --> Gestor
             // Nivel 0: Servicio funcional versión
             // Nivel 1: Aplicación funcional versión
             // Nivel 2: Gestor

             List<RochadeItemData> data = this.getSession().executeReportPath(appName, Types.SERVICIO_V,
                   getServiceVersionName(serviceId), steps, CommonAtt.TYPE, Attributes.ID_USUARIO, CommonAtt.ID, CommonAtt.NAME);
             for (RochadeItemData item : data) {
                if (null != item && Types.USUARIO.equals(item.getType())) {
                   return item.getName();
                }
             }
          }
      return "";
   }

   /**
    * Obtiene la sociedad a la que pertenece un usuario
    *
    * @param userId
    *           Indentificador del usuario
    * @returnNombre de la sociedad a la que pertenece el usuario o vacío si no pertenece a ninguna
    * @throws RochadeDataAccessException
    */
   public String getUserSociety(String userId) throws RochadeDataAccessException {
      if (userId != null && !userId.isEmpty()) {
         ReportPathStep[] steps = new ReportPathStep[3];
         steps[0] = new ReportPathStep(Links.USUARIO__DEPARTAM); // Usuario --> Departamento
         steps[0].hide();
         steps[1] = new ReportPathStep(Links.DEPARTAMENTO__AREA); // Departamento --> Area
         steps[1].hide();
         steps[2] = new ReportPathStep(Links.SOCIEDAD_AREA); // Area <-- Sociedad
         steps[2].setDirection(ReportPathStep.DIRECTION_USAGE);
         // Nivel 0: Usuario
         // Nivel 1: Departamento
         // Nivel 2: Area
         // Nivel 3: Sociedad

         List<RochadeItemData> data = this.getSession().executeReportPath(appName, Types.USUARIO,
               userId.toUpperCase(), steps, CommonAtt.TYPE, CommonAtt.NAME);

         for (RochadeItemData item : data) {
            if (Types.SOCIEDAD.equals(item.getType())) {
               return item.getName();
            }
         }
      }
      return "";
   }

   /**
    * Obtiene la member class de un usuario
    *
    * @param userId
    *           Indentificador del usuario
    * @return Member class que tiene el usario en la subject area
    * @throws RochadeDataAccessException
    */
   public String getMemberClass(String userId) throws RochadeDataAccessException {
      return getAutoPilotSession().getUserMemberClass(appName, userId);
   }

   /**
    * Obtiene la información correspondiente al id que tiene el usuario en rochade (con su combinación de
    * mayúsculas y minúsculas) y su member class
    *
    * @param userId
    *           Indentificador del usuario
    * @return
    * @throws RochadeDataAccessException
    */
   public UserInfo getRochadeUserInfo(String userId) throws RochadeDataAccessException {
      return getAutoPilotSession().gerUserInfo(appName, userId);
   }

   /**
    * Modifica la member class del usuario así como su atributo tipo del item REPO_USUARIO correspondiente
    *
    * @param userId
    *           Identificador del usuario
    * @param memberClass
    *           Member class que se le quiere asignar
    * @throws RochadeDataAccessException
    */
   public void changeMemberClass(String userId, MemberClass memberClass) throws RochadeDataAccessException {
      // Cambio de Member class
      getSession().close();
      rochadeSessionHolder.openRochadeSerssionMemberclass();
      getSession().changeUserMemberClass(appName, userId, memberClass.getValue());

      // Cambio del Rol
      getAdminSession().setUserRole(userId, memberClass.getRole());

      // Cambio de tipo en el Repousuario
      Long userRochadeId = getSession().resolve(appName, Types.USUARIO, userId.toUpperCase());

      TextAttribute attTipo = new TextAttribute(Attributes.TIPO, memberClass.getType());

      if (!getSession().modifyItem(appName, userRochadeId, false, attTipo)) {
         throw new RochadeDataAccessException("No se ha podido modificar el tipo del usuario " + userId);
      }
   }


   /**
    * Crea una Agrupación Técnica para circuitos con origen ALMV2 y operación PUT
    *
    * @param techGroupName
    *           Nombre de la nueva Agrupación Técnica
    * @param techGroupDescription
    *           Descripción de la Agrupación Técnica
    * @param userId
    *           Usuario responsable de la Agrupación técnica
    * @param platformId
    *           Tecnología a la que pertence la Agrupación Técnica
    * @param functionalServiceId
    *           Nombre del servicio del que cuelga la Agrupación Técnica
    * @param repositories
    *           Repositorios donde se encuentra la Agrupación Técnica
    * @return Generated Id
    * @throws RochadeDataAccessException
    */
   public String modifyTechGroupPut(String techGroupId, String techGroupName, String storedTechgroupName, String techGroupDescription, String userId,
         String platformId, String functionalServiceId,String storedServiceId, List<String> repositories ) throws RochadeDataAccessException {

      // Atributos de texto
      TextAttribute name = new TextAttribute(Attributes.NOMBRE, techGroupName); // Nombre
      TextAttribute description = new TextAttribute(Attributes.DESCRIPCION, techGroupDescription); // Descripción
      // Atributos de link
      LinkAttribute responsable = null;
      if (userId != null) {
          responsable = new LinkAttribute(Links.AGRUP_TEC_D__USU_RESPONS, Types.USUARIO, userId.toUpperCase()); // Usuario
      }
      LinkAttribute service = new LinkAttribute(Links.AGRUP_TEC_D__SERVICIO_V, Types.SERVICIO_V, getServiceVersionName(functionalServiceId)); // Servicio
      LinkAttribute repoLink = new LinkAttribute(Links.AGRUP_TEC_D__REPOSITORY); // Repositorios

      for (String repository:repositories){
          LinkData link = new LinkData();
          link.setItemName(repository);
          link.setItemType(Types.NEXUS_REPOSITORY);
          repoLink.addLink(link);
      }

      long techGroupIdMod = getSession().resolve(appName, Types.AGRUP_TEC_D, techGroupId);
      boolean techGroupModified = getSession().modifyItem(appName, techGroupIdMod, true, name, description, responsable, service, repoLink);

      if (techGroupModified) {

          // En caso de cambiar el nombre de la AT se propagan los cambios al nombre e instancia de todas sus ATV
          if (!techGroupName.equals(storedTechgroupName) ) {
              RochadeItemData techGroupVersions = getSession().getAttributes(appName, techGroupIdMod, Links.AGRUP_TEC_D__AGRUP_TEC_V);
              List<Long> techGroupsVersionsId = techGroupVersions.getLinksIds(Links.AGRUP_TEC_D__AGRUP_TEC_V);
              if (techGroupsVersionsId.size() != 0) {
                  for (Long techGroupVersionId : techGroupsVersionsId) {
                      String version = getSession().getAttributes(appName, techGroupVersionId, Attributes.VERSION, CommonAtt.NAME).getTextAttValue(Attributes.VERSION);
                      TextAttribute nameVersion = new TextAttribute(Attributes.NOMBRE, techGroupName + " " + version);
                      TextAttribute publishInstance = new TextAttribute(Attributes.INSTANCIA_PUBLICACION, techGroupName + " " + version);
                      getSession().modifyItem(appName, techGroupVersionId, false, nameVersion, publishInstance);
                  }
              }
          }

          if (!functionalServiceId.equals(storedServiceId)) {

        	  // En cada ATV se cambia el enlace al SFV
        	  RochadeItemData techGroupVersionsList = getSession().getAttributes(appName, techGroupIdMod, Links.AGRUP_TEC_D__AGRUP_TEC_V);
              List<Long> techGroupsVersionsIds = techGroupVersionsList.getLinksIds(Links.AGRUP_TEC_D__AGRUP_TEC_V);
              if (techGroupsVersionsIds.size() != 0) {
                  for (Long techGroupVersionId : techGroupsVersionsIds) {
                      LinkAttribute atvService = new LinkAttribute(Links.AGRUP_TEC_V__SERVV, Types.SERVICIO_V,getServiceVersionName(functionalServiceId));
                      getSession().modifyItem(appName, techGroupVersionId, true, atvService);
                  }
              }

              // Obtengo el SFV  para borrar el enlace a la AT
              Long serviceVersionId = getSession().resolve(appName, Types.SERVICIO_V, getServiceVersionName(storedServiceId));
              RochadeItemData techGroups = getSession().getAttributes(appName, Types.SERVICIO_V, getServiceVersionName(storedServiceId), Links.SERVV__AGRUP_TEC_D, Links.SERVV__AGRUP_TEC_V);
              LinkAttribute atRemove = techGroups.getLinkAtt(Links.SERVV__AGRUP_TEC_D);
              LinkData atLink = atRemove.getLinkByName(techGroupId);
              if (null != atLink ) {
                  atRemove.removeLink(atLink.getId());
              }

              // Obtengo el SFV  para borrar el enlace a las ATV
              LinkAttribute atvRemove = techGroups.getLinkAtt(Links.SERVV__AGRUP_TEC_V);
              RochadeItemData techGroupVersions = getSession().getAttributes(appName, techGroupIdMod, Links.AGRUP_TEC_D__AGRUP_TEC_V);
              List<Long> techGroupsVersionsId = techGroupVersions.getLinksIds(Links.AGRUP_TEC_D__AGRUP_TEC_V);
              List<LinkData> atvLinks = atvRemove.getLinkList();
              if (!techGroupsVersionsId.isEmpty() && !atvLinks.isEmpty()) {
            	  for (Long id : techGroupsVersionsId ) {
            		  atvRemove.removeLink(id);
            	  }
              }

              // Modifico el SFV antiguo sin los enlaces antiguos
              getSession().modifyItem(appName, serviceVersionId, true, atRemove, atvRemove);

              // Y en el nuevo SF poner link de todos sus SFV a la AT
              Long serviceId = getSession().resolve(appName, Types.SERVICIO_D, functionalServiceId);
              RochadeItemData serviceVersions = getSession().getAttributes(appName, serviceId, Links.SERV__SERVVER);
              List<Long> serviceVersionsId = serviceVersions.getLinksIds(Links.SERV__SERVVER);

              // Para cada SFV se mete el link a la AT
              List<String> techGroupsVersionsIdNames = techGroupVersions.getLinksNames(Links.AGRUP_TEC_D__AGRUP_TEC_V);
              if (serviceVersionsId.size() != 0) {
                  for (Long servId : serviceVersionsId) {
                	  // En cada SFV se enlaza con las ATV de la ATV
                	  if (!techGroupsVersionsIdNames.isEmpty()) {
                		  for (String techGroupsVersions : techGroupsVersionsIdNames) {
                			  LinkAttribute technicalVersionLink = new LinkAttribute(Links.SERVV__AGRUP_TEC_V, Types.AGRUP_TEC_V, techGroupsVersions);
                              getSession().modifyItem(appName, servId, false, technicalVersionLink);
                		  }
                	  }
                      // En cada SFV se enlaza con la AT
                	  LinkAttribute technicalLink = new LinkAttribute(Links.SERVV__AGRUP_TEC_D, Types.AGRUP_TEC_D, techGroupId);
                      getSession().modifyItem(appName, servId, false, technicalLink);
                  }
              }
          }
      } else {
          throw new RochadeDataAccessException("No se ha podido modificar la Agrupación Técnica " + techGroupName);
      }
      return techGroupId;
   }


   /**
    * Crea una Agrupación Técnica para circuitos con origen ALMV2 y operación PUT
    *
    * @param techGroupName
    *           Nombre de la nueva Agrupación Técnica
    * @param techGroupDescription
    *           Descripción de la Agrupación Técnica
    * @param userId
    *           Usuario responsable de la Agrupación técnica
    * @param platformId
    *           Tecnología a la que pertence la Agrupación Técnica
    * @param functionalServiceId
    *           Nombre del servicio del que cuelga la Agrupación Técnica
    * @param repositories
    *           Repositorios donde se encuentra la Agrupación Técnica
    * @return Generated Id
    * @throws RochadeDataAccessException
    */
   public String modifyTechGroupPatch(String techGroupId, String techGroupName, String storedTechgroupName, String techGroupDescription, String userId,
         String platformId, String functionalServiceId,String storedServiceId, List<String> repositories ) throws RochadeDataAccessException {

	  long techGroupIdMod = getSession().resolve(appName, Types.AGRUP_TEC_D, techGroupId);
	  boolean techGroupModified = false;

      // Atributos de texto
	  if (null != techGroupName && !techGroupName.isEmpty()) {
		  TextAttribute name = new TextAttribute(Attributes.NOMBRE, techGroupName); // Nombre
		  techGroupModified = getSession().modifyItem(appName, techGroupIdMod, true, name);
	  }
	  if (null != techGroupDescription && !techGroupDescription.isEmpty()) {
		  TextAttribute description = new TextAttribute(Attributes.DESCRIPCION, techGroupDescription); // Descripción
		  techGroupModified = getSession().modifyItem(appName, techGroupIdMod, true, description);
	  }
      // Atributos de link
      LinkAttribute responsable = null;
      if (null != userId && !userId.isEmpty()) {
          responsable = new LinkAttribute(Links.AGRUP_TEC_D__USU_RESPONS, Types.USUARIO, userId.toUpperCase()); // Usuario
          techGroupModified = getSession().modifyItem(appName, techGroupIdMod, true, responsable);
      }

      if (null != functionalServiceId && !functionalServiceId.isEmpty()){
    	  LinkAttribute service = new LinkAttribute(Links.AGRUP_TEC_D__SERVICIO_V, Types.SERVICIO_V, getServiceVersionName(functionalServiceId)); // Servicio
    	  techGroupModified = getSession().modifyItem(appName, techGroupIdMod, true, service);
      }

      if (null != repositories) {
    	  LinkAttribute repoLink = new LinkAttribute(Links.AGRUP_TEC_D__REPOSITORY); // Repositorios

    	  for (String repository:repositories){
    		  LinkData link = new LinkData();
    		  link.setItemName(repository);
    		  link.setItemType(Types.NEXUS_REPOSITORY);
    		  repoLink.addLink(link);
    	  }
    	  techGroupModified = getSession().modifyItem(appName, techGroupIdMod, true, repoLink);
      }

      if (techGroupModified) {
          // En caso de cambiar el nombre de la AT se propagan los cambios al nombre e instancia de todas sus ATV
          if (null != techGroupName && !techGroupName.equals(storedTechgroupName) ) {
              RochadeItemData techGroupVersions = getSession().getAttributes(appName, techGroupIdMod, Links.AGRUP_TEC_D__AGRUP_TEC_V);
              List<Long> techGroupsVersionsId = techGroupVersions.getLinksIds(Links.AGRUP_TEC_D__AGRUP_TEC_V);
              if (techGroupsVersionsId.size() != 0) {
                  for (Long techGroupVersionId : techGroupsVersionsId) {
                      String version = getSession().getAttributes(appName, techGroupVersionId, Attributes.VERSION, CommonAtt.NAME).getTextAttValue(Attributes.VERSION);
                      TextAttribute nameVersion = new TextAttribute(Attributes.NOMBRE, techGroupName + " " + version);
                      TextAttribute publishInstance = new TextAttribute(Attributes.INSTANCIA_PUBLICACION, techGroupName + " " + version);
                      getSession().modifyItem(appName, techGroupVersionId, false, nameVersion, publishInstance);
                  }
              }
          }

          if (null != functionalServiceId && !functionalServiceId.equals(storedServiceId)) {

        	  // En cada ATV se cambia el enlace al SFV
        	  RochadeItemData techGroupVersionsList = getSession().getAttributes(appName, techGroupIdMod, Links.AGRUP_TEC_D__AGRUP_TEC_V);
              List<Long> techGroupsVersionsIds = techGroupVersionsList.getLinksIds(Links.AGRUP_TEC_D__AGRUP_TEC_V);
              if (techGroupsVersionsIds.size() != 0) {
                  for (Long techGroupVersionId : techGroupsVersionsIds) {
                      LinkAttribute atvService = new LinkAttribute(Links.AGRUP_TEC_V__SERVV, Types.SERVICIO_V,getServiceVersionName(functionalServiceId));
                      getSession().modifyItem(appName, techGroupVersionId, true, atvService);
                  }
              }

              // Obtengo el SFV  para borrar el enlace a la AT
              Long serviceVersionId = getSession().resolve(appName, Types.SERVICIO_V, getServiceVersionName(storedServiceId));
              RochadeItemData techGroups = getSession().getAttributes(appName, Types.SERVICIO_V, getServiceVersionName(storedServiceId), Links.SERVV__AGRUP_TEC_D, Links.SERVV__AGRUP_TEC_V);
              LinkAttribute atRemove = techGroups.getLinkAtt(Links.SERVV__AGRUP_TEC_D);
              LinkData atLink = atRemove.getLinkByName(techGroupId);
              if (null != atLink ) {
                  atRemove.removeLink(atLink.getId());
              }

              // Obtengo el SFV  para borrar el enlace a las ATV
              LinkAttribute atvRemove = techGroups.getLinkAtt(Links.SERVV__AGRUP_TEC_V);
              RochadeItemData techGroupVersions = getSession().getAttributes(appName, techGroupIdMod, Links.AGRUP_TEC_D__AGRUP_TEC_V);
              List<Long> techGroupsVersionsId = techGroupVersions.getLinksIds(Links.AGRUP_TEC_D__AGRUP_TEC_V);
              List<LinkData> atvLinks = atvRemove.getLinkList();
              if (!techGroupsVersionsId.isEmpty() && !atvLinks.isEmpty()) {
            	  for (Long id : techGroupsVersionsId ) {
            		  atvRemove.removeLink(id);
            	  }
              }

              // Modifico el SFV antiguo sin los enlaces antiguos
              getSession().modifyItem(appName, serviceVersionId, true, atRemove, atvRemove);

              // Y en el nuevo SF poner link de todos sus SFV a la AT
              Long serviceId = getSession().resolve(appName, Types.SERVICIO_D, functionalServiceId);
              RochadeItemData serviceVersions = getSession().getAttributes(appName, serviceId, Links.SERV__SERVVER);
              List<Long> serviceVersionsId = serviceVersions.getLinksIds(Links.SERV__SERVVER);

              // Para cada SFV se mete el link a la AT
              List<String> techGroupsVersionsIdNames = techGroupVersions.getLinksNames(Links.AGRUP_TEC_D__AGRUP_TEC_V);
              if (serviceVersionsId.size() != 0) {
                  for (Long servId : serviceVersionsId) {
                	  // En cada SFV se enlaza con las ATV de la ATV
                	  if (!techGroupsVersionsIdNames.isEmpty()) {
                		  for (String techGroupsVersions : techGroupsVersionsIdNames) {
                			  LinkAttribute technicalVersionLink = new LinkAttribute(Links.SERVV__AGRUP_TEC_V, Types.AGRUP_TEC_V, techGroupsVersions);
                              getSession().modifyItem(appName, servId, false, technicalVersionLink);
                		  }
                	  }
                      // En cada SFV se enlaza con la AT
                	  LinkAttribute technicalLink = new LinkAttribute(Links.SERVV__AGRUP_TEC_D, Types.AGRUP_TEC_D, techGroupId);
                      getSession().modifyItem(appName, servId, false, technicalLink);
                  }
              }
          }
      } else {
          throw new RochadeDataAccessException("No se ha podido modificar la Agrupación Técnica " + techGroupName);
      }
      return techGroupId;
   }


   /**
    * Crea una Agrupación Técnica para circuitos con origen ALMV2
    *
    * @param techGroupName
    *           Nombre de la nueva Agrupación Técnica
    * @param techGroupDescription
    *           Descripción de la Agrupación Técnica
    * @param userId
    *           Usuario responsable de la Agrupación técnica
    * @param platformId
    *           Tecnología a la que pertence la Agrupación Técnica
    * @param functionalServiceId
    *           Nombre del servicio del que cuelga la Agrupación Técnica
    * @param repositories
    *           Repositorios donde se encuentra la Agrupación Técnica
    * @return Generated Id
    * @throws RochadeDataAccessException
    */
   public String createTechGroupAlm(String techGroupName, String techGroupDescription, String userId,
         String platformId, String functionalServiceId, List<String> repositories) throws RochadeDataAccessException {
      // Creamos la agrupación técnica con su contenido
      RochadeItemData techGroupData = new RochadeItemData();
      // Atributos de texto
      techGroupData.insert(new TextAttribute(CommonAtt.TYPE, Types.AGRUP_TEC_D)); // Tipo
      String randomId = IDGenerator.getRandomId();
      techGroupData.insert(new TextAttribute(CommonAtt.NAME, randomId)); // Item name
      techGroupData.insert(new TextAttribute(Attributes.NOMBRE, techGroupName)); // Nombre
      techGroupData.insert(new TextAttribute(Attributes.DESCRIPCION, techGroupDescription)); // Descripción
      //techGroupData.insert(new TextAttribute(Attributes.PRODUCTO_SGS, techGroupName)); // Código SGS
      // Atributos de link
      if (userId != null) {
         techGroupData
               .insert(new LinkAttribute(Links.AGRUP_TEC_D__USU_RESPONS, Types.USUARIO, userId.toUpperCase())); // Usuario
      } // responsable
      techGroupData.insert(new LinkAttribute(Links.AGRUP_TEC_D__SERVICIO_V, Types.SERVICIO_V,
            getServiceVersionName(functionalServiceId))); // Servicio
                                                          // versión
      techGroupData.insert(new LinkAttribute(Links.AGRUP_TEC_D__TECNOL, Types.TECNOLOGIA, platformId)); // Plataforma

      LinkAttribute repoLink = new LinkAttribute(Links.AGRUP_TEC_D__REPOSITORY);

      for (String repository:repositories){
          LinkData link = new LinkData();
          link.setItemName(repository);
          link.setItemType(Types.NEXUS_REPOSITORY);
          techGroupData.insert(repoLink.addLink(link)); // Repositorios
      }

      long atRochadeId = getSession().createItem(appName, techGroupData);

      if (atRochadeId != 0) {
         // Link del servicio versión a la agrupación técnica
         Long serviceRochadeId = getSession().resolve(appName, Types.SERVICIO_V,getServiceVersionName(functionalServiceId));
         LinkAttribute servLnk = new LinkAttribute(Links.SERVV__AGRUP_TEC_D, Types.AGRUP_TEC_D, randomId);

         if (!getSession().modifyItem(appName, serviceRochadeId, false, servLnk)) {
            boolean deleteSucessfull = getSession().deleteItem(appName, atRochadeId);
            throw new RochadeDataAccessException("No se ha podido enlazar el Servicio Funcional "
                  + getServiceVersionName(functionalServiceId) + " con la nueva Agrupación Técnica "
                  + randomId
                  + (deleteSucessfull
                        ? ". No se ha podido borrar la agrupacion técnica con itemName  " + randomId
                        : ". Se ha borrado la agrupación técnica"));
         }
      } else {
         throw new RochadeDataAccessException("No se ha podido crear la Agrupación Técnica " + techGroupName);
      }
      return randomId;
   }

   /**
    * Crea una Agrupación Técnica para circuitos sin origen ALMV2
    * @param techGroupName
    * @param techGroupDescription
    * @param userId
    * @param platformId
    * @param functionalServiceId
    * @return
    * @throws RochadeDataAccessException
    */
   public String createTechGroup(String techGroupName, String techGroupDescription, String userId,
	         String platformId, String functionalServiceId) throws RochadeDataAccessException {
	      // Creamos la agrupación técnica con su contenido
	      RochadeItemData techGroupData = new RochadeItemData();
	      // Atributos de texto
	      techGroupData.insert(new TextAttribute(CommonAtt.TYPE, Types.AGRUP_TEC_D)); // Tipo
	      String randomId = IDGenerator.getRandomId();
	      techGroupData.insert(new TextAttribute(CommonAtt.NAME, randomId)); // Item name
	      techGroupData.insert(new TextAttribute(Attributes.NOMBRE, techGroupName)); // Nombre
	      techGroupData.insert(new TextAttribute(Attributes.DESCRIPCION, techGroupDescription)); // Descripción
	      // techGroupData.insert(new TextAttribute(Attributes.PRODUCTO_SGS, techGroupName)); // Código SGS
	      // Atributos de link
	      if (userId != null && !userId.isEmpty()) {
	         techGroupData
	               .insert(new LinkAttribute(Links.AGRUP_TEC_D__USU_RESPONS, Types.USUARIO, userId.toUpperCase())); // Usuario
	      } // responsable
	      techGroupData.insert(new LinkAttribute(Links.AGRUP_TEC_D__SERVICIO_V, Types.SERVICIO_V,
	            getServiceVersionName(functionalServiceId))); // Servicio
	                                                          // versión
	      techGroupData.insert(new LinkAttribute(Links.AGRUP_TEC_D__TECNOL, Types.TECNOLOGIA, platformId)); // Plataforma

          techGroupData.insert(new LinkAttribute(Links.AGRUP_TEC_D__REPOSITORY)); // Repositorio

          long atRochadeId = getSession().createItem(appName, techGroupData);

          if (atRochadeId != 0) {
             // Link del servicio versión a la agrupación técnica
             Long serviceRochadeId = getSession().resolve(appName, Types.SERVICIO_V,
                   getServiceVersionName(functionalServiceId));
             LinkAttribute servLnk = new LinkAttribute(Links.SERVV__AGRUP_TEC_D, Types.AGRUP_TEC_D, randomId);

             if (!getSession().modifyItem(appName, serviceRochadeId, false, servLnk)) {
                boolean deleteSucessfull = getSession().deleteItem(appName, atRochadeId);
                throw new RochadeDataAccessException("No se ha podido enlazar el Servicio Funcional "
                      + getServiceVersionName(functionalServiceId) + " con la nueva Agrupación Técnica "
                      + randomId
                      + (deleteSucessfull
                            ? ". No se ha podido borrar la agrupacion técnica con itemName  " + randomId
                            : ". Se ha borrado la agrupación técnica"));
             }
          } else {
             throw new RochadeDataAccessException("No se ha podido crear la Agrupación Técnica " + techGroupName);
          }
          return randomId;
       }

   /**
    * Crea en Rochade una nueva Agrupación Técnica Versión
    * @param techGroupItemName   ItemName de la Agrupación técnica de la que se creará la versión
    * @param version Versión de la Agrupación técnica
    * @param description Descripción de la Agrupación técnica versión
    * @param publishInstance Instacia de publicación de la Agrupación técnica versión
    * @param published Valor del campo Publicado de la Agrupación técnica versión
    * @param validity Valor del campo Vigencia de la Agrupación técnica versión
    * @param publishDate Valor del campo Fecha de Pulbicación de la Agrupación técnica versión
    * @return
    * @throws RochadeDataAccessException
    */
   public String createTechGroupVers(String techGroupItemName, String version, String description,
                   String publishInstance, String status, String published, String validity, String publishDate)
               throws RochadeDataAccessException {
       // Obtenemos la información necesaria de la Agrupación técnica
       RochadeItemData techGroupData = getSession().getAttributes(appName, Types.AGRUP_TEC_D, techGroupItemName,
                       CommonAtt.ID, CommonAtt.NAME, Attributes.NOMBRE, Links.AGRUP_TEC_D__TECNOL, Links.AGRUP_TEC_D__SERVICIO_V);

       // Rellenamos la información necesaria para la Agrupación Técnica Versión
       RochadeItemData techGroupVersData = new RochadeItemData();
       String atvItemName = techGroupItemName + " " + version;
       techGroupVersData.insert(new TextAttribute(CommonAtt.TYPE, Types.AGRUP_TEC_V));
       techGroupVersData.insert(new TextAttribute(CommonAtt.NAME, atvItemName));
       techGroupVersData.insert(new TextAttribute(Attributes.NOMBRE, techGroupData.getTextAttValue(Attributes.NOMBRE) + " " + version));
       techGroupVersData.insert(new TextAttribute(Attributes.VERSION, version));
       techGroupVersData.insert(new TextAttribute(Attributes.DESCRIPCION, description));
       techGroupVersData.insert(new TextAttribute(Attributes.INSTANCIA_PUBLICACION, publishInstance));
       techGroupVersData.insert(new LinkAttribute(Links.AGRUP_TEC_V__STATUS, Types.ESTADO_AGRUPACION, status));
       techGroupVersData.insert(new TextAttribute(Attributes.VIGENTE, validity));
       if (!published.isEmpty()) {
           techGroupVersData.insert(new TextAttribute(Attributes.PUBLICADO, published));
       }
       if (publishDate!= null && !publishDate.isEmpty()) {
           techGroupVersData.insert(new TextAttribute(Attributes.FECHA_PUB, publishDate));
       }
       techGroupVersData.addAttFromData(techGroupData, Links.AGRUP_TEC_D__TECNOL, Links.AGRUP_TEC_V__TECNOL);
       techGroupVersData.addAttFromData(techGroupData, Links.AGRUP_TEC_D__SERVICIO_V, Links.AGRUP_TEC_V__SERVV);

       // Creamos la agrupación técnica versión en Rochade
       long atvRochadeId = this.getSession().createItem(appName, techGroupVersData);
       if (atvRochadeId != 0) {
           // Tiramos los links de la Agrupación Técnica a la versión y del servicio versión a la ATV
           LinkAttribute newLink = new LinkAttribute(Links.AGRUP_TEC_D__AGRUP_TEC_V, Types.AGRUP_TEC_V, atvItemName);
           if (this.getSession().modifyItem(appName, techGroupData.getId(), false, newLink)) {
               List<Long> servLinks = techGroupData.getLinksIds(Links.AGRUP_TEC_D__SERVICIO_V);
               if (!servLinks.isEmpty()) {
                   newLink = new LinkAttribute(Links.SERVV__AGRUP_TEC_V, Types.AGRUP_TEC_V, atvItemName);
                   if (!this.getSession().modifyItem(appName, servLinks.get(0), false, newLink)) {
                       // Si no se ha enlazado correctamente el Servicio versión con la ATV, eliminamos la ATV y lanzamos una excepción
                       boolean deleteSucessfull = getSession().deleteItem(appName, atvRochadeId);
                       throw new RochadeDataAccessException("No se ha podido enlazar el Servicio Funcional versión "
                                       + techGroupData.getLinksNames(Links.AGRUP_TEC_D__SERVICIO_V).get(0) +
                                       " con la nueva Agrupación Técnica Versión " + atvItemName
                                       + (deleteSucessfull
                                             ? ". No se ha podido borrar la Agrupacion técnica versión " + atvItemName
                                             : ". Se ha borrado la agrupación técnica versión"));
                   }
               }
           } else {
               // Si no se ha enlazado correctamente la AT con la ATV, eliminamos la ATV y lanzamos una excepción
               boolean deleteSucessfull = getSession().deleteItem(appName, atvRochadeId);
               throw new RochadeDataAccessException("No se ha podido enlazar la Agrupación Técnica "
                               + techGroupItemName + " con la nueva Agrupación Técnica Versión" + atvItemName
                               + (deleteSucessfull
                                     ? ". No se ha podido borrar la Agrupacion técnica versión " + atvItemName
                                     : ". Se ha borrado la agrupación técnica versión"));
           }
       } else {
           throw new RochadeDataAccessException("No se ha podido crear la Agrupación Técnica Versión " + atvItemName);
       }
       return atvItemName;
   }

   private String getServiceVersionName(String definitionName) {
      return definitionName + " V00R00";
   }

   // Obtiene la agrupación técnica con el businessName, plataforma y repositorios indicados
   public List<String> getTechnicalGroupIdsByBusinessNamePlatformRepository(String businessName, String platform, String repository) throws RochadeDataAccessException {
       StringBuilder query = new StringBuilder();
       query.append("(TYPE = ").append(Types.AGRUP_TEC_D).append(")");
       if (businessName != null && !businessName.isEmpty()) {
           query.append(" AND (TA ").append(Attributes.NOMBRE).append(" / ").append(RochadeSessionTools.rochadeEncode(businessName)).append(")");
       }
       if (platform != null && !platform.isEmpty()) {
           query.append(" AND (LA ").append(Links.AGRUP_TEC_D__TECNOL).append(" -> NAME ~/ ")
               .append(RochadeSessionTools.rochadeEncode(platform)).append(")");
       }
       if (repository != null && !repository.isEmpty()) {
           query.append(" AND (LA ").append(Links.AGRUP_TEC_D__REPOSITORY).append(" -> NAME ~/ ")
               .append(RochadeSessionTools.rochadeEncode(repository)).append(")");
       }

       List<RochadeItemData> queryResult = this.getSession().execQuery(appName, query.toString(), CommonAtt.NAME);
       List<String> idsList = new ArrayList<>();

       for (RochadeItemData item : queryResult) {
           idsList.add(item.getName());
       }

       return idsList;
   }

   public TechnicalGroup getTechnicalGroupById(String uuid) throws RochadeDataAccessException {
      TechnicalGroup techGroupResult = null;
      if (uuid != null) {
         ReportPathStep[] steps = new ReportPathStep[2];
         steps[0] = new ReportPathStep(Links.AGRUP_TEC_D__USU_RESPONS, Links.AGRUP_TEC_D__SERVICIO_V,
               Links.AGRUP_TEC_D__TECNOL, Links.AGRUP_TEC_D__REPOSITORY);// AgrupacionTecnica
                                                                         // --> Usuario,
                                                                         // Servicio,
                                                                         // Plataforma
                                                                         // Repositorios
         steps[1] = new ReportPathStep(Links.USUARIO__DEPARTAM);// Usuario --> Departamento

         List<RochadeItemData> data = this.getSession().executeReportPath(appName, Types.AGRUP_TEC_D, uuid,
               steps, CommonAtt.TYPE, CommonAtt.NAME, Attributes.NOMBRE, Attributes.DESCRIPCION,
               Attributes.PRODUCTO_SGS, CommonAtt.BUSINESS_NAME, Attributes.EMAIL, Attributes.CODIGO, Links.AGRUP_TEC_D__REPOSITORY, Types.NEXUS_REPOSITORY);
         for (RochadeItemData item : data) {
            switch (item.getType()) {
            case Types.AGRUP_TEC_D:
               techGroupResult = new TechnicalGroup();
               techGroupResult.setId(item.getTextAttValue(CommonAtt.NAME));
               techGroupResult.setName(item.getTextAttValue(Attributes.NOMBRE));
               techGroupResult.setDescription(item.getTextAttValue(Attributes.DESCRIPCION));
               techGroupResult.setSgsCode(item.getTextAttValue(Attributes.PRODUCTO_SGS));
               techGroupResult.setRepositories(item.getLinksNames(Links.AGRUP_TEC_D__REPOSITORY));
               break;
            case Types.USUARIO:
               User responsible = new User();
               responsible.setId(item.getTextAttValue(CommonAtt.NAME));
               responsible.setEmail(item.getTextAttValue(Attributes.EMAIL));
               responsible.setName(item.getTextAttValue(Attributes.NOMBRE));
               assert techGroupResult != null;
               techGroupResult.setResponsible(responsible);
               break;
            case Types.DEPARTAMENTO:
               assert techGroupResult != null;
               if (techGroupResult.getResponsible() == null)
                  techGroupResult.setResponsible(new User());
               techGroupResult.getResponsible().setDepartment(item.getBusinessName());
               break;
            case Types.SERVICIO_V:
               FunctionalService serviceResult = new FunctionalService();
               serviceResult.setId(item.getTextAttValue(Attributes.CODIGO));
               serviceResult.setName(item.getTextAttValue(Attributes.NOMBRE));
               serviceResult.setVersion("V00R00");
               assert techGroupResult != null;
               techGroupResult.setFunctionalService(serviceResult);
               break;
            case Types.TECNOLOGIA:
               Platform platformResult = new Platform();
               platformResult.setId(item.getTextAttValue(CommonAtt.NAME));
               platformResult.setName(item.getTextAttValue(CommonAtt.BUSINESS_NAME));
               assert techGroupResult != null;
               techGroupResult.setPlatform(platformResult);
               break;
            }
         }
      }
      return techGroupResult;

   }

   /**
    * Obtiene la información de una Agrupación Técnica Versión
    * @param techGroupId Identificador (itemName) de una Agrupación Técnica versión
    * @return
    * @throws RochadeDataAccessException
    */
   public TechnicalGroupVersion getTechnicalGroupVersionById(String techGroupId) throws RochadeDataAccessException {
       TechnicalGroupVersion techGroupVersionResult = null;
       if (techGroupId != null && !techGroupId.isEmpty()) {
           ReportPathStep[] steps = new ReportPathStep[1];
           steps[0] = new ReportPathStep(Links.AGRUP_TEC_V__ESTADO);   // REPO_APLICF_VERS --> REPO_ESTADO_AGRUP
           // Nivel 0: Agrupación técnica versión
           // Nivel 1: Estado de la agrupación técnica

           List<RochadeItemData> data = this.getSession().executeReportPath(appName, Types.AGRUP_TEC_V, techGroupId,
                           steps, CommonAtt.TYPE, CommonAtt.NAME, CommonAtt.BUSINESS_NAME, Attributes.NOMBRE, Attributes.VERSION,
                           Attributes.DESCRIPCION, Attributes.INSTANCIA_PUBLICACION, Attributes.FECHA_PUB);

           for (RochadeItemData item : data) {
               switch (item.getType()) {
               case Types.AGRUP_TEC_V:
                   techGroupVersionResult = new TechnicalGroupVersion();
                   techGroupVersionResult.setId(item.getName());
                   techGroupVersionResult.setName(item.getTextAttValue(Attributes.NOMBRE));
                   techGroupVersionResult.setVersion(item.getTextAttValue(Attributes.VERSION));
                   techGroupVersionResult.setDescription(item.getTextAttValue(Attributes.DESCRIPCION));
                   String pubInst = item.getTextAttValue(Attributes.INSTANCIA_PUBLICACION);
                   if (pubInst != null && !pubInst.isEmpty()) {
                       techGroupVersionResult.setPublishInstance(pubInst);
                   }
                   String pubDate = item.getTextAttValue(Attributes.FECHA_PUB);
                   if (pubDate != null && !pubDate.isEmpty()) {
                       techGroupVersionResult.setPublishDate(pubDate);
                   }
                   break;
               case Types.ESTADO_AGRUPACION:
                   Status statusResult = new Status();
                   statusResult.setId(item.getName());
                   statusResult.setName(item.getBusinessName());
                   if (techGroupVersionResult != null) {
                       techGroupVersionResult.setStatus(statusResult);
                   }
                   break;
               }
           }
       }

       return  techGroupVersionResult;
   }

   /**
    * Comprueba si el usuario tiene permisos para crear una Agrupación Técnica
    */
   public boolean canCreateTechGroup() throws RochadeDataAccessException {
      return this.getSession().checkItemTypePermission(appName, Types.AGRUP_TEC_D, Permissions.CREATE);
   }

   /**
    * Comprueba si el usuario tiene permisos para crear una Agrupación Técnica versión
    */
   public boolean canCreateTechGroupVersion() throws RochadeDataAccessException {
      return this.getSession().checkItemTypePermission(appName, Types.AGRUP_TEC_V, Permissions.CREATE);
   }

}
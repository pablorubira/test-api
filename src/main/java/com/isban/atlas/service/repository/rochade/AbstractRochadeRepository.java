package com.isban.atlas.service.repository.rochade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import es.isb.atlas.rochade.data.access.RochadeAdminSession;
import es.isb.atlas.rochade.data.access.RochadeApplicationName;
import es.isb.atlas.rochade.data.access.RochadeAutopilotSession;
import es.isb.atlas.rochade.data.access.RochadeSession;
import es.isb.atlas.rochade.data.access.exception.RochadeDataAccessException;
import es.isb.atlas.rochade.data.access.model.RochadeItemData;

@Repository
abstract class AbstractRochadeRepository {

   private RochadeSessionHolder rochadeHolder;

   protected RochadeApplicationName appName;

   @Autowired
   AbstractRochadeRepository(RochadeSessionHolder rochadeHolder, final char type, final String name,
         final String version) {
      this.rochadeHolder = rochadeHolder;
      this.appName = new RochadeApplicationName(type, name, version);
   }

   protected RochadeSession getSession() {
      return rochadeHolder.getRochadeSession();
   }

   protected RochadeAutopilotSession getAutoPilotSession() throws RochadeDataAccessException {
      return rochadeHolder.getRochadeAutopilotSession();
   }

   protected RochadeAdminSession getAdminSession() throws RochadeDataAccessException {
      return rochadeHolder.getRochadeAdminSession();
   }

   /*
    * Verifica si existe un item
    */
   boolean existItem(String itemType, String itemName) throws RochadeDataAccessException {
      if (itemType != null && itemName != null) {
         return getSession().existItem(this.appName, itemType, itemName);
      } else {
         return false;
      }
   }

   /*
    * Verifica si un item está activo
    */
   boolean isActiveItem(String itemType, String itemName) throws RochadeDataAccessException {
      if (itemType != null && itemName != null) {
         return getSession().isActiveItem(this.appName, itemType, itemName);
      } else {
         return false;
      }
   }

   /*
    * Verifica si un item tiene referencias
    */
   boolean referenceItem(String itemType, String itemName) throws RochadeDataAccessException {
      if (itemType != null && itemName != null && getSession().getItemUses(this.appName, itemType, itemName, "").isEmpty()) {
    		 return true;
      } else {
         return false;
      }
   }

   /*
    * Crea un item en Rochade
    */
   void createItem(String itemType, String itemName) throws RochadeDataAccessException {
	   if (itemType != null && itemName != null)
		   getSession().createItem(this.appName, itemType, itemName);
   }

   /*
    * Borra un item en Rochade
    */
   void deleteItem(String itemType, String itemName) throws RochadeDataAccessException {
	   if (itemType != null && itemName != null) {
		   long id = getSession().resolve(appName, itemType, itemName);
		   getSession().deleteItem(appName, id);
	   }
   }

   /*
    * Obtiene el valor de un atributo de texto
    */
   String getTextAttribute(String itemType, String itemName, String attribute)
         throws RochadeDataAccessException {
      if (itemType != null && itemName != null && attribute != null) {
         RochadeItemData data = getSession().getAttributes(appName, itemType, itemName, attribute);
         return data.getTextAttValue(attribute);
      } else {
         return "";
      }
   }

   /*
    * Obtiene el valor de un atributo de link
    */
   String getLinkAttribute(String itemType, String itemName, String attribute)
         throws RochadeDataAccessException {
      if (itemType != null && itemName != null && attribute != null) {
         RochadeItemData data = getSession().getAttributes(appName, itemType, itemName, attribute);
         return data.getLinksNames(attribute).get(0);
      } else {
         return "";
      }
   }

}
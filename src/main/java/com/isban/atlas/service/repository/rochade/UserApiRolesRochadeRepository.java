package com.isban.atlas.service.repository.rochade;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.isban.atlas.service.api.userauth.domain.UserAuthorization;
import com.isban.atlas.service.exceptions.ApiException;

import es.isb.atlas.rochade.data.access.exception.RochadeDataAccessException;
import es.isb.atlas.rochade.data.access.model.RochadeItemData;

/**
 * 
 * @author XI320578
 *
 */
@Repository
public class UserApiRolesRochadeRepository extends AbstractRochadeRepository {

   private static final String CONFIG_ITEM_NAME = "USUARIOS_CONSULTA_API";
   private static final String CONFIG_ITEM_TYPE = "ARS_TEXTOS";
   private static final String CONFIG_ATTRIBUTE_NAME = "VALORES";

   @Autowired
   public UserApiRolesRochadeRepository(RochadeSessionHolder rochadeHolder,
         @Value("${sa.admin.type}") final char type, @Value("${sa.admin.name}") final String name,
         @Value("${sa.admin.version}") final String version) {
      super(rochadeHolder, type, name, version);
   }

   public Collection<UserAuthorization> getUsers() throws RochadeDataAccessException, ApiException {
      RochadeItemData data = this.getSession().getAttributes(appName, CONFIG_ITEM_TYPE, CONFIG_ITEM_NAME,
            CONFIG_ATTRIBUTE_NAME);
      if (data != null) {
         String values = data.getTextAttValue(CONFIG_ATTRIBUTE_NAME);
         return parseUserAuthorizations(values);
      } else {
         throw new ApiException("Internal Error",
               "Error en la configuración de los roles por usuario en Rochade. No se encuentra  "
                     + CONFIG_ITEM_TYPE + " con valor " + CONFIG_ITEM_NAME);
      }
   }

   protected Collection<UserAuthorization> parseUserAuthorizations(String values) throws ApiException {
      Collection<UserAuthorization> users = new ArrayList<>();
      Properties properties = loadStringAsProperties(values);
      Set<String> userIds = properties.stringPropertyNames();
      for (String userId : userIds) {
         UserAuthorization user = new UserAuthorization(userId);
         String roles = properties.getProperty(userId);
         if (!StringUtils.isEmpty(roles)) {
            for (String rol : roles.split(",")) {
               user.addRole(rol.trim());
            }
         }
         users.add(user);
      }
      return users;
   }

   private Properties loadStringAsProperties(String values) throws ApiException {
      try {
         final Properties p = new Properties();
         p.load(new StringReader(values));
         return p;
      } catch (IOException e) {
         throw new ApiException("Internal Error",
               "Error en la configuración de los roles por usuario en Rochade");
      }
   }
}
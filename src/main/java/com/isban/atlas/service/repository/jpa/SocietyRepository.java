package com.isban.atlas.service.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.isban.atlas.service.api.userauth.domain.Society;

public interface SocietyRepository extends JpaRepository<Society, Long> {

   Society findByName(String name);

}

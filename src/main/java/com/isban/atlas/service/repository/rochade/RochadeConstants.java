package com.isban.atlas.service.repository.rochade;

public class RochadeConstants {

    public class Attributes {
        public static final String ACRONIMO                 = "ACRONIMO";
        public static final String ACTIVO                   = "ACTIVO";
        public static final String BROCHURE                 = "BROCHURE";
        public static final String BUSINESS_NAME            = "BUSINESS_NAME";
        public static final String CODIGO                   = "CODIGO";
        public static final String COMENTARIO_JAVA          = "JAVA_COMMENT";
        public static final String CONT_CONS_1              = "CONTENIDO_CONSULTA_1";
        public static final String CONT_CONS_2              = "CONTENIDO_CONSULTA_2";
        public static final String CONT_PETICION            = "CONTENIDO_PETICION";
        public static final String DEFINICION               = "DEFINITION";
        public static final String DEPARTAMENTO             = "DEPARTAMENTO";
        public static final String DESCRIP_OBJ_HOST         = "DESCR250";
        public static final String DESCRIP_OBJ_PWC          = "DESCR250";
        public static final String DESCRIPCION              = "DESCRIPCION";
        public static final String DESCRIPCION_100          = "DESC100";
        public static final String DESCRIPCION_10000        = "DES10000";
        public static final String DESCRIPCION_EN           = "DESCRIPCION_1";
        public static final String DESCRIPCION_ES           = "DESCRIPCION_3";
        public static final String DESCRIPTION              = "DESCRIPTION";
        public static final String DSC_NOMBRE_EN            = "DSC/NAME_1";          // Nombre en inglés
        public static final String DSC_NOMBRE_ES            = "DSC/NAME_3";          // Nombre en español
        public static final String EJECUCION_EN_CURSO       = "EJECUCION_EN_CURSO";
        public static final String EMAIL                    = "EMAIL";
        public static final String ES_PORTAL                = "ES_PORTAL";
        public final static String EXPRESION_REGULAR_AT     = "AT_REGEX";
        public final static String EXPRESION_REGULAR_SGS_AT = "AT_SGS_REGEX";
        public static final String FECHA_CONSULTA           = "FECHA_CONSULTA";
        public static final String FECHA_PUB                = "FECHA_PUBLICACION";
        public static final String ID_USUARIO               = "USERID";
        public static final String INDIC_VIGENT             = "INDVIG";
        public static final String INDIC_SGS                = "ID_CLEARCASE";
        public static final String INFA_WORKFLOW_DESCR      = "INFA/WORKFLOW-DESCR";
        public static final String INSTANCIA_PUBLICACION    = "INSTANCIA_PUBLICACION";
        public static final String NOMBRE                   = "NOMBRE";
        public static final String NOMBRE_CORTO             = "NOMBRE_CORTO";
        public static final String NOMBRE_EN                = "NOMBRE_1";            // Nombre en inglés
        public static final String NOMBRE_ES                = "NOMBRE_3";            // Nombre en español
        public static final String NOMBRE_HOST              = "NOMB60";
        public static final String NOMBRE_JAVA              = "JAVA_NAME";
        public static final String NOMBRE_PHOENIX           = "PHX_NOMBRE";
        public static final String OBJETO                   = "OBJETO";
        public static final String ORIENTADO_SERVICIO       = "ORIENTADO_SERV";
        public static final String PRODUCTO_SGS             = "PRODUCTO_SGS";
        public static final String PROPERTIES               = "PROPERTIES";
        public static final String PUBLICADO                = "PUBLICADO";
        public static final String RELEASE                  = "RELEASE";
        public static final String SIGLA                    = "SIGLA";
        public static final String SUBJECTAREA              = "SUBJECTAREA";
        public static final String TIPO                     = "TIPO";
        public static final String TIPO_SERV_TECN           = "TIPO_SERV_TEC";
        public static final String TIPO_CONSULTA            = "TIPO_CONSULTA";
        public static final String TITULO                   = "TITLE";
        public static final String ULTIMA_EJECUCION_KO      = "ULTIMA_EJECUCION_KO";
        public static final String ULTIMA_EJECUCION_OK      = "ULTIMA_EJECUCION_OK";
        public static final String USUARIO_CONSULTA         = "USUARIO_CONSULTA";
        public static final String VALORES                  = "VALORES";
        public static final String VERSION                  = "VERSION";
        public static final String VERSION_COPY             = "VEROBJ";
        public static final String VERSION_RELEASE          = "VERSION_RELEASE";
        public static final String VIGENTE                  = "VIGENTE";

    }

    public class Links {
        public static final String AGRUP__CLIENTO              = "AGRUPADOR--CLIE_ENTOR";

        public static final String AGRUP_TEC_D__AGRUP_TEC_V    = "APLICF---APLICF_VERS";
        public static final String AGRUP_TEC_D__TECNOL         = "APLICF--PLATAFORMA";
        public static final String AGRUP_TEC_D__SERVICIO_V     = "APLICF--SERVICIO_VER";
        public static final String AGRUP_TEC_D__USU_GEST       = "APLICF---USUGEST";
        public static final String AGRUP_TEC_D__USU_RESPONS    = "APLICF---USURESP";
        public static final String AGRUP_TEC_D__REPOSITORY     = "APLICF--REPOSITORY";

        public static final String AGRUP_TEC_V__ALP            = "APLICF_VERS--ALP";
        public static final String AGRUP_TEC_V__CONSUM         = "APLICF_VERS--APLICF_VERS_CORE";
        public static final String AGRUP_TEC_V__CLIENTO        = "APLICF_VERS--CLIE_ENTOR";
        public static final String AGRUP_TEC_V__CLIPUB         = "APLICF_VERS---CLIE";
        public static final String AGRUP_TEC_V__CLILINENTO     = "APLICF_VERS--CLIE_LIN_ENTOR";
        public static final String AGRUP_TEC_V__ENSAM          = "APLICF_VERS---ENS";
        public static final String AGRUP_TEC_V__ESCEN          = "APLICF_VERS---ESC";
        public static final String AGRUP_TEC_V__ESTADO         = "APLICF_VERS--ESTADO_AGRUP";
        public static final String AGRUP_TEC_V__FAC_PRI        = "APLICF_VERS--FACPRI";
        public static final String AGRUP_TEC_V__FAC_PUB        = "APLICF_VERS--FACPUB";
        public static final String AGRUP_TEC_V__FECHA_CLIE     = "APLICF_VERS---CLIE_FECHA_ENV";
        public static final String AGRUP_TEC_V__MODULO         = "APLICF_VERS---MOD";
        public static final String AGRUP_TEC_V__PROYEC         = "APLICF_VERS---PROY";
        public static final String AGRUP_TEC_V__PROYECTO_COBOL = "APLICF_VERS--PROYECTO";
        public static final String AGRUP_TEC_V__PRY_PHX        = "APLICF_VERS--PHXAPPL";
        public static final String AGRUP_TEC_V__REPOSITORY     = "APLICF_VERS--REPOSITORY";
        public static final String AGRUP_TEC_V__SERVV          = "APLICF_VERS--SERVICIO_VERSION";
        public static final String AGRUP_TEC_V__STATUS         = "APLICF_VERS--ESTADO_AGRUP";
        public static final String AGRUP_TEC_V__TECNOL         = "APLICF_VERS--PLATAFORMA";

        public static final String ALN__CLIENTO                = "ALN--CLIE_ENTOR";
        public static final String ALN__CLILINENTO             = "ALN--CLIE_LIN_ENTOR";
        public static final String ALN__OI                     = "APL---OI";

        public static final String ALP_D__ALP                  = "DALP---ALP";

        public static final String ALP__CLIENTO                = "ALP--CLIE_ENTOR";
        public static final String ALP__CLILINENTO             = "ALP--CLIE_LIN_ENTOR";
        public static final String ALP__OP                     = "APL---OP";

        public static final String API__OPERACION              = "API--OPERACION";
        public static final String APLIC__APLICV               = "APLICACION--APLICACION_VERSION";
        public static final String APLIC__ULTIMAVER            = "APLICACION--ULTIMA_VERSION";
        public static final String APLIC__SERV                 = "APLINF---DEFAPL";

        public static final String APLICV__ALCANCE             = "APLICACION_VERSION--ALCANCE";
        public static final String APLICV__CLIENTO             = "APLICACION_VERSION--CLIE_ENTOR";
        public static final String APLICV__DEMARCA             = "APLICACION_VERSION--DEMARCACION";
        public static final String APLICV__LABRESP             = "APLICACION_VERSION--LAB_RESPON";
        public static final String APLICV__SERVV               = "APLICACION_VER--SERVICIO_VER";
        public static final String APLICV__SETS                = "APLICACION_VERSION--SET";
        public static final String APLICV__TIPOLSW             = "APLICACION_VERSION--TIPOLOGIA_SW";
        public static final String APLICV__TIPOAPLIC           = "APLICACION_VERSION--TIPO_APLICAC";
        public static final String APLICV__USURESP             = "APLICACION_VERSION--USUARIO_RESP";

        public static final String AREALABRESP__AREA           = "LAB_RESPON--AREA";

        public static final String CAPA__SISTEMA               = "CAPA--SISTEMA";

        public static final String CLIENTE_PAIS                = "CLIE--PAIS";

        public static final String CLIENTO__CLIEN              = "CLIE_ENTOR---CLIE";
        public static final String CLIENTO__ENTORNO            = "CLIE_ENTOR---ENTOR";
        public static final String CLIENTO__CLIENTE            = "CLIE_ENTOR--CLIE";

        public static final String CLILINENTO__CLIENTE         = "CLIE_LIN_ENTOR--CLIE";
        public static final String CLILINENTO__ENTORNO         = "CLIE_LIN_ENTOR--ENTOR";
        public static final String CLILINENTO__LINEA           = "CLIE_LIN_ENTOR--LIN";

        public static final String DCOPY__COPYV                = "DCPY--CPY";

        public static final String DEMARCA__APLICV             = "DEMARCACION--APLICACION_VERSION";

        public static final String DENT__ENTV                  = "DENT--ENT";

        public static final String DEPARTAMENTO__AREA          = "LAB_RESPON--AREA";
        public static final String DEPARTAMENTO__GEO           = "LAB_RESPON--GEOGRAFIA";

        public static final String DJCL__CLIENTO               = "DJCL--CLIE_ENTOR";
        public static final String DJCL__CLILINENTO            = "DJCL--CLIE_LIN_ENTOR";

        public static final String DMSJ__MSJV                  = "DMSJ--MSJ";
        public static final String DMSJ__DTRX                  = "DMSJ--DTRX";

        public static final String DPGM__PGMV                  = "DPGM--PGM";

        public static final String DSER__SER_V                 = "DSER--SER";

        public static final String DTRX__TRXV                  = "DTRX--TRX";

        public static final String ENS_D__ENS                  = "DENS---ENS";

        public static final String ENSAMBLADO_CLIENTO          = "ENS---CLIE_ENTOR";

        public static final String ENTV__CLIENTO               = "ENT--CLIE_ENTOR";
        public static final String ENTV__CLILINENTO            = "ENT--CLIE_LIN_ENTOR";

        public static final String ESC_D__ESC                  = "DESC---ESC";

        public static final String ESCENARIO__CLIENTO          = "ESC--CLIE_ENTOR";
        public static final String ESCENARIO__OP               = "ESC---OP";

        public static final String FAC_PRI_D__FAC_PRI          = "DFAC_PRI---FACPRI";

        public static final String FAC_PRI__CLIENTO            = "FACPRI--CLIE_ENTOR";

        public static final String FAC_PUB_D__FAC_PUB          = "DFAC_PUB---FACPUB";

        public static final String FAC_PUB__CLIENTO            = "FACPUB--CLIE_ENTOR";

        public static final String GEO__TIPO_GEO               = "GEOGRAFIA--TIPO_GEOGRAFIA";

        public static final String FOLDER__MAPPING             = "INFA/HAS-MAPPING";
        public static final String FOLDER__MAPPLET             = "INFA/HAS-MAPPLET";
        public static final String FOLDER__WORKFLOW            = "INFA/HAS-WORKFLOW";
        public static final String FOLDER__WORKLET             = "INFA/HAS-WORKLET";

        public static final String MODULO_D__MODULO            = "DMLN---MOD";

        public static final String MODULO__ALN                 = "MODLN---ALN";
        public static final String MODULO__CLIENTO             = "MOD--CLIE_ENTOR";
        public static final String MODULOS_ENSAMBLADO          = "ENS---MOD";

        public static final String MSJV__CLIENTO               = "MSJ--CLIE_ENTOR";
        public static final String MSJV__CLILINENTO            = "MSJ--CLIE_LIN_ENTOR";

        public static final String OI__OPEBKS                  = "OPER---DOPER";

        public static final String OPER__OPERV                 = "OPERACION--OPERACION_VERSION";
        public static final String OPER__ULTIMAVER             = "OPERACION--ULTIMA_VERSION";

        public static final String OP__OPEBKS                  = "OPER---DOPER";

        public static final String OPEBKS__OI                  = "DOPER---OI";
        public static final String OPEBKS__OP                  = "DOPER---OP";

        public static final String PERIM__CLIENTE              = "PERIMETRO--CLIENTE";

        public static final String PGM__COPY                   = "PGM--CPY";

        public static final String PGMV__CLIENTO               = "PGM--CLIE_ENTOR";
        public static final String PGMV__CLILINENTO            = "PGM--CLIE_LIN_ENTOR";

        public static final String PROD_NEG__SUBSIS            = "PRODUCTO_NEGOCIO--SUBSISTEMA";

        public static final String PROYECTO_D__PROYECTO        = "DPROY---PROY";

        public static final String PROYECTO__CLIENTO           = "PROY--CLIE_ENTOR";

        public static final String REPOSITORY__FOLDER          = "INFA/HAS-FOLDER";

        public static final String SAT_OI                      = "OPER---ESTADO_SAT";
        public static final String SAT_MTJ                     = "ESTADO_SAT--MTJ";

        public static final String SER_V__CLIENTO              = "SER--CLIE_ENTOR";
        public static final String SER_V__CLILINENTO           = "SER--CLIE_LIN_ENTOR";

        public static final String SERV__OPER                  = "SERVICIO--OPERACION";
        public static final String SERV__SERVVER               = "SERVICIO--SERVICIO_VERSION";
        public static final String SERV__ULTIMAVER             = "SERVICIO--ULTIMA_VERSION";

        public static final String SERVV__TIPOSERV             = "SERVICIO_VER--TIPO_SW";
        public static final String SERVV__AGRUP_TEC_D          = "SERVICIO_VERSION--APLICF";
        public static final String SERVV__AGRUP_TEC_V          = "SERVICIO_VERSION--APLICF_VERS";
        public static final String SERVV__CLIENTO              = "SERVICIO_VERSION--CLIE_ENTOR";
        public static final String SERVV__DEMARCA              = "SERVICIO_VER--DEMARCACION";
        public static final String SERVV__DCOPY                = "SERVICIO_VER--DCPY";
        public static final String SERVV__DENT                 = "SERVICIO_VER--DENT";
        public static final String SERVV__DJCL                 = "SERVICIO_VER--DJCL";
        public static final String SERVV__DMSJ                 = "SERVICIO_VER--DMSJ";
        public static final String SERVV__DPGM                 = "SERVICIO_VER--DPGM";
        public static final String SERVV__DSER                 = "SERVICIO_VER--DSER";
        public static final String SERVV__DTRX                 = "SERVICIO_VER--DTRX";
        public static final String SERVV__SERVD                = "SERVICIO_VER--SERVICIO";
        public static final String SERVV__TECNOLOGIA           = "SERVICIO_VERSION--PLATAFORMA";
        public static final String SERVV__TIPOAPLIC            = "SERVICIO_VER--ALCANCE";

        public static final String SISTEMA__PROD_NEG           = "SISTEMA--PRODUCTO_NEGOCIO";
        public static final String SIST__SUBSISPER             = "SISTEMA--SUBSISTEMA_PER";

        public static final String SOCIEDAD__SOCIEDAD          = "SOCIEDAD--SOCIEDAD";
        public static final String SOCIEDAD_ARQ_PREFERENTE    = "SOCIEDAD--ARQUITECTURA_PREFERENT";
        public static final String SOCIEDAD_ARQ_REFERENCIA    = "SOCIEDAD--ARQUITECTURA_REFERENCI";

        public static final String SUBSIS__APLIC               = "SISTEM---APLINF";
        public static final String SUBSIS__AREAARQRESP         = "SUBSISTEMA--ARQ_RESPON";
        public static final String SUBSIS__AREALABRESP         = "SUBSISTEMA--LAB_RESPON";
        public static final String SUBSIS__ARQRESP             = "SUBSISTEMA--USUARIO_RESP";
        public static final String SUBSIS__GESTPROD            = "SUBSISTEMA--USUARIO_PROD";
        public static final String SUBSIS__GESTCONF            = "SUBSISTEMA--USUARIO_GESTOR";
        public static final String SUBSIS__SUBSIS_V            = "SUBSISTEMA--SUBSISTEMA_VERSION";
        public static final String SUBSIS__TIPOPROD            = "SUBSISTEMA--TIPO_PRODUCTO";

        public static final String TECNOLOGY_SOURCE			   = "PLATAFORMA--ORIGEN";
        public static final String TRXV__CLIENTO               = "TRX--CLIE_ENTOR";
        public static final String TRXV__CLILINENTO            = "TRX--CLIE_LIN_ENTOR";
        public static final String TRXO_OI                     = "OPER---ESTADO_TRXO";
        public static final String TRXO_MTI                    = "ESTADO_TRXO--MTI";

        public static final String USUARIO__DEPARTAM           = "USUARIO--LAB_RESPON";
        public static final String USUARIO_CONFIG              = "USUARIO--CONFIGURACION";

        public static final String SOCIEDAD_AREA               = "SOCIEDAD--AREA";
        public static final String USUARIO_AREA                = "USUARIO--AREA";
        public static final String SOCIEDAD_CLIENT_LIN         = "SOCIEDAD--CLIE_LIN";

    }

    // Tipos del modelo
    public class Types {
        public static final String APLIC_LOG_NEGOCIO     = "REPO_ALN";
        public static final String APLIC_LOG_PRES        = "REPO_ALP";
        public static final String APLIC_LOG_PRES_D      = "REPO_DALP";
        public static final String APLICACION_D          = "REPO_APLINF";
        public static final String APLICACION_V          = "AR_APLICACION_VERSION";
        public static final String AGRUP_TEC_D           = "REPO_APLICF";
        public static final String AGRUP_TEC_V           = "REPO_APLICF_VERS";
        public static final String ASC_PLAN              = "ASC_PLAN";
        public static final String AGRUPADOR             = "ATL_AGRUPADOR";
        public static final String AREA                  = "REPO_AREA";
        public static final String ASSET                 = "FW_ASSET";
        public static final String ASSET_TYPE            = "FW_ASSETTYPE";
        public static final String BIAN_ARQUITECTURA_REFERENCIA = "BIAN_ARQUITECTURA_REFERENCIA";
        public static final String BHTC                  = "BH_BHTC";
        public static final String CAPA                  = "AR_CAPA";
        public static final String CLIENTE               = "REPO_CLIE";
        public static final String CLIENTE_LINEA_ENTORNO = "H_REPO_CLIE_LIN_ENTOR";
        public static final String CLIENTE_ENTORNO       = "REPO_CLIE_ENTOR";
        public static final String CONCEPTO_MULTIIDIOMA  = "REPO_CONCEP_MULTI";
        public static final String CONF_XML              = "FW_CONFXML";
        public static final String CONFIG_GECOH          = "GECOH_CONFIGURACION";
        public static final String COPY_V                = "H_REPO_CPY";
        public static final String COPY_D                = "H_REPO_DCPY";
        public static final String CSS                   = "FW_CSS";
        public static final String DEMARCACION           = "AR_DEMARCACION";
        public static final String DEPARTAMENTO          = "AR_LAB_RESPON";
        public static final String DSC_ITEMTYPE          = "DSC/ITEMTYPE";
        public static final String EC_XML                = "FW_ECXML";
        public static final String ELEMENT_CATALOG       = "FW_EC";
        public static final String ENSAMBLADO            = "REPO_ENS";
        public static final String ENSAMBLADO_D          = "REPO_DENS";
        public static final String ENTIDAD_D             = "H_REPO_DENT";
        public static final String ENTIDAD_V             = "H_REPO_ENT";
        public static final String ESCENARIO             = "REPO_ESC";
        public static final String ESCENARIO_D           = "REPO_DESC";
        public final static String ESTADO_AGRUPACION     = "REPO_ESTADO_AGRUP";
        public final static String ESTADO_SAT            = "REPO_ESTADO_SAT";
        public final static String ESTADO_TRXO           = "REPO_ESTADO_TRXO";
        public static final String FACHADA_PUBLICA       = "REPO_FACPUB";
        public static final String FACHADA_PUBLICA_D     = "REPO_DFAC_PUB";
        public static final String FACHADA_PRIVADA       = "REPO_FACPRI";
        public static final String FACHADA_PRIVADA_D     = "REPO_DFAC_PRI";
        public static final String FECHA_ENV_CLIE        = "REPO_CLIE_FECHA_ENV";
        public static final String FLEX_FAMILY           = "FW_FLEXFAMILY";
        public static final String FOLDER                = "INFA/FOLDER";
        public static final String GEOGRAFIA             = "IND_GEOGRAFIA";
        public static final String INFA_MAPPING          = "INFA/MAPPING";
        public static final String INFA_MAPPLET          = "INFA/MAPPLET";
        public static final String INFA_WORKFLOW         = "INFA/WORKFLOW";
        public static final String INFA_WORKLET          = "INFA/WORKLET";
        public static final String ITEM                  = "ARS_ITEM";
        public static final String JAVA                  = "ISB_JAVA";
        public static final String JAVASCRIPT            = "FW_JAVASCRIPT";
        public static final String JCL_D                 = "H_REPO_DJCL";
        public static final String LINEA_DESPLIEGUE      = "H_REPO_LIN";
        public static final String MENSAJE_D             = "H_REPO_DMSJ";
        public static final String MENSAJE_V             = "H_REPO_MSJ";
        public static final String MODULO_NEGOCIO        = "REPO_MOD";
        public static final String MODULO_NEGOCIO_D      = "REPO_DMLN";
        public final static String MTI                   = "H_REPO_MTI";
        public final static String MTJ                   = "H_REPO_MTJ";
        public static final String OI                    = "REPO_OI";
        public static final String OP                    = "REPO_OP";
        public static final String OPERACION_BKS_D       = "REPO_DOPER";
        public static final String OPERACION_D           = "AR_OPERACION";
        public static final String OPERACION_V           = "AR_OPERACION_VERSION";
        public static final String PAIS                  = "AR_PAIS";
        public static final String PERIMETRO             = "AR_PERIMETRO_DESPLIEGUE";
        public static final String PROD_NEGOCIO          = "AR_PRODUCTO_NEGOCIO";
        public static final String PROGRAM_D             = "H_REPO_DPGM";
        public static final String PROGRAM_V             = "H_REPO_PGM";
        public static final String PROYECTO              = "REPO_PROY";
        public static final String PROYECTO_D            = "REPO_DPROY";
        public static final String REPOSITORY            = "INFA/REPOSITORY";
        public static final String NEXUS_REPOSITORY      = "AR_REPOSITORY";
        public static final String RESULTADO_CONSULTA    = "ATL_RESULTADO_CONSULTA";
        public static final String SERV_SAT_D            = "H_REPO_DSER";
        public static final String SERV_SAT_V            = "H_REPO_SER";
        public static final String SERVICIO_D            = "REPO_DEFAPL";
        public static final String SERVICIO_V            = "AR_SERVICIO_VERSION";
        public static final String SISTEMA               = "AR_SISTEMA";
        public static final String START_MENU            = "FW_STARTMENU";
        public static final String SUBSISTEMA            = "REPO_SISTEM";
        public static final String SUBSISTEMA_VERS       = "AR_SUBSISTEMA_VERSION";
        public static final String SUBTYPE               = "FW_SUBTYPE";
        public static final String TAG_LIB               = "ISB_TAGLIB";
        public static final String TIPO_APLICACION       = "AR_ALCANCE";
        public static final String TIPO_GEOGRAFIA        = "IND_TIPO_GEOGRAFIA";
        public static final String TIPO_SERVICIO         = "AR_TIPO_SW";
        public static final String TDC                   = "REPO_TDC";
        public static final String TECNOLOGIA            = "AR_PLATAFORMA";
        public static final String TRANSACCION_D         = "H_REPO_DTRX";
        public static final String TRANSACCION_V         = "H_REPO_TRX";
        public static final String TRANSACCION_O         = "H_REPO_TRO";
        public static final String USUARIO               = "REPO_USUARIO";
        public static final String SOCIEDAD              = "IND_SOCIEDAD";
        public static final String CLIENTE_LINEA         = "H_REPO_CLIE_LIN";
    }

    // Tipos para la gestión interna de Atlas
    public class AtlasTypes {
        public static final String DISCOVERY_ITEMTYPE  = "DSC/ITEMTYPE";
        public static final String DISCOVERY_ATTRIBUTE = "DSC/ATTRIBUTE";
        public static final String DISCOVERY_QUESTION  = "DSC/QUESTION";
        public static final String TEXTOS              = "ARS_TEXTOS";
    }

    // Tipos de Geografía
    public class GeographyTypes {
        public static final String GLOBAL = "0001";
        public static final String LOCAL  = "0002";
    }

    // Tipos de Aplicación
    public class ApplicationType {
        public static final String CORPORATIVO = "CORPORATIVO";
        public static final String GLOBAL      = "PRODUCTO";
        public static final String LATAM       = "HOMOLOGADO";
        public static final String LOCAL       = "LOCAL";
        public static final String NO_ISBAN    = "LEGACY";
        public static final String PERIMETRO   = "PERIMETRO";
    }

    // Entorno
    public class Environment {
        public static final String DESARROLLO    = "DE";
        public static final String PREPRODUCCION = "OR";
        public static final String PRODUCCION    = "EX";
    }

    // Indicador SGS
    public class SgsIndicator {
        public static final String YES = "SI";
        public static final String NO  = "NO";
    }


    public enum MemberClass {
        ADMINIST ("Administrador", "Administrador"),
        ARQUITEC ("Application Manager", "Arquitecto_Funcional"),
        GEST_SW  ("Gestion SW", "Gestion_Software"),
        CONSULTA ("General", "DefaultRol", ""),
        GESTORES ("Gestor", "Gestor_Configuracion"),
        LABORATO ("Governance", "Governance");

        private String type;
        private String value;
        private String role;

        MemberClass(String type, String role) {
            this.type = type;
            this.role = role;
            this.value = this.toString();
        }

        MemberClass(String type, String role, String value) {
            this.type = type;
            this.role = role;
            this.value = value;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }
    }

}

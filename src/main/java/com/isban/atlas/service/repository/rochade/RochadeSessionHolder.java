package com.isban.atlas.service.repository.rochade;

import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import com.isban.atlas.service.exceptions.ApiException;

import es.isb.atlas.rochade.data.access.RochadeAdminSession;
import es.isb.atlas.rochade.data.access.RochadeAutopilotSession;
import es.isb.atlas.rochade.data.access.RochadeSession;
import es.isb.atlas.rochade.data.access.RochadeSessionFactory;
import es.isb.atlas.rochade.data.access.exception.RochadeDataAccessException;

@Component()
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class RochadeSessionHolder {

   private static final Logger LOGGER = LoggerFactory.getLogger(RochadeSessionHolder.class);
   @Value("${rochade.host}")
   private String host;
   @Value("${rochade.port}")
   private int port;
   @Value("${rochade.database}")
   private String database;
   @Value("${rochade.user}")
   private String user;
   @Value("${rochade.password}")
   private String pwd;

   private RochadeSession rochadesession = null;
   private RochadeAutopilotSession rochadeAutopilotSession = null;
   private RochadeAdminSession rochadeAdminSession = null;
   private String username;
   private String password;

   public RochadeSessionHolder() {
      this.rochadesession = null;
   }

   public RochadeSessionHolder(String host, int port, String database) {
      this.rochadesession = null;
      this.host = host;
      this.port = port;
      this.database = database;
   }

   public void openRochadeSerssionMemberclass() throws ApiException {
	   try{
		   LOGGER.info("Estableciendo conexión a {}:{}. Base de datos {}", host, port, database);
	         this.rochadesession = RochadeSessionFactory.openRochadeSession(host, port, user, pwd, database);
	   } catch (RochadeDataAccessException e) {
	         throw new ApiException("DatabaseConnectonError", "Error en la conexion con la base de datos", e);
	   }
   }

   public void openRochadeSession(String username, String password) throws ApiException {
      try {
         LOGGER.info("Estableciendo conexión a {}:{}. Base de datos {}", host, port, database);
         this.username = username;
         this.password = password;
         this.rochadesession = RochadeSessionFactory.openRochadeSession(host, port, username.toUpperCase(),
               password, database);

      } catch (RochadeDataAccessException e) {
         throw new ApiException("DatabaseConnectonError", "Error en la conexion con la base de datos", e);
      }
   }

   public boolean isConnected() {
      return this.rochadesession != null;
   }

   public boolean isAutopilotConnected() {
      return this.rochadeAutopilotSession != null;
   }

   public boolean isAdminConnected() {
      return this.rochadeAdminSession != null;
   }

   public RochadeSession getRochadeSession() {
      return rochadesession;
   }

   public RochadeAutopilotSession getRochadeAutopilotSession() throws RochadeDataAccessException {
      if (!isAutopilotConnected()) {
         LOGGER.info("Estableciendo conexión autopilot a {}:{}. Base de datos {}", host, port, database);
         this.rochadeAutopilotSession = RochadeSessionFactory.openAutopilotSession(host, port,
               username.toUpperCase(), password, database);
      }
      return rochadeAutopilotSession;
   }

   public RochadeAdminSession getRochadeAdminSession() throws RochadeDataAccessException {
      if (!isAdminConnected()) {
         LOGGER.info("Estableciendo conexión administración a {}:{}. Base de datos {}", host, port, database);
//         this.rochadeAdminSession = RochadeSessionFactory.openAdminSession(host, port, username.toUpperCase(),
//               password);
         this.rochadeAdminSession = RochadeSessionFactory.openAdminSession(host, port, user, pwd);
      }
      return rochadeAdminSession;
   }

   private void closeRochadeSession() {
      if (this.rochadesession != null) {
         this.rochadesession.close();
         this.rochadesession = null;
      }
   }

   private void closeAutopilotSession() {
      if (this.rochadeAutopilotSession != null) {
         this.rochadeAutopilotSession.close();
         this.rochadeAutopilotSession = null;
      }
   }

   private void closeAdminSession() {
      if (this.rochadeAdminSession != null) {
         this.rochadeAdminSession.close();
         this.rochadeAdminSession = null;
      }
   }

   @PreDestroy
   public void cleanUp() {
      closeRochadeSession();
      closeAutopilotSession();
      closeAdminSession();
      LOGGER.info("RochadeSessionHolder Spring Container is destroy! Customer clean up");
   }

}

package com.isban.atlas.service.repository.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import com.isban.atlas.service.api.userauth.domain.UserAuthorization;

public interface UserAuthorizationRepository extends JpaRepository<UserAuthorization, Long> {

   UserAuthorization findByName(String name);

}

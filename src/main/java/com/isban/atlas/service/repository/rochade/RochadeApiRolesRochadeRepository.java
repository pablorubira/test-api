package com.isban.atlas.service.repository.rochade;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import es.isb.atlas.rochade.data.access.exception.RochadeDataAccessException;

/**
 *
 * @author XI320578
 *
 */
@Repository
public class RochadeApiRolesRochadeRepository extends AbstractRochadeRepository {

   private static final Logger LOGGER = LoggerFactory.getLogger(RochadeApiRolesRochadeRepository.class);

   @Autowired
   public RochadeApiRolesRochadeRepository(RochadeSessionHolder rochadeHolder,
         @Value("${sa.work.type}") final char type, @Value("${sa.work.name}") final String name,
         @Value("${sa.work.version}") final String version) {
      super(rochadeHolder, type, name, version);
   }

   public String getRochadeLoggedUserType() {
       try {
           return this.getSession().getMemberClass(appName);
       } catch (RochadeDataAccessException e) {
           LOGGER.error("Error al obtener el tipo de usuario en Rochade: ", e.getMessage(), e);
       }
       return null;
   }

   public String getRochadeUserType(String userName) {
      try {
         return this.getAutoPilotSession().getUserMemberClass(appName, userName);
      } catch (RochadeDataAccessException e) {
         LOGGER.error("Error al obtener el tipo de usuario en Rochade: ", e.getMessage(), e);
      }
      return null;
   }
}
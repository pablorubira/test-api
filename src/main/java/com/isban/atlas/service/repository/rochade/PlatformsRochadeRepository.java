package com.isban.atlas.service.repository.rochade;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestParam;

import com.isban.atlas.service.api.common.domain.Platform;
import com.isban.atlas.service.exceptions.BadRequestException;
import com.isban.atlas.service.repository.rochade.RochadeConstants.Links;
import com.isban.atlas.service.repository.rochade.RochadeConstants.Types;

import es.isb.atlas.rochade.data.access.exception.RochadeDataAccessException;
import es.isb.atlas.rochade.data.access.model.RochadeItemData;
import es.isb.atlas.rochade.data.access.model.RochadeItemData.CommonAtt;

@Service
public class PlatformsRochadeRepository extends AbstractRochadeRepository {

   @Autowired
   public PlatformsRochadeRepository(RochadeSessionHolder rochadeHolder,
         @Value("${sa.work.type}") final char type, @Value("${sa.work.name}") final String name,
         @Value("${sa.work.version}") final String version) {
      super(rochadeHolder, type, name, version);
   }


   	   /**
   	    * Método que crea la plataforma en el Json
   	    * @param id
   	    * @param name
   	    * @return
   	    */

   	   public Platform createPlatform(String id, String name) {

   		   Platform layer = new Platform();
   		   layer.id(id);
   		   layer.name(name);

   		   return layer;
		}

   	   /**
   	    * Valida los parámetros de la operación /technicalGroups
   	    * @param params
   	    */
   	   public void checkParams(@RequestParam MultiValueMap<String,String> params) {
   	       final Iterator<Entry<String, List<String>>> it = params.entrySet().iterator();
   	       while(it.hasNext()) {
   	           final String param = it.next().getKey();
   	           if (!param.matches(("name|origin"))) {
   	        	   throw new BadRequestException("InvalidParameter", "El parámetro " + param + " es incorrecto. Parámetros posibles: name | origin");
   	           }
   	       }
   	   }

	   /**
	    * Obtiene la lista de Plataformas en función de los filtros
	    *
	    * @return
	    * @throws RochadeDataAccessException
	    */

	   public List<Platform> getPlatformByFilters(String origin, String name) throws RochadeDataAccessException {

		    try {

		    	List<RochadeItemData> layerList = this.getSession().getAllItems(appName, Types.TECNOLOGIA, CommonAtt.ID, CommonAtt.NAME, CommonAtt.BUSINESS_NAME, Links.TECNOLOGY_SOURCE);
		    	List<Platform> jsonBody = new ArrayList<Platform>();
		    	int searchsize=0;

		    	// Búsqueda con comodines
		    	if (name != null && !name.isEmpty()) {
			        for (RochadeItemData itemLayer : layerList) {
			        	if (itemLayer.getLinkAtt(Links.TECNOLOGY_SOURCE).hasLinks()){

			        		 long idOrigin = itemLayer.getLinkAtt(Links.TECNOLOGY_SOURCE).getLink(0).getId();
			        		 String businessNameOrigin = this.getSession().getBusinessName(appName, idOrigin);

			        		 if (name.contains("*")) {
			        			 String[] result = name.split("\\*");
			        			 for (String word: result) {
			        				 if (!StringUtils.containsIgnoreCase(itemLayer.getBusinessName(), word)) {
			        					 searchsize=0;
			        				 } else {
			        					 searchsize=searchsize + 1;
			        				 }
			        			 }

			        			 if (businessNameOrigin.equals(origin) && searchsize==result.length){
			        				 jsonBody.add(createPlatform(itemLayer.getName(),itemLayer.getBusinessName()));
			        				 searchsize=0;
			        			 }

			        		 } else if (businessNameOrigin.equals(origin) && StringUtils.equalsIgnoreCase(itemLayer.getName(), (name))){
			        			 jsonBody.add(createPlatform(itemLayer.getName(),itemLayer.getBusinessName()));
			        		 }
			        	}

			        }

			    // Búsqueda sin comodines
		    	} else {

		    		for (RochadeItemData itemLayer : layerList) {
		    			if (itemLayer.getLinkAtt(Links.TECNOLOGY_SOURCE).hasLinks()) {

		            		long idOrigin = itemLayer.getLinkAtt(Links.TECNOLOGY_SOURCE).getLink(0).getId();
			        		String businessNameOrigin = this.getSession().getBusinessName(appName, idOrigin);

		    				if (businessNameOrigin.equals(origin)) {
			        			jsonBody.add(createPlatform(itemLayer.getName(),itemLayer.getBusinessName()));
		    				}
		    			}
			        }
		    	}

		        return jsonBody;

		    } catch (Exception ex) {
		        throw new RochadeDataAccessException("Error obteniendo la lista de Plataformas: " + ex, ex);
		    }
		}

}
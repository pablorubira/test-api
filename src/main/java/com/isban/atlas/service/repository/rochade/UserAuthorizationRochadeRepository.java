package com.isban.atlas.service.repository.rochade;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import com.isban.atlas.service.api.userauth.domain.AuthorizedArchitecture;
import com.isban.atlas.service.api.userauth.domain.AuthorizedClient;
import com.isban.atlas.service.api.userauth.domain.AuthorizedSociety;
import com.isban.atlas.service.api.userauth.domain.PreferredArchitecture;
import com.isban.atlas.service.api.userauth.domain.Society;
import com.isban.atlas.service.api.userauth.domain.UserAuthorization;
import com.isban.atlas.service.repository.rochade.RochadeConstants.Links;
import com.isban.atlas.service.repository.rochade.RochadeConstants.Types;

import es.isb.atlas.rochade.data.access.ReportPathStep;
import es.isb.atlas.rochade.data.access.exception.RochadeDataAccessException;
import es.isb.atlas.rochade.data.access.model.RochadeItemData;
import es.isb.atlas.rochade.data.access.model.RochadeItemData.CommonAtt;

/**
 *
 * Usuario -> Departamento -> Area Laboratorio <- Sociedad -> Cliente – Linea Despliegue \-> Sociedad
 *
 * El report path tendra como semilla las sociedades y tomara tres caminos:
 *
 * IND_SOCIEDAD -[SOCIEDAD--AREA]-> REPO_AREA <-[USUARIO--AREA]- REPO_USUARIO
 *
 * IND_SOCIEDAD -[SOCIEDAD--CLIE_LIN]-> H_REPO_CLIE_LIN
 *
 * IND_SOCIEDAD -[SOCIEDAD--SOCIEDAD]-> IND_SOCIEDAD
 *
 * IND_SOCIEDAD -[SOCIEDAD--ARQUITECTURA_REFERENCI]-> BIAN_ARQUITECTURA_REFERENCIA
 *
 * IND_SOCIEDAD -[SOCIEDAD--ARQUITECTURA_PREFERENT]-> BIAN_ARQUITECTURA_REFERENCIA
 *
 * @author XI320578
 *
 */

@Repository
public class UserAuthorizationRochadeRepository extends AbstractRochadeRepository {

   private static final int ROOT_LEVEL = 0;
   private static final Logger LOGGER = LoggerFactory.getLogger(UserAuthorizationRochadeRepository.class);
   private static Map<String, String> areasCache = null;

   @Autowired
   public UserAuthorizationRochadeRepository(RochadeSessionHolder rochadeHolder,
         @Value("${sa.work.type}") final char type, @Value("${sa.work.name}") final String name,
         @Value("${sa.work.version}") final String version) {
      super(rochadeHolder, type, name, version);
   }

   public Collection<Society> getSocieties() throws RochadeDataAccessException {
      LOGGER.info("Se inicia la consulta de sociedades presentes en Rochade");
      Map<String, String> architecturesCache = getArchitectures();
      Map<String, String> societiesCache = getSocietyMap();
      areasCache = getAreaMap();
      ReportPathStep[] steps = new ReportPathStep[2];
      steps[0] = new ReportPathStep(Links.SOCIEDAD_AREA, Links.SOCIEDAD_CLIENT_LIN, Links.SOCIEDAD__SOCIEDAD, Links.SOCIEDAD_ARQ_REFERENCIA, Links.SOCIEDAD_ARQ_PREFERENTE);
      steps[1] = new ReportPathStep(Links.USUARIO_AREA);
      steps[1].setDirection(ReportPathStep.DIRECTION_USAGE);
      ReportPathStep itemType = new ReportPathStep(Types.SOCIEDAD);

      List<RochadeItemData> data = this.getSession().executeTypeReportPath(appName, itemType, steps,
            CommonAtt.NAME, CommonAtt.TYPE, CommonAtt.ID, CommonAtt.LEVEL, CommonAtt.BUSINESS_NAME, Links.SOCIEDAD__SOCIEDAD, Links.SOCIEDAD_ARQ_PREFERENTE, Links.SOCIEDAD_ARQ_REFERENCIA, Links.USUARIO_AREA);
      Collection<Society> societies = new ArrayList<>();
      Society tmpSociety = null;
      for (RochadeItemData item : data) {
         switch (item.getType()) {
         case Types.SOCIEDAD:
            if (isRootLevel(item)) {
               tmpSociety = handleRootSociety(item);
               societies.add(tmpSociety);

               if (item.getLinkAtt(Links.SOCIEDAD_ARQ_REFERENCIA).hasLinks() ) {
               	List<String> referencelist = item.getLinksNames(Links.SOCIEDAD_ARQ_REFERENCIA);
               	for (String architecture : referencelist) {
               		handleAuthorizedArchitecture(tmpSociety, architecture, architecturesCache.get(architecture));
               	}
               }

               if (item.getLinkAtt(Links.SOCIEDAD_ARQ_PREFERENTE).hasLinks() ) {
               	List<String> preferredlist = item.getLinksNames(Links.SOCIEDAD_ARQ_PREFERENTE);
               	for (String architecture : preferredlist) {
               		handlePreferredArchitecture(tmpSociety, architecture, architecturesCache.get(architecture));
               	}
               }

               if (item.getLinkAtt(Links.SOCIEDAD__SOCIEDAD).hasLinks() ) {
            	   List<String> userSociety = item.getLinksNames(Links.SOCIEDAD__SOCIEDAD);
            	   for (String society : userSociety) {
            		   if (society.equals(item.getName())) {
            			   handleAuthorizedUserSociety(tmpSociety, society, societiesCache.get(society));
            		   }
            	   }
               }

            } else {
               handleAuthorizedSociety(tmpSociety, item);
            }

            break;
         case Types.USUARIO:
            handleUserAuthorization(tmpSociety, item);
            break;
         case Types.CLIENTE_LINEA:
            handleAuthorizedClient(tmpSociety, item);
            break;
         case Types.AREA:
            break;
         case Types.BIAN_ARQUITECTURA_REFERENCIA:
        	 break;
         case Types.DEPARTAMENTO:
        	 break;
         default:
            throw new RochadeDataAccessException(
                  "Tipo no soportado en consulta getSocieties " + item.getType());
         }

      }
      LOGGER.info("Consulta de sociedades finalizada");
      return societies;
   }


   /**
    * Autorización de Sociedades semilla
    * @param item
    * @return
    */

   private Society handleRootSociety(RochadeItemData item) {
      Society tmpSociety;
      tmpSociety = new Society(item.getName());
      tmpSociety.setBusinessName(item.getBusinessName());
      return tmpSociety;
   }

   /**
    * Autorización de Sociedades
    * @param tmpSociety
    * @param item
    */

   private void handleAuthorizedSociety(Society tmpSociety, RochadeItemData item) {
      AuthorizedSociety authorizedSociety = new AuthorizedSociety(item.getName());
      authorizedSociety.setBusinessName(item.getBusinessName());
      assert tmpSociety != null;
      tmpSociety.addAuthorizedSociety(authorizedSociety);
   }

   /**
    * Autorización de Sociedad del usuario
    * @param tmpSociety
    * @param item
    */

   private void handleAuthorizedUserSociety(Society tmpSociety, String itemname, String businessName) {
      AuthorizedSociety authorizedSociety = new AuthorizedSociety(itemname);
      authorizedSociety.setBusinessName(businessName);
      assert tmpSociety != null;
      tmpSociety.addAuthorizedSociety(authorizedSociety);
   }

   /**
    * Autorización de Arquitectura de referencia
    * @param tmpSociety
    * @param itemname
    * @param businessname
    */

   private void handleAuthorizedArchitecture (Society tmpSociety, String itemname, String businessname) {
	      AuthorizedArchitecture authorizedArchitecture = new AuthorizedArchitecture(itemname);
	      authorizedArchitecture.setBusinessName(businessname);
	      assert tmpSociety != null;
	      tmpSociety.addAuthorizedArchitecture(authorizedArchitecture);
   }

   /**
    * Autorización de Arquitectura Preferente
    * @param tmpSociety
    * @param itemname
    * @param businessname
    */

   private void handlePreferredArchitecture (Society tmpSociety, String itemname, String businessname) {
	   	  PreferredArchitecture preferredArchitecture = new PreferredArchitecture(itemname);
	   	  preferredArchitecture.setBusinessName(businessname);
	      assert tmpSociety != null;
	      tmpSociety.setPreferredArchitecture(preferredArchitecture);
   }

   /**
    * Autorización de Clientes
    * @param tmpSociety
    * @param item
    */

   private void handleAuthorizedClient(Society tmpSociety, RochadeItemData item) {
      AuthorizedClient authorizedClient = new AuthorizedClient(item.getName());
      assert tmpSociety != null;
      tmpSociety.addAuthorizedClient(authorizedClient);
   }

   /**
    * Autorización de Usuarios
    * @param tmpSociety
    * @param item
    * @throws RochadeDataAccessException
    */

   private void handleUserAuthorization(Society tmpSociety, RochadeItemData item) {
      UserAuthorization user = new UserAuthorization(item.getName());
      user.setSociety(tmpSociety);
      List<String> userArea = item.getLinksNames(Links.USUARIO_AREA);
      if (!userArea.isEmpty()) {
    	  user.setArea(areasCache.get(userArea.get(0)));
      } else {
    	  user.setArea("");
      }
      assert tmpSociety != null;
      tmpSociety.addUser(user);
   }

	/**
	 * Cache con cada Arquitectura de referencia <ItemName,BusinessName>
	 * @return
	 * @throws RochadeDataAccessException
	 */

	private Map<String, String> getArchitectures() throws RochadeDataAccessException {

		Map<String, String> architecturesList = new HashMap<String, String>();
		List<RochadeItemData> architectures = this.getSession().getAllItems(appName, Types.BIAN_ARQUITECTURA_REFERENCIA, CommonAtt.ID, CommonAtt.NAME, CommonAtt.BUSINESS_NAME);

        for (RochadeItemData architecture : architectures) {
    	    architecturesList.put(architecture.getName(), architecture.getBusinessName());
        }

		return architecturesList;
	}

	/**
	 * Caché con las sociedades
	 * @return
	 * @throws RochadeDataAccessException
	 */
	private Map<String, String> getSocietyMap() throws RochadeDataAccessException {

		Map<String, String> societiesList = new HashMap<String, String>();
		List<RochadeItemData> userSocieties = this.getSession().getAllItems(appName, Types.SOCIEDAD, CommonAtt.ID, CommonAtt.NAME, CommonAtt.BUSINESS_NAME);

        for (RochadeItemData society : userSocieties) {
        	societiesList.put(society.getName(), society.getBusinessName());
        }

		return societiesList;
	}

	/**
	 * Caché con las areas
	 * @return
	 * @throws RochadeDataAccessException
	 */
	private Map<String, String> getAreaMap() throws RochadeDataAccessException {

		Map<String, String> areasList = new HashMap<String, String>();
		List<RochadeItemData> userArea = this.getSession().getAllItems(appName, Types.AREA, CommonAtt.ID, CommonAtt.NAME, CommonAtt.BUSINESS_NAME);

        for (RochadeItemData area : userArea) {
        	areasList.put(area.getName(), area.getName());
        }

		return areasList;
	}


   private boolean isRootLevel(RochadeItemData item) {
      return item.getLevel() == ROOT_LEVEL;
   }

}
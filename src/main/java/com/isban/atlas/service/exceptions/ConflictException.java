package com.isban.atlas.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ConflictException extends ApiException {

   private static final long serialVersionUID = -8618245588720738863L;

   public ConflictException(String code, String msg) {
      super(code, msg);
   }

   public ConflictException(String code, String msg, Throwable th) {
      super(code, msg, th);
   }

}

package com.isban.atlas.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.LOCKED)
public class LockResourceException extends ApiException {

   private static final long serialVersionUID = -4659780754898542225L;

   public LockResourceException(String code, String msg) {
      super(code, msg);
   }

   public LockResourceException(String code, String msg, Throwable th) {
      super(code, msg, th);
   }

}

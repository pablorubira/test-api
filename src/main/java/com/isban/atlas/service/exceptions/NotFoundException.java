package com.isban.atlas.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends ApiException {

   private static final long serialVersionUID = -7427061177386756100L;

   public NotFoundException() {
       super(null, null);
   }

   public NotFoundException(String code, String msg) {
      super(code, msg);
   }

   public NotFoundException(String code, String msg, Throwable th) {
      super(code, msg, th);
   }
}

package com.isban.atlas.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends ApiException {

   private static final long serialVersionUID = 2166708117797351092L;

   public BadRequestException(String code, String msg) {
      super(code, msg);
   }

   public BadRequestException(String code, String msg, Throwable th) {
      super(code, msg, th);
   }

}

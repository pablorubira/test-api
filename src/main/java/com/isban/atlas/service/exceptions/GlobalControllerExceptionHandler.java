package com.isban.atlas.service.exceptions;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.isban.atlas.service.api.Error;

@RestControllerAdvice
public class GlobalControllerExceptionHandler {

   private static final String GENERIC_ERROR_MESSAGE = "Se ha producido una error provocado por la excepcion  {} ";

   private static final Logger LOGGER = LoggerFactory.getLogger(GlobalControllerExceptionHandler.class);

   public static final String DEFAULT_ERROR_VIEW = "Error no controlado";
   private static final HttpStatus DEFAULT_ERROR_HTTP_STATUS = HttpStatus.BAD_REQUEST;

   @ExceptionHandler(ApiException.class)
   public ResponseEntity<Error> handleApiException(ApiException e) {
      LOGGER.error(GENERIC_ERROR_MESSAGE, e.getMessage(), e);
      HttpStatus exceptionHttpStatus = DEFAULT_ERROR_HTTP_STATUS;
      ResponseStatus responseStatusFromAnnotation = AnnotationUtils.findAnnotation(e.getClass(),
            ResponseStatus.class);
      if (responseStatusFromAnnotation != null) {
         exceptionHttpStatus = responseStatusFromAnnotation.value();
      } else {
         LOGGER.debug("Si pasa esto deberiamos lanzar una excepcion y dejarla salir??");
         LOGGER.warn("Se establece el status code por defecto pero esto no deberia de pasar nunca");
      }
      if (e.getCode() != null) {
          Error error = createError(e.getCode(), e.getMessage());
          return new ResponseEntity<>(error, exceptionHttpStatus);
      } else {
          return new ResponseEntity<>(exceptionHttpStatus);
      }
   }

   @ExceptionHandler(MethodArgumentNotValidException.class)
   public ResponseEntity<Error> invalidInput(MethodArgumentNotValidException e)
         throws MethodArgumentNotValidException {
      LOGGER.error("Se ha producido una error provocado por la excepcion {} ", e.getMessage(), e);
      if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null)
         throw e;
      BindingResult result = e.getBindingResult();
      List<String> errorMessages = new ArrayList<>();
      for (ObjectError objectError : result.getAllErrors()) {
         errorMessages.add(objectError.getDefaultMessage());
      }
      Error error = createError("MissingRequiredParameter",
            StringUtils.join(errorMessages.toArray(new String[errorMessages.size()]), "; "));
      return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
   }

   @ExceptionHandler(MissingServletRequestParameterException.class)
   public ResponseEntity<Error> missingParameter(MissingServletRequestParameterException e)
                   throws MissingServletRequestParameterException {
       LOGGER.error("Se ha producido una error provocado por la excepcion {} ", e.getMessage(), e);
       if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null)
          throw e;
       Error error = createError("MissingRequiredParameter", "No se ha especificado el parámetro obligatorio '" + e.getParameterName() + "'");
       return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

   @ExceptionHandler(UnsatisfiedServletRequestParameterException.class)
   public ResponseEntity<Error> missingParameter(UnsatisfiedServletRequestParameterException e)
                   throws UnsatisfiedServletRequestParameterException {
       LOGGER.error("Se ha producido una error provocado por la excepcion {} ", e.getMessage(), e);
       if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null)
          throw e;
       Error error = createError("MissingRequiredParameter", e.getMessage());
       return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

   @ExceptionHandler(value = Exception.class)
   public <T extends Exception> ResponseEntity<Error> defaultErrorHandler(HttpServletRequest req, T e)
         throws T {
      LOGGER.error("Se ha producido una error provocado por la excepcion {} ", e.getMessage(), e);
      if (AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class) != null)
         throw e;
      Error error = createError(DEFAULT_ERROR_VIEW, e.getMessage());
      return new ResponseEntity<>(error, DEFAULT_ERROR_HTTP_STATUS);
   }

   private Error createError(String code, String message) {
      return new Error().code(code).message(message);
   }
}

package com.isban.atlas.service.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ApiException extends RuntimeException {

   private static final long serialVersionUID = 106206740435557550L;

   private final String code;

   public ApiException(String code, String msg) {
      super(msg);
      this.code = code;
   }

   public ApiException(String code, String msg, Throwable th) {
      super(msg, th);
      this.code = code;
   }

   public String getCode() {
      return code;
   }

}

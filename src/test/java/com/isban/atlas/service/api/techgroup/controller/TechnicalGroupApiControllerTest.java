package com.isban.atlas.service.api.techgroup.controller;

import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.isban.atlas.service.api.common.domain.Platform;
import com.isban.atlas.service.api.techgroup.domain.FunctionalService;
import com.isban.atlas.service.api.techgroup.domain.TechnicalGroup;
import com.isban.atlas.service.api.techgroup.domain.TechnicalGroupPost;
import com.isban.atlas.service.api.techgroup.domain.User;
import com.isban.atlas.service.api.techgroup.service.TechnicalGroupService;
import com.isban.atlas.service.exceptions.ApiException;
import com.isban.atlas.service.exceptions.ConflictException;
import com.isban.atlas.service.exceptions.InternalErrorException;
import com.isban.atlas.service.exceptions.NotFoundException;

@RunWith(SpringRunner.class)
@WebMvcTest(TechnicalGroupsApiController.class)
public class TechnicalGroupApiControllerTest {

   @Autowired
   private MockMvc mvc;

   @MockBean
   private TechnicalGroupService technicalGroupService;

   @Test
   @WithMockUser
   public void test1() throws Exception {
      given(this.technicalGroupService.createNewTechnicalGroup(any(TechnicalGroupPost.class)))
            .willReturn(createMockTechnicalGroup());
      this.mvc.perform(post("/technicalGroups").content(asJsonString(createMockTechnicalGroup()))
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().is(201));
   }

   @Test
   @WithMockUser
   public void test2() throws Exception {
      given(this.technicalGroupService.createNewTechnicalGroup(any(TechnicalGroupPost.class)))
            .willThrow(new InternalErrorException("codigo", "mensaje"));
      this.mvc.perform(post("/technicalGroups").content(asJsonString(createMockTechnicalGroup()))
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().is(500));
   }

   @Test
   @WithMockUser
   public void test3() throws Exception {
      given(this.technicalGroupService.createNewTechnicalGroup(any(TechnicalGroupPost.class)))
            .willThrow(new ApiException("codigo", "mensaje"));
      this.mvc.perform(post("/technicalGroups").content(asJsonString(createMockTechnicalGroup()))
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().is(400));
   }

   @Test
   @WithMockUser
   public void test4() throws Exception {
      given(this.technicalGroupService.createNewTechnicalGroup(any(TechnicalGroupPost.class)))
            .willThrow(new NotFoundException("codigo", "mensaje"));
      this.mvc.perform(post("/technicalGroups").content(asJsonString(createMockTechnicalGroup()))
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().is(404));
   }

   @Test
   @WithMockUser
   public void test5() throws Exception {
      given(this.technicalGroupService.createNewTechnicalGroup(any(TechnicalGroupPost.class)))
            .willThrow(new ConflictException("codigo", "mensaje"));
      this.mvc.perform(post("/technicalGroups").content(asJsonString(createMockTechnicalGroup()))
            .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).accept(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(status().is(409));
   }


   public static String asJsonString(final Object obj) {
      try {
         final ObjectMapper mapper = new ObjectMapper();
         final String jsonContent = mapper.writeValueAsString(obj);
         return jsonContent;
      } catch (Exception e) {
         throw new RuntimeException(e);
      }
   }

   private TechnicalGroup createMockTechnicalGroup() {
      System.out.println("Prueba de creación de Agrupación técnica");
      TechnicalGroup techGroupInfo = new TechnicalGroup();
      techGroupInfo.setName("PAAS_TEST_API_TechGroup_JL_10");
      techGroupInfo.setDescription("Prueba de alta de Agrupación Técnica a través de la API");
      // Datos del servicio funcional
      FunctionalService serviceInfo = new FunctionalService();
      serviceInfo.setId("51200113");
      techGroupInfo.setFunctionalService(serviceInfo);
      // Datos de la plataforma
      Platform platformInfo = new Platform();
      platformInfo.setId("PAAS");
      techGroupInfo.setPlatform(platformInfo);
      // Datos del usuario responsable
      User userInfo = new User();
      userInfo.setId("xIS00770");
      techGroupInfo.setResponsible(userInfo);
      return techGroupInfo;
   }

   private TechnicalGroup createMockTechnicalGroupWithoutUser() {
      System.out.println("Prueba de creación de Agrupación técnica");
      TechnicalGroup techGroupInfo = new TechnicalGroup();
      techGroupInfo.setName("PAAS_TEST_API_TechGroup_JL_10");
      techGroupInfo.setDescription("Prueba de alta de Agrupación Técnica a través de la API");
      // Datos del servicio funcional
      FunctionalService serviceInfo = new FunctionalService();
      serviceInfo.setId("51200113");
      techGroupInfo.setFunctionalService(serviceInfo);
      // Datos de la plataforma
      Platform platformInfo = new Platform();
      platformInfo.setId("PAAS");
      techGroupInfo.setPlatform(platformInfo);
      return techGroupInfo;
   }
}

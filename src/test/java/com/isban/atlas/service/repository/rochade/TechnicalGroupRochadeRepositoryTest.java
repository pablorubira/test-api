package com.isban.atlas.service.repository.rochade;

import java.util.ArrayList;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.isban.atlas.service.exceptions.ApiException;
import com.isban.atlas.service.repository.rochade.RochadeConstants.MemberClass;

import es.isb.atlas.rochade.data.access.exception.RochadeDataAccessException;

@RunWith(SpringRunner.class)
@SpringBootTest
@Ignore
public class TechnicalGroupRochadeRepositoryTest {

   private static final Logger LOGGER = LoggerFactory.getLogger(TechnicalGroupRochadeRepositoryTest.class);

   @Autowired
   private RochadeSessionHolder rochadeSessionHolder;

   @Autowired
   private TechnicalGroupRochadeRepository technicalGroupRochadeRepository;

   @Test
   public void whenFindByName_thenReturnEmployee() throws RochadeDataAccessException, ApiException {
      rochadeSessionHolder.openRochadeSession("ARS_USER", "laqueyoquiera");
      technicalGroupRochadeRepository.changeMemberClass("XI320578", MemberClass.CONSULTA);
   }

   @Test
   public void whenFindByName_thenReturnEmployee2() throws RochadeDataAccessException, ApiException {
      rochadeSessionHolder.openRochadeSession("ARS_USER", "laqueyoquiera");
      LOGGER.debug("Prueba de creación de Agrupación técnica");
      ArrayList<String> repositories = new ArrayList<String>();
      repositories.add("https://github.com/foo/Foo.git");

      String resultTechGroup = technicalGroupRochadeRepository.createTechGroup(
            "PAAS_TEST_API_TechGroup_JL_16", "Prueba de alta de Agrupación Técnica a través de la API",
            "xis00770", "SAMC_JAVA", "51200113");
      LOGGER.debug("Id Resultado {}", resultTechGroup);
   }

}

package com.isban.atlas.service.repository.jpa;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.isban.atlas.service.api.userauth.domain.AuthorizedClient;
import com.isban.atlas.service.api.userauth.domain.AuthorizedSociety;
import com.isban.atlas.service.api.userauth.domain.Society;
import com.isban.atlas.service.api.userauth.domain.UserAuthorization;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SocietyRepositoryTest {

   @Autowired
   private TestEntityManager entityManager;

   @Autowired
   private SocietyRepository societyRepository;

   @Test
   public void whenFindByName_thenReturnEmployee() {
      // given
      Society society = new Society("sociedad_test");

      AuthorizedClient authorizedClient = new AuthorizedClient("PEPE LUIS");
      society.addAuthorizedClient(authorizedClient);
      AuthorizedSociety authorizedSociety = new AuthorizedSociety("sociedad autorizada");
      society.addAuthorizedSociety(authorizedSociety);

      UserAuthorization user = new UserAuthorization("XI320578");
      user.setSociety(society);
      society.addUser(user);

      entityManager.persist(society);
      entityManager.flush();

      // when
      Society found = societyRepository.findByName(society.getName());

      // then
      assertThat(found.getName()).isEqualTo(society.getName());
      assertThat(found.getAuthorizations().getAuthorizedClients().size()).isEqualTo(1);
      assertThat(found.getAuthorizations().getAuthorizedSocieties().size()).isEqualTo(1);
      assertThat(found.getUsers().size()).isEqualTo(1);

      societyRepository.deleteAll();

   }

}
